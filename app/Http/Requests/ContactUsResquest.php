<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class ContactUsResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'string|required',
            'mobile'=>'numeric|required',
            'message'=>'string|required',
            'email'=>'email',
        ];

    }
    public function messages()
    {
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        $message=__('validation.attributes.invalidForm');
        $response = new JsonResponse(['ok' => 'false',
            'code' => 403,
            'message' => $message,
            'data' => $validator->errors()
        ]);

        throw new ValidationException($validator, $response);
    }
}
