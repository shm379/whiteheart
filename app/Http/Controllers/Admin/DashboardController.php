<?php

namespace App\Http\Controllers\Admin;

use App\Models\Invoice;
use App\Models\Pistachio;
use App\Models\Product;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Show the main admin dashboard
     *
     * @return Factory|View
     */
    public function mainDashboard()
    {
        $todayInvoices = Invoice::where('created_at', '>=', today('Asia/Tehran')->timezone('UTC'))->count();
        $todayUsers = User::where('created_at', '>=', today('Asia/Tehran')->timezone('UTC'))->count();
        $products = Product::count();
        $users = User::count();
        $newInvoices = Invoice::with(['user'])->where('status', '0')->get();
        return view('admin.main_dashboard', [
            'users' => $users,
            'todayUsers' => $todayUsers,
            'products' => $products,
            'todayInvoices' => $todayInvoices,
            'newInvoices' => $newInvoices
        ]);
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('admin.login.form');
    }
}
