@extends('admin.frame')
@section('custom-style')
    <script src="/js/sparkline-chart.js"></script>
    <script src="/js/easy-pie-chart.js"></script>
@endsection
@section('main-content')
    <div class="row state-overview">
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol terques">
                    <i class="icon-user"></i>
                </div>
                <div class="value">
                    <h1>{{$users}}</h1>
                    <p>کاربران</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol red">
                    <i class="icon-food"></i>
                </div>
                <div class="value">
                    <h1>{{$products}}</h1>
                    <p>غذاها</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol yellow">
                    <i class="icon-shopping-cart"></i>
                </div>
                <div class="value">
                    <h1>{{$todayInvoices}}</h1>
                    <p>سفارشات امروز</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol blue">
                    <i class="icon-power-off"></i>
                </div>
                <div class="value">
                    <h1>{{$todayUsers}}</h1>
                    <p>کاربران امروز</p>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    سفارشات بررسی نشده
                </header>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>کاربر</th>
                        <th>مبلغ کل</th>
                        <th>مبلغ قابل پرداخت</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($newInvoices as $invoice)
                        <tr>
                            <td>
                                <a href="{{route('admin.dashboard.invoices.show',[$invoice->id])}}">{{$invoice->id}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.dashboard.users.show',[$invoice->user->id])}}">{{$invoice->user->name}} {{$invoice->user->family}}</a>
                            </td>
                            <td>{{$invoice->total_price}}</td>
                            <td>{{$invoice->payable_price}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>


@endsection
