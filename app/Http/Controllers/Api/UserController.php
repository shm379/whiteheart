<?php

namespace App\Http\Controllers\Api;

use App\Events\UserSignedIn;
use App\Events\UserSignedUp;
use App\Events\UserUpdated;
use App\Http\Requests\UserSignUpRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\FalseTextResource;
use App\Http\Resources\TrueTextResource;
use App\Http\Resources\UserResource;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**@var User */
    public $user;

    public function __construct()
    {
        $this->middleware('auth:api')->only(['show', 'update']);
        $this->user = auth('api')->user();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserSignUpRequest $request
     * @return TrueTextResource|\Illuminate\Http\JsonResponse|object
     * @throws \Throwable
     */
    public function store(UserSignUpRequest $request)
    {
        try {
            $user = User::createOrUpdateWhenMobileNotVerified($request->all());
            if(!$user)
                return falseJsonResponse("این شماره موبایل قبلا در سیستم ثبت شده است .");
            event(new UserSignedUp($user));
            $code = $user->generateVerifyingCode();
            \Kavenegar::VerifyLookup($user->mobile, $code, null, null, 'verifyrestaurant');
            return trueJsonResponse(__('message.api.userSignedUpAndVerifyCodeSent'), 203, $user);
        } catch (\Exception $exception) {
            return falseJsonResponse(__('message.api.userSignedUpFailed'), 403, $request);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return UserResource
     */
    public function show()
    {
        return new UserResource($this->user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @return UserResource
     */
    public function update(UserUpdateRequest $request)
    {
        if ($this->user->update($request->all())) {
            event(new UserUpdated($this->user));
            return new UserResource($this->user);
        }
        return falseJsonResponse(__('message.api.updateInformationFailed'), 403, $this->user);
    }


    /**
     * Sign in and send verify code to user with mobile number
     * @param Request $request
     * @return TrueTextResource
     */
    public function getVerifyCode(Request $request)
    {
        try {
            $user = User::whereMobile($request->input('mobile'))->firstOrFail();
            $code = $user->generateVerifyingCode();
            \Kavenegar::VerifyLookup($user->mobile, $code, null, null, 'verifyrestaurant');
            return trueJsonResponse(__('message.api.sendVerifyCodeSms'), 203, ['mobile' => $user->mobile]);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.userNotFound'));
        }

    }

    /**
     * If  code and mobile were valid , return access-token
     * @param Request $request
     * @return FalseTextResource|TrueTextResource
     */
    public function verifyMobile(Request $request)
    {
        try {
            $user = User::whereMobile($request->input('mobile'))->firstOrFail();
            if (!$user->checkVerifyingCode($request->input('code')))
                return falseJsonResponse(__('message.api.wrongMobileVerifyCode'), 401);
            $user->verifyMobile();
            return trueJsonResponse("", 202, ['access_token' => $user->createToken("Api Login")->accessToken]);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.userNotFound'));
        }

    }

    public function login(Request $request)
    {
        try {
            $user = User::whereMobile($request->input('mobile'))->firstOrFail();
            if (!$user->mobile_verified_at)
                return falseJsonResponse(__('message.api.yourAccountHaveNotVerified'), 401);
            if (!Hash::check($request->input('password'), $user->password))
                return falseJsonResponse(__('message.api.wrongInformation'), 401);
            $token = $user->createToken("Api Login")->accessToken;
            event(new UserSignedIn($user));
            return trueJsonResponse("", 202, ['access_token' => $token]);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.userNotFound'));
        }
    }

    public function comments()
    {
        $comments = auth('api')
            ->user()
            ->comments()
            ->with(['product.images'])
            ->get();
        return new CommentCollection($comments);
    }


    public function deleteComment($id)
    {
        try {
            $comment = Comment::where('user_id', auth('api')->id())->findOrFail($id);
            return $comment->delete() ? trueJsonResponse(__('message.api.commentDeleteSuccessfully')) : falseJsonResponse(__('message.api.somethingWasWrong'));
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.modelNotFound'));
        }
    }
}
