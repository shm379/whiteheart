@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                   پیک ها
                    <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                        +
                    </a>
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                         class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                    </button>
                                    <h4 class="modal-title">ساخت پیک</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                                          action="{{route('admin.dashboard.couriers.store')}}" enctype="multipart/form-data" novalidate="novalidate">
                                        @csrf
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">نام</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="name"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">کد ملی</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="national_number"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">موبایل</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="mobile"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">تصویر</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="avatar"
                                                       type="file">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">رمز عبور</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="password"
                                                       type="text"  value="{{rand(12345678,99999999)}}" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-lg-4">
                                                <button class="btn btn-danger" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th class="hidden-phone">نام</th>
                        <th class="hidden-phone">کد ملی</th>
                        <th class="hidden-phone">موبایل</th>
                        <th class="hidden-phone">تصویر</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($couriers)
                        @foreach($couriers as $courier)
                            <tr class="gradeX odd">
                                <td class=" "><a
                                            href="{{route('admin.dashboard.couriers.show',$courier->id)}}">{{$courier->name}}</a>
                                </td>
                                <td class="center hidden-phone ">{{$courier->national_number}}</td>
                                <td class="center hidden-phone ">{{$courier->mobile}}</td>
                                <td class="center hidden-phone ">{{$courier->avatar}}</td>
                                <td class="center hidden-phone ">{{$courier->created_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
