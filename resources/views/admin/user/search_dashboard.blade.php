@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-3">
            <section class="panel">
                <header class="panel-heading">
                    جستجو
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="GET" action="">
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 control-label">ایدی</label>
                            <div class="col-lg-10">
                                <input name="id" type="text" class="form-control" placeholder="ایدی">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 control-label">نام</label>
                            <div class="col-lg-10">
                                <input name="name" type="text" class="form-control" placeholder="نام">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword1" class="col-lg-2 control-label">نام خانوادگی</label>
                            <div class="col-lg-10">
                                <input name="family" type="text" class="form-control" placeholder="نام خانوادگی">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword1" class="col-lg-2 control-label">موبایل</label>
                            <div class="col-lg-10">
                                <input name="mobile" type="tel" class="form-control" placeholder="موبایل">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-success">جستجو</button>
                            </div>
                        </div>
                    </form>
                </div>

            </section>
        </div>
        <div class="col-lg-9">
            <section class="panel">
                <header class="panel-heading">
                    نتایج
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th class="hidden-phone">نام و نام خانوادگی</th>
                        <th class="hidden-phone">موبایل</th>
                        <th class="hidden-phone">تاریخ عضویت</th>
                        <th class="hidden-phone">تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach($users as $user)
                            <tr class="gradeX odd">
                                <td class=""><a href="{{route('admin.dashboard.users.show',[$user->id])}}">{{$user->id}}</a></td>
                                <td class=""><a href="{{route('admin.dashboard.users.show',[$user->id])}}">{{$user->name}} {{$user->family}}</a></td>
                                <td class="center hidden-phone "><a href="{{route('admin.dashboard.users.show',[$user->id])}}">{{$user->mobile}}</a></td>
                               <td class="center hidden-phone ">{{$user->created_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$user->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
