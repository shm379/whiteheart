<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Courier;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $couriers = Courier::all();
        return view('admin.courier.index_dashboard', ['couriers' => $couriers]);
    }
//
//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//        return view('admin.courier.create');
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $courier = new Courier($request->all());
        $courier->save() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.couriers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $courier = Courier::query()->with('image')->findOrFail($id);
            return view('admin.courier.show_dashboard', ['courier' => $courier]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $courier = Courier::findOrFail($id);
            return view('admin.courier.edit_dashboard', ['courier' => $courier]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $courier = Courier::findOrFail($id);
            if ($courier->update($request->all()))
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.couriers.edit', $courier->id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $courier = Courier::findOrFail($id);
            $courier->delete() ? flash()->success('کد تخفیف با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.couriers.index');
        }
    }

}
