@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-info col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    ویرایش سوال
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.questions.update',[ $question->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-1">عنوان </label>
                                <div class="col-lg-11">
                                    <input class=" form-control" id="cname" name="title" minlength="2" type="text"
                                           required="" value="{{$question->title}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-1">توضیحات </label>
                                <div class="col-lg-11">
                                    <textarea class="form-control" name="body" cols="60"
                                              rows="5">{{$question->body}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.questions.destroy',[ $question->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </aside>
    </div>
@endsection
