@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                   ایجاد شعبه جدید
                    <div class="row">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">ساخت شعبه</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                                          action="{{route('admin.dashboard.branches.store')}}" enctype="multipart/form-data" novalidate="novalidate">
                                        @csrf
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">نام</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="name"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">تلفن ثابت</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="phone"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">موبایل</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="mobile"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">آدرس</label>
                                            <div class="col-lg-12">
                                                <textarea style="background: #000" class="form-control" name="address"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">رمز عبور</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="password"
                                                       type="text"  value="{{rand(12345678,99999999)}}" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-lg-4">
                                                <button class="btn btn-danger" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

            </section>
        </div>
    </div>
@endsection
