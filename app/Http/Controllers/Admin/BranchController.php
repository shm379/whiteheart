<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Product;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::all();
        return view('admin.branch.index_dashboard', ['branches' => $branches]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.branch.create_dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $branch = new Branch($request->all());
        $branch->save() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.branches.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $branch = Branch::query()->findOrFail($id);
            return view('admin.branch.show_dashboard', ['branch' => $branch]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $branch = Branch::findOrFail($id);
            return view('admin.branch.edit_dashboard', ['branch' => $branch]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $branch = Branch::findOrFail($id);
            if ($branch->update($request->all()))
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.branches.edit', $branch->id);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $branch = Branch::findOrFail($id);
            $branch->delete() ? flash()->success('کد تخفیف با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.branches.index');
        }
    }
}
