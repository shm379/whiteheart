@extends('layouts.theme')
@section('pageTitle')
    معرفی شعب
@endsection
@section('content')

    <div class="back">
        <div class="ciry">
            <ul>
                <li><a class="sdv">تهران</a> </li>

                <li><a class="sdv">اصفهان</a></li>.
                <li><a class="sdv">کرج</a></li>
                <li><a class="sdv">یزد</a></li>


            </ul>
            <div class="shobe">
                <a href="">تحویل در شعبه</a>
                <a href="">  ارسال با پیک</a>
                <i class="far fa-times-circle"></i>


            </div>
        </div>
        <i class="fab fa-instagram"></i>
        <h1>مارادر اینستاگرام <br>
            دنبــــــــــال کنید
        </h1>
    </div>
@endsection

@push('scripts')


    <script src="js/jquery.js"></script>
    <script>
	 
        $(".sdv").click(function () {
            $(".shobe").toggle('fast');
            event.stopPropagation();
        });

        $(".shobe").click(function () {
            event.stopPropagation();
        });
        $(".far.fa-times-circle").click(function () {
            $(".shobe").toggle('fast');
            event.stopPropagation();
        });

        $(".shobe").click(function () {
            event.stopPropagation();
        });

     
    </script>

@endpush
