@if(session()->has('flash_message'))
    <script>
        Swal.fire({
            icon: '{{session('flash_message.type')}}',
            title: '{{session('flash_message.title')}}',
            text: '{{session('flash_message.text')}}',
            timer: 2000,
            confirmButtonText: 'باشه!'
        })
    </script>
@endif
