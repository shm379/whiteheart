<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/artisan_cli',[\artisan_cli\gui\Http\Controllers\ArtisanCliController::class,'index'])->middleware('auth:admin');
Route::post('/artisan-cli-runner',[\artisan_cli\gui\Http\Controllers\ArtisanCliController::class,'runCli'])->middleware('auth:admin');

use Illuminate\Support\Facades\Route;
// Route::middleware('verify.mobile')->group(function (){

    Route::get('/','HomeController@index')->name('home');
    Route::get('/verify-mobile/{id}','AuthController@verifyMobile')->name('verifyMobile');
    Route::post('/verify-mobile/{id}','AuthController@verifyMobileProcess')->name('verifyMobileProcess');
    Route::get('/stores','BranchController@index')->name('branches.index');
    Route::get('/store/{slug}','BranchController@show')->name('branches.show');
    Route::get('/branch-request','HomeController@branchRequest')->name('branch.request');
    Route::get('/cooperation-request','HomeController@cooperationRequest')->name('cooperation.request');
	Route::get('/select-branch','HomeController@selectBranch')->name('select.branch');
	Route::post('/select-branch','HomeController@selectBranchPost')->name('select.branchPost');
    Route::get('/about-branches','HomeController@aboutBranches')->name('about.branches');
    Route::get('/support','HomeController@support')->name('support');
    Route::get('/map','HomeController@map')->name('map');
    Route::get('/about-us','HomeController@aboutUs')->name('about_us');
    Route::get('/contact-us','HomeController@contactUs')->name('contact_us');
    Route::get('/cart','CartController@index')->name('cart');
    Route::get('/cart/{product_id}_{branch_id}/add','CartController@add')->name('cart.add');
    Route::get('/cart/{product_id}/remove','CartController@remove')->name('cart.remove');
    Route::post('/cart/empty','CartController@empty')->name('cart.empty');
    Route::get('/checkout','CheckoutController@checkout')->name('checkout');
    Route::post('/checkout/pay','CheckoutController@pay')->name('checkout.pay');
    Route::get('/thank-you','CheckoutController@thank_you')->name('checkout.after_pay');
// });

Route::any('paymentCallback', 'TransactionController@callback');
Route::get('/images/{id}', 'ImageController@show')->name('images.show');
Route::get('/storage/videos/{url}', 'ProductController@showVideo')->name('video.show');
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/',function (){
        if(\Illuminate\Support\Facades\Auth::guard('admin')->check())
        {
            return redirect()->route('admin.dashboard.main');
        }
        else
        {
            return redirect()->route('admin.login.');
        }
    });
    Route::group(['prefix' => 'login', 'as' => 'login.'], function () {
        Route::get('/', 'Admin\LoginController@form')->name('form');
        Route::post('/', 'Admin\LoginController@authenticate');
    });
    Route::get('/logout', 'Admin\DashboardController@logout')->name('logout');
    Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => 'auth:admin'], function () {
        Route::get('/', 'Admin\DashboardController@mainDashboard')->name('main');
        Route::get('/users/search', 'Admin\UserController@search')->name('users.search');
        Route::resource('/users', 'Admin\UserController')->middleware('permission:users');
        Route::resource('/products', 'Admin\ProductController')->middleware('permission:products');
        Route::post('/products/{id}/addBranch', 'Admin\ProductController@addBranch')->middleware('permission:products')->name('products.addBranch');
        Route::post('/products/{id}/removeBranch', 'Admin\ProductController@removeBranch')->middleware('permission:products')->name('products.removeBranch');
        Route::resource('/invoices', 'Admin\InvoiceController')->middleware('permission:invoices');
        Route::resource('/admins', 'Admin\AdminController')->middleware('permission:admins');
        Route::resource('/branches', 'Admin\BranchController')->middleware('permission:admins');
        Route::resource('/categories', 'Admin\CategoryController')->middleware('permission:categories');
        Route::resource('/subcategories', 'Admin\SubCategoryController')->middleware('permission:categories');
        // Route::resource('/options', 'Admin\OptionController')->middleware('permission:settings');
        Route::resource('/discounts', 'Admin\DiscountController')->middleware('permission:settings');
        Route::resource('/deliveries', 'Admin\DeliveryController')->middleware('permission:settings');
        Route::resource('/couriers', 'Admin\CourierController')->middleware('permission:settings');
        // Route::resource('/festivals', 'Admin\FestivalController')->middleware('permission:categories');
        Route::resource('/comments', 'Admin\CommentController')->except(['show', 'create', 'store'])->middleware('permission:comments');
        Route::resource('/contactUs', 'Admin\ContactUsController')->middleware('permission:requests');
        Route::resource('/questions', 'Admin\QuestionController')->middleware('permission:questions');
        Route::get('/appInfo', 'Admin\AppInfoController@show')->name('appInfo.show')->middleware('permission:settings');
        Route::get('/appInfo/edit', 'Admin\AppInfoController@edit')->name('appInfo.edit')->middleware('permission:settings');
        Route::put('/appInfo', 'Admin\AppInfoController@update')->name('appInfo.update')->middleware('permission:settings');
        Route::resource('/logs', 'Admin\LogController')->only(['show', 'index'])->middleware('permission:admins');

    });
});
Route::group(['prefix' => 'courier', 'as' => 'courier.'], function () {
    Route::get('/' , function () {
        if (\Illuminate\Support\Facades\Auth::guard('courier')->check()) {
            return redirect()->route('courier.dashboard.main');
        } else {
            return redirect()->route('courier.login.');
        }
    });
    Route::group(['prefix' => 'login' , 'as' => 'login.','namespace'=>'Courier'] , function () {
        Route::get('/' , 'LoginController@form')->name('form');
        Route::post('/' , 'LoginController@authenticate');
    });
    Route::get('/logout' , 'Courier\DashboardController@logout')->name('logout');

    Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => 'auth:courier','namespace'=>'Courier'], function () {
        Route::get('/', 'DashboardController@mainDashboard')->name('main');
        Route::resource('/invoices', 'InvoiceController');
    });
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['auth:admin']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

//Auth Normal
//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
