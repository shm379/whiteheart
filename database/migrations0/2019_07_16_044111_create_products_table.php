<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('subcategory_id');
            //$table->unsignedBigInteger('variant_id');
            $table->string('title', 30);
            $table->string('slug', 50)->unique();
            $table->boolean('status')->default(0);
            $table->text('description');
            $table->unsignedBigInteger('price');
            $table->unsignedTinyInteger('discount')->default(0);
            $table->unsignedSmallInteger('priority')->default(0);
            $table->unsignedInteger('quantity')->default(0);
            $table->boolean('suggested')->default(false);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('subcategory_id')->references('id')->on('subcategories')->onDelete('cascade');
            //$table->foreign('variant_id')->references('id')->on('variants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
