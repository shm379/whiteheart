<?php

namespace App\Http\Controllers\Admin;

use App\Models\Log;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $logs = Log::all();
        return view('admin.log.index_dashboard', ['logs' => $logs]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        try {
            $log = Log::findOrFail($id);
            return view('admin.log.show_dashboard', ['log' => $log]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

}
