@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فرم ثبت ادمین جدید
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.admins.store')}}" novalidate="novalidate">
                            @csrf
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام خانوادگی (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="family" minlength="2" type="text"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام کاربری (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="username" minlength="2" type="text"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">کلمه عبور (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="password" minlength="4"
                                           type="password"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
