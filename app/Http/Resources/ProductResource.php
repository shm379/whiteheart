<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'isFavorite' => auth('api')->check() ? !!$this->followers()->where('id', auth('api')->id())->count() : false,
            'details' => $this->whenLoaded('branches',function () {
                $prices = [];
                foreach ($this->branches()->get() as $key => $branch) {
                    array_push($prices,[
                        'price' => $this->branches()->get()->flatten()[$key]->pivot->price,
                        'quantity' => $this->branches()->get()->flatten()[$key]->pivot->quantity,
                        'status' => $this->branches()->get()->flatten()[$key]->pivot->status ? true : false,
                        'product_id' => $this->branches()->get()->flatten()[$key]->pivot->product_id,
                        'branch_id' => $this->branches()->get()->flatten()[$key ]->pivot->branch_id
                    ]);
                }
                return $prices;
            }),
            'description' => $this->description,
            'discount' => $this->discount,
            'inStock' => $this->inStock > 0 ? true : false,
            'quantity' => $this->quantity,
            'subcategory' => SubCategoryResource::make($this->whenLoaded('subcategory')),
            'options' => OptionResource::collection($this->whenLoaded('options')),
            'items' => ItemResource::collection($this->whenLoaded('items')),
            'images' => ImageResource::collection($this->whenLoaded('images')),
            'inInvoice' => $this->whenPivotLoaded('invoice_product', function () {
                return $this->pivot->count;
            }),
            'branches' => $this->whenLoaded('branches'),
            'rate' => round($this->avgRate, 1),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
