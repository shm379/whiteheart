<?php
/**
 * Created by PhpStorm.
 * User: masih
 * Date: 8/13/19
 * Time: 11:54 AM
 */

namespace App\Traits;

use App\Models\Activity;

namespace App\Traits;


use Carbon\Carbon;

trait ActivityLogger
{

    public function logCreatedActivity($logModel, $causer, $title, $description = "")
    {
        $activity = activity($title)
            ->causedBy($causer)
            ->performedOn($logModel)
            ->log($description);

        return true;
    }

    public function logUpdatedActivity($list, $before, $list_changes, $causer, $title, $description = "")
    {
        unset($list_changes['updated_at']);
        $old_keys = [];
        $old_value_array = [];
        if (empty($list_changes)) {
            $changes = 'No attribute changed';

        } else {

            if (count($before) > 0) {

                foreach ($before as $key => $original) {
                    if (array_key_exists($key, $list_changes)) {

                        $old_keys[$key] = $original;
                    }
                }
            }
            $old_value_array = $old_keys;
            $changes = 'Updated with attributes ' . implode(', ', array_keys($old_keys)) . ' with ' . implode(', ', array_values($old_keys)) . ' to ' . implode(', ', array_values($list_changes));
        }

        $properties = [
            'attributes' => $list_changes,
            'old' => $old_value_array
        ];

        $activity = activity($title)
            ->causedBy($causer)
            ->performedOn($list)
            ->withProperties($properties)
            ->log($changes . ' made by ' . $causer->name . ' ' . $causer->family . "\n Note: " . $description);
        return true;
    }

    public function logDeletedActivity($model, $causer, $title, $description = "")
    {
        $attributes = $this->unsetAttributes($model);

        $properties = [
            'attributes' => $attributes->toArray()
        ];

        $activity = activity($title)
            ->causedBy($causer)
            ->performedOn($model)
            ->withProperties($properties)
            ->log($description);

        return true;
    }

    public function logLoginDetails($user, $causer, $title, $description)
    {
        $updated_at = Carbon::now()->format('d/m/Y H:i:s');
        $properties = [
            'attributes' => ['name' => $user->name, 'family' => $user->family, 'description' => 'Login into the system by ' . $updated_at]
        ];

        $changes = 'User ' . $user->username . " logged in into the system \n Note: $description";
        $activity = activity($title)
            ->causedBy($causer)
            ->performedOn($user)
            ->withProperties($properties)
            ->log($changes);

        return true;
    }

    public function unsetAttributes($model)
    {
        unset($model->created_at);
        unset($model->updated_at);
        return $model;
    }
}
