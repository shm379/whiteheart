<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\UserAddressRequest;
use App\Http\Resources\UserAddressCollection;
use App\Http\Resources\UserAddressResource;
use App\Models\UserAddress;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class UserAddressController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy', 'index']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return UserAddressCollection
     */
    public function index()
    {
        return new UserAddressCollection(auth('api')->user()->addresses);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param UserAddressRequest $request
     * @return UserAddressResource
     */
    public function store(UserAddressRequest $request)
    {
        $user = auth('api')->user();
        $address = new UserAddress($request->all());
        return $user->addresses()->save($address) ? new UserAddressResource($address) : falseJsonResponse(__('message.api.somethingWasWrong'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return UserAddressResource
     */
    public function show($id)
    {
        try {
            $address = UserAddress::where('user_id', auth('api')->id())->findOrFail($id);
            return new UserAddressResource($address);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.addressNotFound'), 401, null, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserAddressRequest $request
     * @param int $id
     * @return UserAddressResource
     */
    public function update(UserAddressRequest $request, $id)
    {
        try {
            $address = UserAddress::where('user_id', auth('api')->id())->findOrFail($id);
            return $address->update($request->all()) ? new UserAddressResource($address) : falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.addressNotFound'), 401, null, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $address = UserAddress::where('user_id', auth('api')->id())->findOrFail($id);
            return $address->delete() ? trueJsonResponse(__('message.api.addressDeletedSuccessfully'), 201) : falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.addressNotFound'), 401, null, 404);
        }
    }
}
