<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\App;
use Kavenegar\KavenegarApi;
use Kavenegar\Laravel\Channel\KavenegarChannel;
use Kavenegar\Laravel\Message\KavenegarMessage;
use Kavenegar\Laravel\Notification\KavenegarBaseNotification;

class SendVerifyCode extends KavenegarBaseNotification
{
    use Queueable;
    public $code;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
//
//    public function via($notifiable)
//    {
//        return [\App\Broadcasting\KavenegarChannel::class];
//    }
//
//    public function via($notifiable)
//    {
//        return ['database'];
//    }
//
//    public function toArray($notifiable){
//        return [
//            'code' => $notifiable->generateVerifyingCode()
//        ];
//    }
    public function toKavenegar($notifiable)
    {
        $code = $notifiable->generateVerifyingCode();

        return (new KavenegarMessage())
            ->verifyLookup('verifyrestaurant',$code);
    }
}
