<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="{{isActive(['courier.dashboard.main'])}}">
                <a class="" href="{{route('courier.dashboard.main')}}">
                    <i class="icon-dashboard"></i>
                    <span>داشبورد</span>
                </a>
            </li>

                <li class="{{isActive(['courier.dashboard.invoices.index'])}}">
                    <a class="" href="{{route('courier.dashboard.invoices.index')}}">
                        <i class="icon-shopping-cart"></i>
                        <span>سفارشات</span>
                    </a>
                </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
