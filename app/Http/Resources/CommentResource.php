<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'text' => $this->text,
            'isBuyer' => !!$this->is_buyer,
            'approved' => !!$this->approved,
            'user' => $this->whenLoaded('user') ? ['name' => $this->user->name, 'family' => $this->user->family] : null,
            'product' => ProductResource::make($this->whenLoaded('product')),
            'createdAt' => $this->created_at,
        ];
    }
}
