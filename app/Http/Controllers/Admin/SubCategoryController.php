<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Image;
use App\Models\Option;
use App\Models\Subcategory;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $subcategories = Subcategory::all();
        $categories = Category::all();
        return view('admin.subcategory.index_dashboard', ['subcategories' => $subcategories, 'categories' => $categories]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $category = Category::findOrFail($request->input('categoryId'));
        $subcategory = new Subcategory($request->all());
        $category->subcategories()->save($subcategory) ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.subcategories.edit', [ $subcategory->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $subcategory = Subcategory::with([ 'products'])->findOrFail($id);
            return view('admin.subcategory.show_dashboard', ['subcategory' => $subcategory]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $subcategory = Subcategory::with([ 'image'])->findOrFail($id);
            return view('admin.subcategory.edit_dashboard', ['subcategory' => $subcategory]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $subcategory = Subcategory::findOrFail($id);
            if ($request->input('addImage')) {
                $subcategory->attacheUploadedImage($request->file('image'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('deleteImage')) {
                $image = Image::findOrFail($request->input('image'));
                $image->delete() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            } elseif ($request->input('addOption')) {
                $subcategory->options()->attach($request->input('option'));
            } elseif ($request->input('deleteOption')) {
                $subcategory->options()->detach($request->input('option'));

            } else {
                $subcategory->update($request->all());
            }
            flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.subcategories.edit', [ $id]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $category = Subcategory::findOrFail($id);
            $category->delete() ? flash()->success('دسته با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.subcategories.index');
        }
    }
}
