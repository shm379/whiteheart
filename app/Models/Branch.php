<?php

namespace App\Models;

use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Branch extends Authenticatable
{
    use TehranTime, Notifiable;

//    protected $with = ['products'];
    protected $guarded = [];
    protected $hidden = ['password'];
    protected $casts = ['created_at','updated_at'];

    const BASE_IMAGE_PATH = '/branches';


    public function setPasswordAttribute ($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }


    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)
            ->withPivot(['price','discount','quantity','status']);
//            ->using(BranchProduct::class);
//            ->as('details');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    public function couriers()
    {
        return $this->hasMany(Courier::class);
    }
}
