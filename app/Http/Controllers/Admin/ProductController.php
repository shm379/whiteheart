<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\ProductCollection;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use App\Models\Subcategory;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $products = Product::with(['category','branches'=>function($b){
            return $b;
        }])->orderByDesc('created_at');
        $products = $request->input('id') ? $products->where('id', $request->input('id')) : $products;
        $products = $request->input('subcategoryId') ? $products->where('subcategory_id', $request->input('subcategoryId')) : $products;
        $products = $request->input('startPrice') ? $products->where('price', '>=', $request->input('startPrice')) : $products;
        $products = $request->input('endPrice') ? $products->where('price', '<=', $request->input('endPrice')) : $products;
        $products = $request->input('startQuantity') ? $products->where('quantity', '>=', $request->input('startQuantity')) : $products;
        $products = $request->input('endQuxantity') ? $products->where('quantity', '<=', $request->input('endQuantity')) : $products;
        $products = $request->input('startDiscount') ? $products->where('discount', '>=', $request->input('startDiscount')) : $products;
        $products = $request->input('endDiscount') ? $products->where('discount', '<=', $request->input('endDiscount')) : $products;
        $products = $request->input('title') ? $products->where('title', 'like', "%{$request->input('title')}%") : $products;
        $products = $request->input('qty') ? $products->limit($request->input('qty')) : $products->limit(100);
        $products = $products->get();
        $subcategories = Subcategory::all();
        return view('admin.product.index_dashboard', ['products' => $products, 'subcategories' => $subcategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $subcategories = Subcategory::all();
        $branches = Branch::all();
        return view('admin.product.create_dashboard', compact(['branches','subcategories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $subcategory = Subcategory::findOrFail($request->input('categoryId'));
        $product = new Product($request->except(['price','quantity']));

        $subcategory->products()->save($product) ;
        if($request->branchIds) {
            foreach ($request->branchIds as $branch) {
                $product->branches()->attach($branch,[
                    'branch_id' => $branch,
                    'product_id' => $product->id,
                    'price' => $request->price,
                    'quantity' => $request->quantity
                ]);
            }
        }
            flash()->success('اطلاعات با موفقیت به روزرسانی شد') ;
            //flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.products.edit', [$product->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $product = Product::with(['subcategory', 'images', 'branches'])->findOrFail($id);
            return view('admin.product.show_dashboard', ['product' => $product]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $subcategories = Subcategory::all();
            $product = Product::with(['subcategory', 'suggestions','branches'])->findOrFail($id);
            $branches = Branch::all();
            $suggestionsNotExist = Product::whereNotIn('id', $product->suggestions->pluck('id')->toArray())->get();
            return view('admin.product.edit_dashboard', ['branches'=>$branches,'product' => $product, 'suggestionsNotExist' => $suggestionsNotExist, 'subcategories' => $subcategories]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $product = Product::findOrFail($id);
            if ($request->input('addImage')) {
                $product->attacheUploadedImage($request->file('image'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('addVideo')) {
                $product->update(['video'=>explode('/',$request->input('video'))[sizeof(explode('/',$request->input('video')))-2].'/'.last(explode('/',$request->input('video')))]);
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('deleteImage')) {
                $image = Image::findOrFail($request->input('image'));
                $image->delete() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            } elseif ($request->input('deleteItem')) {
                $product->items()->detach($request->input('item'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('addItem')) {
                $product->items()->attach($request->input('item'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('deleteSuggestion')) {
                $product->suggestions()->detach($request->input('suggestion'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('addSuggestion')) {
                if ($request->input('suggestion'))
                    $product->suggestions()->attach($request->input('suggestion'));
                elseif ($request->input('auto'))
                    $product->suggestions()->attach(Product::whereNotIn('id', $product->suggestions->pluck('id')->toArray())->inRandomOrder($id)->limit(5)->pluck('id')->toArray());
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } else {
                $product->suggested = !!$request->input('suggested');
                $product->subcategory_id = $request->input('categoryId');
//                if($product->branches()->get())

                $product->branches()->detach();

                foreach ($request->branchIds as $key => $branchId){
//                    $product->branches()->detach($request->branchIds);
                    $product->branches()->attach($branchId,[
                        'status'=>$request->status[$key],
                        'price'=>$request->price[$key],
                        'discount'=>$request->discount[$key],
                        'quantity'=>$request->quantity[$key],
                    ]);
                }

                $product->update($request->except('status','price','discount','quantity','_token')) ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            }
            return redirect()->route('admin.dashboard.products.edit', [$id]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->delete() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('admin.dashboard.products.index');
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('admin.dashboard.products.show', [$id]);
        }
    }


    /*  Add Branch */

    public function addBranch(Request $request,$id)
    {
        $product = Product::query()->with('branches')->find($id);
        $product->branches()->attach($request->branchIds,[
                'status'=>0,
                'price'=>0,
                'discount'=>0,
                'quantity'=>0,
        ]);

        return redirect()->back();

    }
    /*  Remove Branch */

    public function removeBranch(Request $request,$id)
    {
        $product = Product::query()->with('branches')->find($id);
        $product->branches()->detach($request->branchIds);
//        $product->branches()->attach($request->branchIds,[
//                'status'=>0,
//                'price'=>0,
//                'discount'=>0,
//                'quantity'=>0,
//        ]);

        return redirect()->back();
    }

	public function showVideo(Request $request, $url)
	{
		return '11';
	}


}
