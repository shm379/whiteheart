<?php

namespace App\Models;

use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use TehranTime;
    protected $fillable = ['title', 'body'];
}
