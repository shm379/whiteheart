@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-info col-lg-12">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات گزارش
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$log->id}}</p>
                        </div>

                        <div class="bio-row">
                            <p><span>کاربر :</span><a
                                        href="{{$log->causer instanceof \App\Models\Admin?route('admin.dashboard.admins.show',['id'=>$log->causer->id]):route('admin.dashboard.users.show',['id'=>$log->causer->id])}}">{{$log->causer->name}} {{$log->causer->family}}
                                </a></p>
                        </div>
                        <div class="bio-row">
                            <p><span>نوع کاربر </span>:@if($log->causer instanceof \App\Models\Admin)<span class="label label-success">مدیر</span>@else
                                    <span class="label label-danger">کاریر</span>@endif</p>
                        </div>

                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$log->log_name}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>اعمال بر :</span> {{get_class($log->subject)}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {!! str_replace("\n",'<br>',$log->description) !!}</p>
                        </div>

                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$log->created_at->timezone('Asia/Tehran')}}</p>
                        </div>



                    </div>
                </div>
            </section>
        </aside>

    </div>
@endsection
