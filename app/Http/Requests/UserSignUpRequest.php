<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UserSignUpRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'between:2,255'],
            'family' => ['required', 'string', 'between:2,255'],
            'mobile' => ['required', 'numeric'],
            'password' => ['required', 'string','min:6'],
            'email' => 'nullable|email',
        ];
    }

}
