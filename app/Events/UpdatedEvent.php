<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UpdatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Model
     */
    protected $model;
    /**
     * @var Model
     */
    protected $causer;

    /**
     * @var array
     */
    protected $before;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * Create a new event instance.
     *
     * @param Model $model
     * @param array $before
     * @param Model $causer
     * @param string $title
     * @param string $description
     */
    public function __construct(Model $model, array $before, Model $causer, string $title = "default", string $description = "")
    {
        $this->model = $model;
        $this->before = $before;
        $this->causer = $causer;
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @return Model
     */
    public function getCauser(): Model
    {
        return $this->causer;
    }

    /**
     * @return array
     */
    public function getBefore(): array
    {
        return $this->before;
    }
}
