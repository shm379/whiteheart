@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-info col-lg-12">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات دسته
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$request->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام:</span> {{$request->name}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>موبایل:</span> {{$request->mobile}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>ایمیل:</span> {{$request->email}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>پیام :</span> {{$request->message}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$request->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$request->updated_at_tehran}}</p>
                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>
    <aside class="profile-info col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                ویرایش درخواست
            </header>
            <div class="panel-body">
                <div class=" form">
                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                          action="{{route('admin.dashboard.contactUs.update',['id' => $request->id])}}"
                          novalidate="novalidate">
                        @csrf
                        @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">وضعیت</label>
                                <div class="col-lg-10">
                                    <select name="status" id="minbeds" class="form-control bound-s">
                                        <option value="1" @if($request->status)selected @endif><span
                                                class="label label-danger">بررسی شده</span></option>
                                        <option value="0" @if(!$request->status)selected @endif><span
                                                class="label label-success">بررسی نشده</span></option>
                                    </select>
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-success" type="submit">ذخیره</button>
                                <button class="btn btn-default" type="button">انصراف</button>
                            </div>
                        </div>
                    </form>
                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                          action="{{route('admin.dashboard.contactUs.destroy',['id' => $request->id])}}"
                          novalidate="novalidate">
                        @csrf
                        @method('DELETE')
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </aside>

    </div>
@endsection
