@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="/img/profile-avatar.jpg" alt="">
                    </a>
                    <h1>{{$festival->title}}</h1>
                    <p>{{$festival->description}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.festivals.show',['id' => $festival->id])}}">
                            <i
                                class="icon-user"></i> دسته</a></li>
                    <li><a href="{{route('admin.dashboard.festivals.edit',['id' => $festival->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش دسته</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات دسته
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$festival->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$festival->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$festival->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>محصولات :</span>
                              {{$festival->pistachios->count()}}
                            </p></div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$festival->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$festival->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    محصولات ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>عنوان</th>
                        <th>گروه</th>
                        <th>تعداد</th>
                        <th>قیمت</th>
                        <th>تخفیف</th>
                        <th>وضعیت</th>
                        <th>توضیحات</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($festival->pistachios)
                        @foreach($festival->pistachios as $pistachio)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.pistachios.show',['id'=>$pistachio->id])}}">{{$pistachio->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.pistachios.show',['id'=>$pistachio->id])}}">{{$pistachio->title}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.subcategories.show',['id'=>$pistachio->subcategory->id])}}">{{$pistachio->subcategory->title}}</a>
                                </td>
                                <td>{{$pistachio->quantity}}</td>
                                <td>{{$pistachio->price}}</td>
                                <td>{{$pistachio->discount}}</td>
                                <td>{{$pistachio->status?'فعال':'غیر فعال'}}</td>
                                <td>{{$pistachio->description}}</td>
                                <td>{{$pistachio->created_at_tehran}}</td>
                                <td>{{$pistachio->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    </div>
@endsection
