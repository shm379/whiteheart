<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    public function alternatives()
    {
        return $this->hasMany(Alternative::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
