<?php

namespace App\Http\Controllers\Courier;

use App\Events\Banner\InvoiceDeleted;
use App\Events\Banner\InvoiceUpdated;
use App\Models\Delivery;
use App\Models\Invoice;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $invoices = Invoice::with(['user', 'courier','delivery'])->orderByDesc('created_at')->where('courier_id',Auth::guard('courier')->id());
        $invoices = $request->input('id') ? $invoices->where('id', $request->input('id')) : $invoices;
        $invoices = $request->input('status') ? $invoices->where('status', $request->input('status')) : $invoices;
        $invoices = $request->input('startPrice') ? $invoices->where('total_price', '>=', $request->input('startPrice')) : $invoices;
        $invoices = $request->input('endPrice') ? $invoices->where('total_price', '<=', $request->input('endPrice')) : $invoices;
        $invoices = $request->input('delivery') ? $invoices->whereHas('delivery', function (Builder $query) use ($request) {
            $query->where('id', $request->input('delivery'));
        }) : $invoices;
        $invoices = $request->input('user') ? $invoices->whereHas('user', function (Builder $query) use ($request) {
            $query->where('name', 'like', "%{$request->input('user')}%")->orWhere('family', 'like', "%{$request->input('user')}%");
        }) : $invoices;
        $invoices = $request->input('qty') ? $invoices->limit($request->input('qty')) : $invoices->limit(100);
        $invoices = $invoices->get();
        $deliveries = Delivery::all();
        return view('courier.invoice.index_dashboard', ['invoices' => $invoices, 'deliveries' => $deliveries]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $invoice = Invoice::with(['transactions', 'user', 'products', 'delivery'])->findOrFail($id);
            $invoice['googleMapLink'] = "https://maps.google.com/maps/dir//{$invoice->user_address->lat},{$invoice->user_address->lng}/@{$invoice->user_address->lat},{$invoice->user_address->lng},16z?hl=fa";
            return view('courier.invoice.show_dashboard', ['invoice' => $invoice]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $invoice = Invoice::with(['user', 'products', 'delivery'])->findOrFail($id);
            return view('courier.invoice.edit_dashboard', ['invoice' => $invoice]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $invoice = Invoice::findOrFail($id);
            $invoice->status = $request->input('status');
            if ($invoice->status == 1 && $request->input('get_ready_at') >1)
                $invoice->get_ready_at = now()->addMinutes($request->input('get_ready_at'));
            $beforeChange = $invoice->getOriginal();
            $invoice->update() ? event(new InvoiceUpdated($invoice, $beforeChange, auth()->user())) && flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('courier.dashboard.invoices.edit', [$invoice->id]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $invoice = Invoice::findOrFail($id);
            $invoice->delete() ? event(new InvoiceDeleted($invoice, auth()->user())) && flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('courier.dashboard.invoices.index');
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return back();
        }
    }

}
