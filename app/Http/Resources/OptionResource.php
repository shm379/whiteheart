<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OptionResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'isOptional' => !!$this->is_optional,
            'type' => $this->type,
            'items' => ItemResource::collection($this->whenLoaded('items')),
        ];
    }
}
