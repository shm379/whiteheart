@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            @include('admin.courier.profile_nav')
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات پیک
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>کد :</span> {{$courier->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام و نام خانوادگی :</span> {{$courier->name}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کد ملی:</span> {{$courier->national_number}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>موبایل :</span> {{$courier->mobile}}</p>
                        </div>
                        <div class="bio-row">
                        <p><span>تاریخ ثبت :</span> {{$courier->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$courier->updated_at_tehran}}</p>
                        </div>
                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    سفارشات
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>شناسه</th>
                        <th>کاربر</th>
                        <th>قابل پرداخت</th>
                        <th>مبلغ کل</th>
                        <th>وضعیت</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @foreach($courier->invoices as $invoice)
                        <tr class="gradeX odd">
                            <td>
                                <a href="{{route('admin.dashboard.invoices.show',$invoice->id)}}">{{$invoice->id}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.dashboard.users.show',$invoice->user_id)}}">{{$invoice->user->name}}{{$invoice->user->family}}</a>
                            </td>
                            <td>{{$invoice->payable_price}}</td>
                            <td>{{$invoice->total_price}}</td>
                            <td>{{$invoice->status}}</td>
                            <td>{{$invoice->created_at_tehran}}</td>
                            <td>{{$invoice->updated_at_tehran}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    </div>
@endsection
