<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="/img/favicon.html">
    <title>صفحه ورود</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/style-responsive.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <!-- sweet alert-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
</head>
<body class="login-body">
<div class="container">
    <form class="form-signin" action="" method="POST">
        <h2 class="form-signin-heading">ورود به سایت</h2>
        <div class="login-wrap">
            @csrf
            <input type="text" name="mobile" class="form-control" placeholder="نام کاربر" autofocus>
            <input type="password" name="password" class="form-control" textmode="password" placeholder="کلمه عبور">
            <button class="btn btn-lg btn-login btn-block" type="submit">ورود</button>
        </div>
    </form>
</div>
</body>
@include('flash_message')
</html>
