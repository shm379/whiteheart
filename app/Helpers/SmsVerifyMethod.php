<?php
namespace App\Helpers;

use App\Models\User;
use App\Notifications\SendVerifyCode;
use SanjabVerify\Contracts\VerifyMethod;

class SmsVerifyMethod implements VerifyMethod
{
    public function send(string $receiver, string $code)
    {
        $user = User::query()->where('mobile',$receiver)->first();
        // Send code to receiver
        $notify = $user->notify(new SendVerifyCode($user));
        dd($notify);
        if ($notify == 'success') {
            return true; // If code sent successfuly then return true
        }
        return false; // If send code failed return false
    }
}
