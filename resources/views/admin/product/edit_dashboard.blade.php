@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                @if($product->images->count())
                    <div class="container">
                        <h2>عکس ها</h2>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @for($i=0;$i<$product->images->count();$i++)
                                    <li data-target="#myCarousel" data-slide-to="{{$i}}"
                                        class="{{$i==0?'active':''}}"></li>
                                @endfor
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @for($i=0;$i<$product->images->count();$i++)
                                    <div class="item {{$i==0?'active':''}}">
                                        <img src="{{route('images.show',[$product->images[$i]->id])}}"
                                             alt="{{$product->images[$i]->original_name}}" style="width:100%;">
                                    </div>
                                @endfor

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                @endif


                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.products.show',[$product->id])}}">
                            <i
                                class="icon-user"></i> محصول</a></li>
                    <li><a href="{{route('admin.dashboard.products.edit',[ $product->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش محصول</a></li>
                </ul>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    انتخاب ویدیو
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="input-group">
                   <span class="input-group-btn">
                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        انتخاب ویدیو
                     </a>
                   </span>
                   <form enctype="multipart/form-data" action="{{route('admin.dashboard.products.update',[$product->id])}}" method="post">
                       @csrf
                       @method('patch')
                    <input id="thumbnail" class="form-control" type="text" name="video" value="{{$product->video}}">
                       <br>
                     <button name="addVideo" value="true" type="submit" class="btn btn-danger">ذخیره ویدیو</button>
                   </form>
                    </div>

                <img id="holder" style="margin-top:15px;max-height:100px;">
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    عکس
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.products.update',[$product->id])}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عکس</label>
                                <div class="col-lg-8">
                                    <input type="file" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addImage" value="true" class="btn btn-success">افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.products.update',[$product->id])}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عکس ها</label>
                                <div class="col-lg-8">
                                    <select name="image" multiple="" class="form-control">
                                        @foreach($product->images as $image)
                                            <option value="{{$image->id}}">{{$image->original_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteImage" value="true" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <header class="panel-heading">
                    ویرایش محصول
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.products.update',[$product->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-1">عنوان </label>
                                <div class="col-lg-11">
                                    <input class=" form-control" id="cname" name="title" minlength="2" type="text"
                                           required="" value="{{$product->title}}">
                                </div>
                            </div>
                            @foreach($product->branches()->get() as $branch)
                                <div class="row">
                                    <h5 class="col-lg-12">{{$branch->name}}</h5>
                                    <div class="col-lg-3">
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">تعداد </label>
                                            <div class="col-lg-8">
                                                <input class=" form-control" id="cname" name="quantity[]" minlength="2"
                                                       type="text"
                                                       required="" value="{{$branch->pivot->quantity}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-2">قیمت </label>
                                            <div class="col-lg-10">
                                                <input class=" form-control" id="cname" name="price[]" minlength="2"
                                                       type="text"
                                                       required="" value="{{$branch->pivot->price}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-2">تخفیف </label>
                                            <div class="col-lg-10">
                                                <input class=" form-control" id="cname" name="discount[]" minlength="2"
                                                       type="text"
                                                       required="" value="{{$branch->pivot->discount}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-2">وضعیت</label>
                                            <div class="col-lg-10">
                                                <select name="status[]" id="minbeds" class="form-control bound-s">
                                                    <option value="0" @if(!$branch->pivot->status)selected @endif><span
                                                            class="label label-danger">غیر فعال</span></option>
                                                    <option value="1" @if(!!$branch->pivot->status)selected @endif><span
                                                            class="label label-success">فعال</span></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-1">توضیحات </label>
                                <div class="col-lg-11">
                                    <textarea class="form-control" name="description" cols="60"
                                              rows="5">{{$product->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group" style="text-align: center;margin: 0 auto;display: block">
                                <a href="#addBranch" data-toggle="modal" class="btn btn-danger">افزودن شعبه</a>
                                <a href="#removeBranch" data-toggle="modal" class="btn btn-danger">حذف شعبه</a>

                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-1">لیست شعب </label>
                                    <div class="col-lg-11">
                                    <select multiple id="branches" name="branchIds[]" class="form-control">
                                    @foreach($branches as $key => $branch)
                                        <option
                                            @foreach($product->branches()->get() as $selectedBranch)
                                            @if($selectedBranch->id==$branch->id)
                                            selected
                                            @endif
                                            @endforeach
                                            value="{{$branch->id}}">{{$branch->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="cname" class="control-label">دسته</label>
                                <select name="categoryId" class="form-control bound-s">
                                    @foreach($subcategories as $subcategory)
                                        <option value="{{$subcategory->id}}"@if($subcategory->id == $product->subcategory_id)selected @endif>{{$subcategory->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="cname" class="control-label">الویت</label>
                                <input class="form-control bound-s" id="cname" name="priority" minlength="2" type="number"
                                           required="" value="{{$product->priority}}">
                            </div>

                            <div class="col-lg-4">
                                <label for="cname" class="control-label">پیشنهاد شده</label>
                                <input type="checkbox" name="suggested" @if($product->suggested) checked @endif>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
{{--                                    <button class="btn btn-default" type="button">انصراف</button>--}}
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.products.destroy',[ $product->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    پیشنهادات
                </header>
                <div class="panel-body">
                    <div class="panel-body bio-graph-info">
                        <form class="form-horizontal" role="form"
                              action="{{route('admin.dashboard.products.update',[$product->id])}}"
                              method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <select name="suggestion" multiple class="form-control" >
                                            @isset($suggestionsNotExist)
                                                @foreach($suggestionsNotExist as $suggestion)
                                                    <option value="{{$suggestion->id}}">{{$suggestion->title}}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <div class="col-lg-1">

                                        <div class="form-group">
                                            <div class=" col-lg-12">
                                                <button class="btn btn-success" name="addSuggestion" value="true"
                                                        type="submit">
                                                    اضافه
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <button class="btn btn-danger" name="deleteSuggestion" value="true"
                                                        type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                                    حذف
                                                </button>
                                            </div>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="auto" value="true" type="checkbox"> خودکار
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <select name="suggestion" multiple="" class="form-control">
                                            @isset($product->suggestions)
                                                @foreach($product->suggestions as $suggestion)
                                                    <option value="{{$suggestion->id}}">{{$suggestion->title}}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>


                                </div>
                            </div>
                        </form>
                        <div aria-hidden="true" aria-labelledby="addBranchLabel" role="dialog" tabindex="-1" id="addBranch"
                             class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                        </button>
                                        <h4 class="modal-title">افزودن شعبه به محصول</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="branchProduct-form" id="branchProductForm" method="POST" action="{{route('admin.dashboard.products.addBranch',$product->id)}}">
                                            @csrf
                                            <select multiple id="branches" name="branchIds[]" class="form-control">
                                                @foreach($branches as $key => $branch)
                                                    <option
                                                        @foreach($product->branches()->get() as $selectedBranch)
                                                        @if($selectedBranch->id==$branch->id)
                                                        selected
                                                        disabled
                                                        style="display: none"
                                                        @endif
                                                        @endforeach
                                                        value="{{$branch->id}}">{{$branch->name}}</option>
                                                @endforeach
                                            </select>
                                            <input type="submit" class="btn btn-success" value="ثبت">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div aria-hidden="true" aria-labelledby="removeBranchLabel" role="dialog" tabindex="-1" id="removeBranch"
                             class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                        </button>
                                        <h4 class="modal-title">حذف شعبه از محصول</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="branchProduct-form" id="branchProductForm" method="POST" action="{{route('admin.dashboard.products.removeBranch',$product->id)}}">
                                            @csrf
                                            <select multiple id="branches" name="branchIds[]" class="form-control">
                                                @foreach($branches as $key => $branch)
                                                    <option
                                                        @foreach($product->branches()->get() as $selectedBranch)
                                                        @if($selectedBranch->id==$branch->id)
                                                        selected
                                                        style="display: block"
                                                        @endif
                                                        @endforeach
                                                        value="{{$branch->id}}" style="display: none">{{$branch->name}} </option>
                                                @endforeach
                                            </select>
                                            <input type="submit" class="btn btn-success" value="حذف">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </aside>
    </div>
@endsection
@push('scripts')
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        let $ = jQuery;
        $('#lfm').filemanager('video');
        $('#branches').on('change',function (){
           console.log(this)
           console.log( $('#branches'))
        });
    </script>
@endpush
