<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $comments = Comment::with(['user', 'pistachio'])->orderByDesc('created_at');
        $comments = $request->input('id') ? $comments->where('id', $request->input('id')) : $comments;
        $comments = $request->input('status') ? $comments->where('approved', $request->input('status')) : $comments;
        $comments = $request->input('text') ? $comments->where('text', 'like', "%{$request->input('text')}%") : $comments;
        $comments = $request->input('minRate') ? $comments->where('rate', '>=', $request->input('minRate')) : $comments;
        $comments = $request->input('maxRate') ? $comments->where('rate', '<=', $request->input('maxRate')) : $comments;
        $comments = $request->input('user') ? $comments->whereHas('user', function (Builder $query) use ($request) {
            $query->where('name', 'like', "%{$request->input('user')}%")->orWhere('family', 'like', "%{$request->input('user')}%");
        }) : $comments;
        $comments = $request->input('qty') ? $comments->limit($request->input('qty')) : $comments->limit(100);
        $comments = $comments->get();
        return view('admin.comment.index_dashboard', ['comments' => $comments]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

        try {
            $comment = Comment::with(['user', 'pistachio'])->findOrFail($id);
            return view('admin.comment.edit_dashboard', ['comment' => $comment]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
            try {
                $comment = Comment::findOrFail($id);
                $comment->approved = $request->input('status');
                $comment->update() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
                return view('admin.comment.edit_dashboard', ['comment' => $comment]);
            } catch (ModelNotFoundException $exception) {
                return abort(404);
            } catch (Exception $exception) {
                flash()->error('اطلاعات وارد شده صحیح نمی باشد');
                return back();
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $comment = Comment::findOrFail($id);
            $comment->delete() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('admin.dashboard.comments.index');
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return back();
        }
    }
}
