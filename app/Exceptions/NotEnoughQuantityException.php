<?php

namespace App\Exceptions;

use App\Models\Product;
use Exception;
use Throwable;

class NotEnoughQuantityException extends Exception
{
    public $product;

    public function __construct(Product $product, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->product = $product;
        parent::__construct($message, $code, $previous);
    }
}
