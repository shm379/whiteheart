<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FalseTextResource extends BaseResource
{
    public function __construct(string $message="", $code = 400, $resource = null)
    {
        $this->with['message'] = $message;
        $this->with['code'] = $code;
        $this->with['ok'] = false;
        parent::__construct($resource);
    }
}
