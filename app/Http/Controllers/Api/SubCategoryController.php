<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\SubCategoryCollection;
use App\Models\Subcategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return SubCategoryCollection
     */
    public function index()
    {
        return new SubCategoryCollection(Subcategory::with(['image'])->get());
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return SubCategoryResource
     */
    public function show($id)
    {
        try {
            $category = Subcategory::with(['options.items', 'pistachios' => function ($query) {
                $query->where('status', '>', '0')->with('images');
            }, 'image'])
                ->findByIdOrSlugOrFail($id);
            return new SubCategoryResource($category);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.categoryNotFound'), 401);
        }
    }

}
