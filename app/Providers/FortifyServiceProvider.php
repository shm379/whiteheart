<?php

namespace App\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Models\User;
use App\Notifications\SendVerifyCode;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Contracts\LoginResponse;
use Laravel\Fortify\Contracts\TwoFactorLoginResponse;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Http\Requests\LoginRequest;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->loginResponseBindings();
    }

    /**
     * Login the response bindings.
     *
     * @return void
     */
    protected function loginResponseBindings()
    {
        $this->app->singleton(LoginResponse::class, \App\Http\Responses\LoginResponse::class);

    }

    /**
     * Register the response bindings.
     *
     * @return void
     */
    protected function registerResponseBindings()
    {
        $this->app->singleton(LoginResponseContract::class, LoginResponse::class);
        $this->app->singleton(TwoFactorLoginResponse::class, TwoFactorLoginResponse::class);
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Fortify::authenticateUsing(function (LoginRequest $request) {

                if (session('login-step') == null) {
                    $user = \App\Models\User::where('mobile', $request->mobile)->first();
                    $generatedCode = Cache::get("MobileVerifyCode|{$request->mobile}");
                    if (!$user) {
                        Validator::make($request->toArray(), [
//                            'name' => ['required', 'string', 'max:255'],
//                            'family' => ['required', 'string', 'max:255'],
                            'mobile' => ['required', 'string', 'regex:/^09(1[0-9]|9[0-2]|2[0-2]|0[1-5]|41|3[0,3,5-9])\d{7}$/', 'max:255', 'unique:users'],
//                            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//                            'password' => ['required', 'string', 'min:8', 'confirmed'],
                        ]);
                        $user = User::query()->create([
                                'name' => ' ',
                                'family' => ' ',
                                'mobile' => $request->mobile,
//                                'email' => $data['email'],
                                'password' => Hash::make($request->mobile.'@Ba0987654321Ab@'),
                        ]);
                    }
                    $user->notify(new SendVerifyCode($user));
                    Session::put('login-step', 2);
                    Session::put('user_login_id', $user->id);

                    return redirect()->route('login');
                }

                if(session('login-step')==2){
                    $user = \App\Models\User::find(Session::get('user_login_id'));
                    $generatedCode = Cache::get("MobileVerifyCode|{$user->mobile}");
                    if ($user && isset($generatedCode) && $request->code == $generatedCode)
                        $user->verifyMobile();
                        Session::pull('login-step');
                        Session::pull('user_login_id');
                        return $user;
                }
                    return redirect()->route('login');
        });
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);
    }
}
