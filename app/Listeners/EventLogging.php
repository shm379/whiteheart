<?php

namespace App\Listeners;

use App\Events\CreatedEvent;
use App\Events\DeletedEvent;
use App\Events\SignedInEvent;
use App\Events\UpdatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventLogging
{
    use \App\Traits\ActivityLogger;
    public $event;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {


    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return bool
     */
    public function handle($event)
    {
        $this->event = $event;

        if ($event instanceof CreatedEvent)
            return $this->createdEvent();
        elseif ($event instanceof UpdatedEvent)
            return $this->updatedEvent();
        elseif ($event instanceof DeletedEvent)
            return $this->deletedEvent();
        elseif ($event instanceof SignedInEvent)
            return $this->signedInEvent();
    }

    public function createdEvent()
    {
        return $this->logCreatedActivity($this->event->getModel(), $this->event->getCauser(), $this->event->getTitle(), $this->event->getDescription());
    }

    public function updatedEvent()
    {
        return $this->logUpdatedActivity($this->event->getModel(), $this->event->getBefore(), $this->event->getModel()->getAttributes(), $this->event->getCauser(), $this->event->getTitle(), $this->event->getDescription());
    }

    public function deletedEvent()
    {
        return $this->logDeletedActivity($this->event->getModel(), $this->event->getCauser(), $this->event->getTitle(), $this->event->getDescription());
    }

    public function signedInEvent()
    {
        return $this->logLoginDetails($this->event->getModel(), $this->event->getModel(), $this->event->getTitle(), $this->event->getDescription());
    }
}
