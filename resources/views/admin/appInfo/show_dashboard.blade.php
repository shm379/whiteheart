@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.appInfo.show')}}">
                            <i class="icon-user"></i> اطلاعات سایت</a></li>
                    <li><a href="{{route('admin.dashboard.appInfo.edit')}}"> <i
                                class="icon-user"></i>
                            ویرایش اطلاعات</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    اطلاعات
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>دفاتر :</span><br>
                                @foreach($info['agencies'] as $agency)
                                    عنوان: {{$agency['title']}}<br>
                                    ادرس: {{$agency['address']['text_fa'].','.$agency['address']['text_en'].','.$agency['address']['latitude'].','.$agency['address']['longitude']}}
                                    <br>
                                    تلفن: @foreach($agency['numbers'] as $number){{$number}}, @endforeach <br><br>
                                @endforeach
                            </p>
                        </div>
                        <div class="bio-row">
                            <p><span>اسلاید اصلی :</span><br>
                                @foreach($info['mainCarousel'] as $item)
                                    متن: {{$item['caption']}}<br>
                                    لینک: <a href="{{$item['link']}}"> {{$item['link']}}</a><br>
                                    عکس: <a href="{{$item['image']}}">{{$item['image']}}</a><br>
                                @endforeach<br>
                            </p>
                        </div>
                        <div class="bio-row">
                            <p><span>شبکه اجتماعی:</span><br>
                                @foreach($info['socialNetworks'] as $item)
                                    عنوان: {{$item['title']}}<br>
                                    لینک: <a href="{{$item['link']}}"> {{$item['link']}}</a><br>
                                    عکس: <a href="{{$item['image']}}"><img src="{{$item['image']}}" width="40 px"></a><br>
                                @endforeach<br>
                            </p>
                        </div>
                        <div class="bio-row">
                            <p><span>ادرس سایت :</span>{{$info['websiteUrl']}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توسعه دهنده :</span><a href="{{$info['developedBy']['link']}}">{{$info['developedBy']['title']}} <img src="{{$info['developedBy']['image']}}"></a> </p>
                        </div>
                        <div class="bio-row">
                            <p><span>ایمیل :</span>{{$info['email']}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
@endsection
