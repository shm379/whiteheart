<?php

namespace App\Models;

use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;

class Courier extends Authenticatable
{
    const BASE_IMAGE_PATH = '/couriers';

    use TehranTime;
    protected $fillable = ['name','national_number','mobile','avatar','password'];

    public function setPasswordAttribute ($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
    public function invoices ()
    {
        return $this->hasMany(Invoice::class);
    }

    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }
    /**
     * @param UploadedFile $image
     * @return false|Model
     */
    public function attacheUploadedImage(UploadedFile $image)
    {
        $image->storeAs(self::BASE_IMAGE_PATH, $image->hashName());
        return $this->image()->save(new Image(['original_name' => $image->getClientOriginalName(), 'hash_name' => $image->hashName()]));
    }

    public function setAvatarAttribute ($value)
    {
        $this->attacheUploadedImage($value);
    }
}
