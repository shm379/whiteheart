<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

abstract class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function allInSnakeStyle()
    {
        $attributes = $this->all();
        $snake = [];
        foreach ($attributes as $key => $value) {
            $snake[Str::snake($key)] = $value;
        }
        return $snake;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $message = __('validation.attributes.invalidForm');
        $response = new JsonResponse(['ok' => 'false',
            'code' => 403,
            'message' => $message,
            'data' => $this->implodeErrorMessageBag($validator->errors()->getMessages())
        ], 400);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    protected function implodeErrorMessageBag($errors)
    {
        $errorBag = [];
        foreach ($errors as $key => $value) {
            $errorBag[$key] = implode("\n", $value);
        }
        return $errorBag;
    }

}

