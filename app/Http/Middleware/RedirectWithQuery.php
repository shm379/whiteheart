<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\URL;
use Spatie\Permission\Exceptions\UnauthorizedException;

class RedirectWithQuery
{
    public function handle($request, Closure $next)
    {

        if (!empty($request->branch_id) && !empty($request->directGet)) {
            return redirect($next($request));
        }
        return $next($request);
    }
}
