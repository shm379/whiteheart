@extends('courier.frame')
@section('custom-style')
    <script src="/js/sparkline-chart.js"></script>
    <script src="/js/easy-pie-chart.js"></script>
@endsection
@section('main-content')
    <div class="row state-overview">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    مشخصات شما
                </header>
                <div class="panel-body">
                    <div class="col-lg-4">
                        <span class="badge badge-danger">نام شما : {{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                        <br/>
                        <span class="badge badge-danger">موبایل شما : {{\Illuminate\Support\Facades\Auth::user()->mobile}}</span>
                    </div>
                    <div class="col-lg-4">
                        <span class="badge badge-danger">کدملی : {{\Illuminate\Support\Facades\Auth::user()->natinal_number}}</span>

                    </div>
                    <div class="col-lg-4">
                        <span class="badge badge-danger">تصویر شما : {{\Illuminate\Support\Facades\Auth::user()->avatar}}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    سفارشات بررسی نشده
                </header>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>کاربر</th>
                        <th>مبلغ کل</th>
                        <th>مبلغ قابل پرداخت</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($newInvoices as $invoice)
                        <tr>
                            <td>
                                <a href="{{route('courier.dashboard.invoices.show',[$invoice->id])}}">{{$invoice->id}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.dashboard.users.show',[$invoice->user->id])}}">{{$invoice->user->name}} {{$invoice->user->family}}</a>
                            </td>
                            <td>{{$invoice->total_price}}</td>
                            <td>{{$invoice->payable_price}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>


@endsection
