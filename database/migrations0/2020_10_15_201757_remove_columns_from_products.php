<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnsFromProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('discount');
            $table->dropColumn('price');
            $table->dropColumn('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->addColumn('boolean','status')->after('slug')->default(0);
            $table->addColumn('bigInteger','price')->unsigned()->after('description');
            $table->addColumn('tinyInteger','discount')->unsigned()->after('price')->default(0);
            $table->addColumn('integer','quantity')->unsigned()->after('priority')->default(0);
        });
    }
}
