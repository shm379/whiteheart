<aside class="profile-nav col-lg-3">
    <section class="panel">
        <div class="user-heading round">
            <h1>{{$user->name}} {{$user->family}}</h1>
            <p>{{$user->mobile}}</p>
        </div>

        <ul class="nav nav-pills nav-stacked">
            <li class="{{isActive(['admin.dashboard.users.show'])}}"><a href="{{route('admin.dashboard.users.show',[ $user->id])}}"> <i
                        class="icon-user"></i> پروفایل</a></li>
            <li class="{{isActive(['admin.dashboard.users.edit'])}}"><a href="{{route('admin.dashboard.users.edit',[$user->id])}}"> <i class="icon-user"></i>
                    ویرایش پروفایل</a></li>
        </ul>
    </section>
</aside>
