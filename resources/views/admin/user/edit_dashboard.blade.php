@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        @include('admin.user.profile_nav')
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات کاربر
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$user->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام و نام خانوادگی :</span> {{$user->name}} {{$user->family}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>شماره موبایل :</span> {{$user->mobile}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>ایمیل :</span> {{$user->email}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$user->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$user->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش کاربر
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.users.update',[ $user->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text"
                                           required="" value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام خانوادگی (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="family" minlength="2" type="text"
                                           required="" value="{{$user->family}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">موبایل (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="mobile" minlength="2" type="number"
                                           required="" value="{{$user->mobile}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">ایمیل (اختیاری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="email" minlength="4" type="email"
                                           value="{{$user->email}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </div>
    </div>
@endsection
