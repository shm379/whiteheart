<section class="panel">
    @if($category->image)
        <div class="container">
            <h2>عکس ها</h2>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{route('images.show',[$category->image->id])}}"
                             alt="{{$category->image->original_name}}" style="width:100%;">
                    </div>

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    @endif


    <ul class="nav nav-pills nav-stacked">
        <li><a href="{{route('admin.dashboard.categories.show',[ $category->id])}}"> <i
                    class="icon-user"></i> دسته</a></li>
        <li class="active"><a href="{{route('admin.dashboard.categories.edit',[ $category->id])}}"> <i
                    class="icon-user"></i>
                ویرایش دسته</a></li>
    </ul>
</section>
<section class="panel">
    <div class="bio-graph-heading">
        عکس
    </div>
    <div class="panel-body bio-graph-info">
        <form class="form-horizontal" role="form" action="{{route('admin.dashboard.categories.update',[$category->id])}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-4 control-label">عکس</label>
                    <div class="col-lg-8">
                        <input type="file" name="image" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" name="addImage" value="true" class="btn btn-success">افزودن</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
    <div class="panel-body bio-graph-info">
        <form class="form-horizontal" role="form" action="{{route('admin.dashboard.categories.update',[$category->id])}}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <div class="row">
                    <label class="col-lg-4 control-label">عکس ها</label>
                    <div class="col-lg-8">
                        <select name="image" multiple="" class="form-control">
                            @if($category->image)
                                <option value="{{$category->image->id}}">{{$category->image->original_name}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-danger" name="deleteImage" value="true" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</section>
