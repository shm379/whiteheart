<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'originalName' => $this->original_name,
            'hashName' => $this->hash_name,
            'link' => route('images.show', ['id' => $this->id]),
        ];
    }
}
