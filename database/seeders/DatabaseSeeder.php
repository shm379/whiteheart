<?php

namespace Database\Seeders;

use Illuminate\Database\Query\Processors\MySqlProcessor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
/*
        INSERT INTO `admins` (`id`, `name`, `family`, `username`, `password`, `created_at`, `updated_at`) VALUES
    (1, 'مدیر', 'سیستم', 'modir_ghalb', '$2y$10$gdJSKkjiUdtWv8AxhxIcee/pRrzh.kAq7bE0Jpeq6F9oIbs0YuALi', '2020-12-09 20:32:06', '2020-12-09 17:02:43');

--
-- Dumping data for table `branches`
    --

                    INSERT INTO `branches` (`id`, `name`, `slug`, `phone`, `mobile`, `address`, `password`, `created_at`, `updated_at`) VALUES
    (1, 'شعبه اصفهان (سعادت آباد)', 'esfahan-saadatabad', '03136610737', '09391727950', 'خیابان سعادت آباد، جنب بانک تجارت', '$2y$10$zQq9jVgC8gAIBXmyyDQkYuvuYgWVYAN72YWkJIdALVHIJt8YBdDJu', '2020-12-09 16:51:07', '2020-12-09 16:51:07'),
(2, 'شعبه اصفهان (مصلی)', 'esfahan-mosala', '03136610737', '09391727950', 'خیابان مصلی،هایپرمی', '$2y$10$bOW4TidO4nMhUQW2/TW2pOOziDgUbdsGFkrLIYXcqQVbTibqj6Rqe', '2020-12-09 16:51:43', '2020-12-09 16:51:43'),
(3, 'شعبه تهران', 'tehran-kaj', '02122066664', '09391727950', 'بالاتر از میدان کاج،خیابان عقبری دوم، نبش بلوار بهزاد شمالی،مقابل بوستان شقایق', '$2y$10$qyljlXeRRafTBD3mY6jRk.JA2uaO9WITXlhsZJIF1DTyXnKbXYABW', '2020-12-09 17:37:12', '2020-12-09 17:37:12');

--
-- Dumping data for table `categories`
    --

                    INSERT INTO `categories` (`id`, `title`, `slug`, `description`, `priority`, `created_at`, `updated_at`) VALUES
    (1, 'فست فود', 'فست-فود', NULL, 0, '2020-12-09 16:56:38', '2020-12-09 16:56:38');

--
-- Dumping data for table `images`
    --

                    INSERT INTO `images` (`id`, `original_name`, `hash_name`, `imageable_id`, `imageable_type`, `created_at`, `updated_at`) VALUES
    (1, 'pack.png', 'XUBkkpoekIxg4yxHASuDDufRwiPL0dYm7iEiP3pr.png', 1, 'App\\Models\\Category', '2020-12-09 16:57:02', '2020-12-09 16:57:02'),
(2, 'pack.png', 'Z1IkJ4UIunaThjOMJmsGTybvtiC4jrxg7LLyEMZm.png', 1, 'App\\Models\\Subcategory', '2020-12-09 17:05:40', '2020-12-09 17:05:40'),
(3, 'pack.png', 'CepB87z9olbOM205B3DuYSwafJolT8jNXi8Jq9PH.png', 1, 'App\\Models\\Product', '2020-12-09 17:09:59', '2020-12-09 17:09:59');


--
-- Dumping data for table `password_resets`
    --

                    INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
    ('shm379@gmail.com', '$2y$10$Wq9vHFc.6lIubJuIzsxqiOgTVqqq7Q8Y0G0fDAze/CJ33x/xwOlj.', '2020-12-07 11:49:47');


--
-- Dumping data for table `subcategories`
    --

                    INSERT INTO `subcategories` (`id`, `category_id`, `title`, `slug`, `description`, `priority`, `created_at`, `updated_at`) VALUES
    (1, 1, 'پیتزا', 'پیتزا', NULL, 0, '2020-12-09 17:04:18', '2020-12-09 17:04:18');

--
-- Dumping data for table `products`
    --

                    INSERT INTO `products` (`id`, `subcategory_id`, `title`, `slug`, `description`, `priority`, `suggested`, `created_at`, `updated_at`, `deleted_at`) VALUES
    (1, 1, 'پیتزا پپرونی', 'پیتزا-پپرونی', 'تست', 0, 0, '2020-12-09 17:07:06', '2020-12-09 17:22:45', NULL),
(2, 1, 'پیتزا ایتالیایی', 'پیتزا-ایتالیایی', 'تست', 0, 0, '2020-12-09 17:17:09', '2020-12-09 17:17:09', NULL),
(3, 1, 'پیتزا ایتالیایی', 'پیتزا-ایتالیایی-1', 'تست', 0, 0, '2020-12-09 17:17:30', '2020-12-09 17:17:30', NULL),
(4, 1, 'پیتزا گوشتی', 'پیتزا-گوشتی', 'پیتزا گوشت و قارچ', 0, 0, '2020-12-09 17:18:05', '2020-12-09 17:18:05', NULL),
(5, 1, 'پیتزا گوشتی', 'پیتزا-گوشتی-1', 'پیتزا گوشت و قارچ', 0, 0, '2020-12-09 17:19:01', '2020-12-09 17:19:01', NULL),
(6, 1, 'پیتزا گوشتی', 'پیتزا-گوشتی-2', 'پیتزا گوشت و قارچ', 0, 0, '2020-12-09 17:20:07', '2020-12-10 06:21:52', NULL);



--
-- Dumping data for table `branch_product`
    --

                    INSERT INTO `branch_product` (`branch_id`, `product_id`, `status`, `price`, `discount`, `quantity`, `created_at`, `updated_at`) VALUES
    (1, 6, 0, 0, 0, 0, NULL, NULL),
(2, 6, 0, 0, 0, 0, NULL, NULL),
(3, 6, 0, 0, 0, 0, NULL, NULL);

--
-- Dumping data for table `roles`
    --

                    INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
    (1, 'superadmin', 'web', '2020-12-07 15:02:28', '2020-12-07 15:02:30');


--
-- Dumping data for table `model_has_roles`
    --

                    INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
    (1, 'App\\Models\\Admin', 1);

--
-- Dumping data for table `sessions`
    --

                    INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
    ('bLPkrJWR2ZA1QMxawDpco2WhfKYMCM0cP15ysH5C', 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'YTo4OntzOjY6Il90b2tlbiI7czo0MDoiMk9pZFA0TUM1VXJjdU5MbHA5NjhrZFNQaTlwZDBMbFZRV2dEc2pQWCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI5OiJodHRwOi8vd2hpdGVoZWFydC50ZXN0L3N0b3JlcyI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRaaEZtMWVFVXg1ZHdkTVNvekNYWGUuNGtWZWxYVTZQcUROWURxMm04ZVMxUFhoZWV5YlB6VyI7czo1MjoibG9naW5fYWRtaW5fNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MTk6InBhc3N3b3JkX2hhc2hfYWRtaW4iO3M6NjA6IiQyeSQxMCRnZEpTS2tqaVVkdFd2OEF4aHhJY2VlL3BScnpoLmtBcTdiRTBKcGVxNkY5b0liczBZdUFMaSI7fQ==', 1607629060);

--
-- Dumping data for table `suggestions`
    --

                    INSERT INTO `suggestions` (`product_id`, `suggested_id`) VALUES
    (1, 1);

--
-- Dumping data for table `users`
    --

                    INSERT INTO `users` (`id`, `name`, `family`, `credit`, `mobile`, `password`, `remember_token`, `current_team_id`, `two_factor_secret`, `two_factor_recovery_codes`, `email`, `email_verified_at`, `mobile_verified_at`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
    (1, 'Hussain', 'Mousavi', 0, '09391727950', '$2y$10$ZhFm1eEUx5dwdMSozCXXe.4kVelXU6PqDNYDq2m8eS1PXheeybPzW', NULL, NULL, NULL, NULL, 'shm379@gmail.com', NULL, NULL, NULL, '2020-12-07 11:26:06', '2020-12-07 11:26:06');*/

                // $this->call(UsersTableSeeder::class);
    }
}
