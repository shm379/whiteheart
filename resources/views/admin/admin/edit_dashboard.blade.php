@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            @include('admin.admin.profile_nav')
            <section class="panel">
                <div class="bio-graph-heading">
                    نقش ها
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.admins.update',[$user->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">نقش ها</label>
                                <div class="col-lg-8">
                                    <select name="item" multiple="" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->name}}-{{$role->guard_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addRole" value="true" class="btn btn-success">افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.admins.update',[$user->id])}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">نقش ها</label>
                                <div class="col-lg-8">
                                    <select name="item" multiple="" class="form-control">
                                        @foreach($user->roles as $role)
                                            <option value="{{$role->id}}">{{$role->name}}-{{$role->guard_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteRole" value="true" type="submit"
                                            onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                        حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>

        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات ادمین
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$user->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام و نام خانوادگی :</span> {{$user->name}} {{$user->family}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نقش :</span> {{$user->roles->pluck('name')->implode(',')}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام کاربری :</span> {{$user->username}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$user->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$user->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش ادمین
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.admins.update',[ $user->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text"
                                           required="" value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام خانوادگی (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="family" minlength="2" type="text"
                                           required="" value="{{$user->family}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">کلمه عبور (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="password" minlength="4"
                                           type="password"
                                           required="" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.admins.destroy',[ $user->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit"
                                            onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                        حذف
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>

        </aside>

    </div>

@endsection
