@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    روش های ارسال
                    <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                        +
                    </a>
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                         class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                    </button>
                                    <h4 class="modal-title">ن</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                                          action="{{route('admin.dashboard.deliveries.store')}}" novalidate="novalidate">
                                        @csrf
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">عنوان</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="title"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">قیمت</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="price"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">توضیحات</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="description"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-lg-4">
                                                <button class="btn btn-danger" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th class="hidden-phone">عنوان</th>
                        <th class="hidden-phone">قیمت</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($deliveries)
                        @foreach($deliveries as $delivery)
                            <tr class="gradeX odd">
                                <td class=" "><a
                                            href="{{route('admin.dashboard.deliveries.show',['id' => $delivery->id])}}">{{$delivery->title}}</a>
                                </td>
                                <td class="center hidden-phone ">{{$delivery->price}}</td>
                                <td class="center hidden-phone ">{{$delivery->created_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
