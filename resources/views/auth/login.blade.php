<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
						<img src="http://order.qalbesefid.com/wp-content/uploads/2020/05/logosite-1.png" width="40%" style="margin: auto;">

               <!-- <x-application-logo class="w-20 h-20 fill-current text-gray-500" />-->
            </a>
        </x-slot>
		 <link href="http://shop.qalbesefid.com/html/stayle.css" rel='stylesheet' type='text/css'>
<link href='http://www.fontonline.ir/css/BYekan.css' rel='stylesheet' type='text/css'>	<style>
   

	 *{
		font-family:'BYekan',Sans-Serif;
		font-size:20px;
		text-align:right
		}
		.bg-gray-100 {
    background: url(https://uupload.ir/files/bsiu_splashpattern.jpg) !important;
    background-position: center;
}
.w-full input {
    border: 1px solid #ddd;
    height: 40px;
}

	</style>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf
            @if(session()->get('login-step')==null)
                <div>
                    <x-label for="mobile" :value="__('شماره همراه خود را وارد کنید')" />

                    <x-input id="mobile" class="block mt-1 w-full" type="number" name="mobile" :value="old('mobile')" required autofocus />
                </div>

            @else

                @if(session()->get('newRegister')!=null)
                    <div>
                        <x-label for="name" :value="__('نام')" />

                        <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                    </div>

                    <div>
                        <x-label for="family" :value="__('نام خانوادگی')" />

                        <x-input id="family" class="block mt-1 w-full" type="text" name="family" :value="old('family')" required autofocus />
                    </div>
                @endif

                <div>
                    <x-label for="code" :value="__('کد ارسال شده')" />

                    <x-input id="code" class="block mt-1 w-full" type="number" maxlength="4" name="code" :value="old('code')" required autofocus />
                </div>

                <a href="{{route('sendOtpAgain',\Illuminate\Support\Facades\Session::get('user_login_id'))}}">
                    <x-button type="button">
                        ارسال مجدد کد
                    </x-button>
                </a>
            @endif

{{--            <!-- Password -->--}}
{{--            <div class="mt-4">--}}
{{--                <x-label for="password" :value="__('Password')" />--}}

{{--                <x-input id="password" class="block mt-1 w-full"--}}
{{--                                type="password"--}}
{{--                                name="password"--}}
{{--                                required autocomplete="current-password" />--}}
{{--            </div>--}}

{{--            <!-- Remember Me -->--}}
{{--            <div class="block mt-4">--}}
{{--                <label for="remember_me" class="inline-flex items-center">--}}
{{--                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">--}}
{{--                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>--}}
{{--                </label>--}}
{{--            </div>--}}

            <div class="flex items-center justify-end mt-4">
{{--                @if (Route::has('password.request'))--}}
{{--                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">--}}
{{--                        {{ __('Forgot your password?') }}--}}
{{--                    </a>--}}
{{--                @endif--}}

                <x-button class="ml-3">
                    {{ __('ورود') }}
                </x-button>
            </div>
        </form>
{{--        <form method="POST" action="{{route('logout')}}">--}}
{{--            @csrf--}}
{{--            <x-button type="submit" class="ml-3">--}}
{{--                {{ __('خروج') }}--}}
{{--            </x-button>--}}
{{--        </form>--}}
    </x-auth-card>
</x-guest-layout>
