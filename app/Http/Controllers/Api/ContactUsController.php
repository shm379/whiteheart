<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ContactUsResquest;
use App\Models\ContactUs;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param ContactUsResquest $request
     * @return void
     */
    public function store(ContactUsResquest $request)
    {
        try {
            $contactUs = new ContactUs($request->all());
            return $contactUs->save() ? trueJsonResponse(__('message.api.saveInformationSuccessfully')) : falseJsonResponse(__('message.api.saveInformationFailed'));
        } catch (Exception $exception) {
            return falseJsonResponse(__('message.api.somethingWasWrong'));
        }
    }

}
