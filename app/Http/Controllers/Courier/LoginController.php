<?php

namespace App\Http\Controllers\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LoginController extends Controller
{

    /**
     * Admin login form
     * @return Factory|View
     */
    public function form()
    {
        return view('courier.login_form');
    }

    /**
     * Check information for admin authenticate
     * @param Request $request
     * @return RedirectResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('mobile', 'password');
        if (Auth::guard('courier')->attempt($credentials)) {
            // Authentication passed...
            return redirect()->route('courier.dashboard.main', ['wasGuest' => true]);
        }
        flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return back();
    }

}
