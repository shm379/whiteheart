<?php

namespace App\Models;

use App\Traits\TehranTime;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


class UserAddress extends Model
{

    protected $fillable = ['name', 'family', 'mobile', 'phone', 'address' , 'branch_id'];
    use TehranTime;

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * @return BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
    /**
     * @return BelongsTo
     */
    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }

    public function forInvoice()
    {
        $details = [ $this->address, $this->mobile, $this->phone, "{$this->name} {$this->family}"];
        return implode('-', $details);
    }
}
