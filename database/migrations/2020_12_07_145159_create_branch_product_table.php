<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_product', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('product_id')->index('branch_product_product_id_foreign');
            $table->boolean('status')->default(0);
            $table->unsignedBigInteger('price');
            $table->unsignedTinyInteger('discount')->default(0);
            $table->unsignedInteger('quantity')->default(0);
            $table->timestamps();
            $table->unique(['branch_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_product');
    }
}
