@extends('admin.frame')
@section('custom-style')
    <script src="/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!--custom switch-->
    <script src="/js/bootstrap-switch.js"></script>
    <script src="/js/jquery.tagsinput.js"></script>

    <!--custom checkbox & radio-->
    <script type="text/javascript" src="/js/ga.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>

    <script src="/js/form-component.js"></script>
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فیلتر ها
                </header>
                <div class="panel-body">
                    <form class="form-horizontal tasi-form" method="GET" action="">
                        <!--date picker start-->
                        <div class="form-group">

                            <div class="col-lg-2">
                                <label class="control-label ">ایدی </label>
                                <input name="id" type="text" size="16" class="form-control">
                                <label class="control-label ">نام </label>
                                <input name="company" type="text" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">موبایل</label>
                                <input name="country" type="text" size="16" class="form-control">
                                <label class="control-label">ایمیل</label>
                                <input name="email" type="email" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label ">وضعیت </label>
                                <select name="status" class="form-control bound-s">
                                    <option value="">---</option>
                                    <option value="1">بررسی شده</option>
                                    <option value="0">بررسی نشده</option>
                                </select>

                            </div>
                            <div class="col-lg-2">
                                <label class="control-label ">تعداد نمایش </label>
                                <input name="qty" type="number" value="10" size="16" class="form-control">
                                <button class="btn btn-info" type="submit">جستجو</button>
                            </div>
                        </div>
                        <!--date picker end-->
                    </form>


                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    درخواست ها
                </header>
                <table class="table table-striped border-top" id="request_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>نام</th>
                        <th>ایمیل</th>
                        <th>موبایل</th>
                        <th>وضعیت</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($requests)
                        @foreach($requests as $request)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.contactUs.edit',['id'=>$request->id])}}">{{$request->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.contactUs.edit',['id'=>$request->id])}}">{{$request->name}}</a>
                                </td>

                                <td>{{$request->email}}</td>
                                <td>{{$request->mobile}}</td>
                                <td>{{$request->status?'بررسی شده':'بررسی نشده'}}</td>
                                <td>{{$request->created_at_tehran}}</td>
                                <td>{{$request->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
