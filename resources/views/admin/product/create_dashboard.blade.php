@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فرم ثبت محصول جدید
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.products.store')}}" novalidate="novalidate">
                            @csrf
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">شعبه (ضروری)</label>
                                <select multiple name="branchIds[]" class="form-control bound-s" style="width: 25%;">
                                    @foreach($branches as $branch)
                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">دسته (ضروری)</label>
                                <select name="categoryId" class="form-control bound-s">
                                    @foreach($subcategories as $subcategory)
                                        <option value="{{$subcategory->id}}">{{$subcategory->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">عنوان (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" name="title" minlength="2" type="text"
                                           required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">قیمت (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" name="price" minlength="2" type="number"
                                           required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">تعداد (ضروری)</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" name="quantity" minlength="2" type="number"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">توضیحات (ضروری)</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="description" cols="60" rows="5"
                                              required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
