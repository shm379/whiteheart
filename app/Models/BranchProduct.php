<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class BranchProduct extends Pivot
{


    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }
}
