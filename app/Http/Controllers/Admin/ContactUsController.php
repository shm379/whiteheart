<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContactUs;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $requests = ContactUs::orderByDesc('created_at');
        $requests = $request->input('id') ? $requests->where('id', $request->input('id')) : $requests;
        $requests = $request->input('name') ? $requests->where('name', $request->input('name')) : $requests;
        $requests = $request->input('mobile') ? $requests->where('sample_id', $request->input('sample')) : $requests;
        $requests = $request->input('status') ? $requests->where('status', $request->input('status')) : $requests;
        $requests = $request->input('email') ? $requests->where('email', $request->input('email')) : $requests;
        $requests = $request->input('qty') ? $requests->limit($request->input('qty')) : $requests->limit(100);

        return view('admin.contact_us.index_dashboard', [ 'requests' => $requests->get()]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $request = ContactUs::findOrFail($id);
            return view('admin.contact_us.edit_dashboard', ['request' => $request]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $contactRequest = ContactUs::findOrFail($id);
            $contactRequest->status = $request->input('status');
            $contactRequest->update() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('مشکلی به وجود امده');
            return redirect()->route('admin.dashboard.contactUs.edit', ['id' => $id]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
