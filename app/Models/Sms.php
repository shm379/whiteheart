<?php

namespace App\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Sms
 *
 * @method static Builder|Sms newModelQuery()
 * @method static Builder|Sms newQuery()
 * @method static Builder|Sms query()
 * @mixin Eloquent
 */
class Sms extends Model
{
    const API_KEY="6E377A476B425752335A6474633554564163596C4250685575494F5063314769" ;
    protected $client;
    protected $fillable =['from','to','message','template','isVerifyLookup','tokens'];
    public $from;
    public $to;
    public $message;
    public $template="verify";
    public $isVerifyLookup=false;
    public $token=null;
    public $token2=null;
    public $token3=null;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
       $this->client=new \Kavenegar\KavenegarApi( self::API_KEY );
    }

    public function send()
    {
        if($this->isVerifyLookup)
            return $this->client->VerifyLookup($this->to, $this->token, $this->token2, $this->token3, $this->template, $type = null);
        return $this->client->Send($this->from, $this->to, $this->message, $date = null, $type = null, $localid = null);
    }

    public function scopeVerifyLookup($to,$message)
    {
        return $this->client->VerifyLookup($this->to, $this->token, $this->token2, $this->token3, $this->template, $type = null);
    }
}
