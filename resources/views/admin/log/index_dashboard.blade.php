@extends('admin.frame')
@section('custom-style')
    <script src="/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!--custom switch-->
    <script src="/js/bootstrap-switch.js"></script>
    <script src="/js/jquery.tagsinput.js"></script>

    <!--custom checkbox & radio-->
    <script type="text/javascript" src="/js/ga.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>

    <script src="/js/form-component.js"></script>
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فیلتر ها
                </header>
                <div class="panel-body">
                    <form class="form-horizontal tasi-form" method="GET" action="">
                        <!--date picker start-->
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label class="control-label">بازه زمانی </label>
                                از<input name="startTime" type="text" placeholder="1398-01-01 مثال" size="16"
                                         class="form-control">
                                الی<input name="endTime" type="text" placeholder="1398-12-29 مثال" size="16"
                                          class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label ">عنوان </label>
                                <input name="title" type="text" size="16" class="form-control">
                                <label class="control-label ">ایدی </label>
                                <input name="id" type="number" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label ">توضیحات </label>
                                <input name="description" type="text" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label ">تعداد نمایش </label>
                                <input name="qty" type="number" value="10" size="16" class="form-control">
                                <button class="btn btn-info" type="submit">جستجو</button>
                            </div>
                        </div>
                        <!--date picker end-->
                    </form>


                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    گزارش ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>عنوان</th>
                        <th>کاربر</th>
                        <th>توضیحات</th>
                        <th>تاریخ ثبت</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($logs)
                        @foreach($logs as $log)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.logs.show',['id'=>$log->id])}}">{{$log->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.logs.show',['id'=>$log->id])}}">{{$log->log_name}}</a>
                                </td>
                                <td>
                                    <a href="{{$log->causer instanceof \App\Models\Admin?route('admin.dashboard.admins.show',['id'=>$log->causer->id]):route('admin.dashboard.users.show',['id'=>$log->causer->id])}}">{{$log->causer->name .' '.$log->causer->family}}</a>
                                </td>
                                <td>{{'...'.substr($log->description,0,75)}}</td>
                                <td>{{$log->created_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
