<?php
/**
 * Created by PhpStorm.
 * User: masih
 * Date: 4/19/19
 * Time: 10:32 AM
 */

return [
    'api' => [
        'userSignedUp' => "User sign up successfully",
        'userSignedUpFailed' => "We have problem in signing up user",
        'userNotFound' => "User not found",
        'sendVerifyCodeSms' => "Your authentication code send successfully",
        'wrongMobileVerifyCode' => "Wrong code",
        'accessToken' => "Wrong code",
        'saveInformationSuccessfully' => "Information saved successfully",
        'updateInformationSuccessfully' => "Information updated successfully",
        'saveInformationFailed' => "Save information was failed",
        'updateInformationFailed' => "Update information was failed",
        'addressNotFound' => "Address not found",
        'addressDeletedSuccessfully' => "Address deleted successfully",
        'modelNotFound' => "Model not found",
        'categoryNotFound' => "Category not found",
        'bannerNotFound' => "Banner not found",
        'somethingWasWrong' => "Something was wrong",
        'bannerUpdatedSuccessfully' => "Banner updated successfully",
        'bannerCreatedSuccessfully' => "Banner created successfully",
        'bannerDeletedSuccessfully' => "Banner deleted successfully",
        'bannerUpdateFailed' => "Banner update failed",
        'bannerCreateFailed' => "Banner create failed",
        'bannerDeleteFailed' => "Banner delete failed",
        'invalidItem' => "The given item was invalid",

        'bannerUnFollowedSuccessfully' => 'Banner UnFollowed successfully',
        'bannerFollowedSuccessfully' => 'Banner followed successfully',

        'discountNotValid' => 'Discount is not valid for this invoice',
        'invoiceCouldNotCreate' => 'Invoice could not create successfully',
        'invoiceNotFound' => 'Invoice not found',
        'commentDeleteSuccessfully' =>'comment deleted successfully',
        "notEnoughQuantity"=>":product به اتمام رسیده است.",

    ],
];
