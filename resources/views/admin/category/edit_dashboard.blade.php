@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            @include('admin.category.profile_nav')
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات دسته
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$category->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$category->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$category->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>زیر دسته ها :</span>
                                @foreach($category->subcategories as $subcategory)
                                    {{$subcategory->title}} ,
                                @endforeach
                            </p></div>
                        <div class="bio-row">
                            <p><span>الویت :</span> {{$category->priority}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$category->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$category->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.categories.update',[ $category->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">عنوان</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="title" minlength="2" type="text"
                                           required="" value="{{$category->title}}">
                                </div>
                                <label for="cname" class="control-label col-lg-1">الویت</label>
                                <div class="col-lg-1">
                                    <input class=" form-control" id="cname" name="priority"  type="number"
                                           required="" value="{{$category->priority}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">توضیحات</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="description" cols="60" rows="5">{{$category->description}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.categories.destroy',[ $category->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </aside>

    </div>
@endsection
