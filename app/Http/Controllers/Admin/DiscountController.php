<?php

namespace App\Http\Controllers\Admin;

use App\Models\Discount;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $discounts = Discount::all();
        return view('admin.discount.index_dashboard', ['discounts' => $discounts]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function store(Request $request)
    {
        $discount = new Discount($request->all());
        $discount->save() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.discounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        try {
            $discount = Discount::with('invoices')->findOrFail($id);
            return view('admin.discount.show_dashboard', ['discount' => $discount]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        try {
            $discount = Discount::findOrFail($id);
            return view('admin.discount.edit_dashboard', ['discount' => $discount]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function update(Request $request, $id)
    {
        try {
            $discount = Discount::findOrFail($id);
            if ($discount->update($request->all()))
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.discounts.edit', ['id' => $discount->id]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Factory|View
     */
    public function destroy($id)
    {
        try {
            $discount = Discount::findOrFail($id);
            $discount->delete() ? flash()->success('کد تخفیف با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.discounts.index');
        }
    }
}
