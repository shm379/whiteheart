<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function callback(Request $request)
    {
        try {

            $gateway = \Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $cardNumber = $gateway->cardNumber();
            $invoice = \App\Models\Transaction::findOrFail($gateway->transactionId())->transactionable;
            if($invoice->payingWithCreditCard())
                return redirect()->to('https://saxon.ir/#/./dashboard');
            // تراکنش با موفقیت سمت بانک تایید گردید
            // در این مرحله عملیات خرید کاربر را تکمیل میکنیم

        } catch (\Larabookir\Gateway\Exceptions\RetryException $e) {
            // تراکنش قبلا سمت بانک تاییده شده است و
            // کاربر احتمالا صفحه را مجددا رفرش کرده است
            // لذا تنها فاکتور خرید قبل را مجدد به کاربر نمایش میدهیم
            echo $e->getMessage() . "<br>";
        } catch (\Exception $e) {
            // نمایش خطای بانک
            echo $e->getMessage();
        }
    }
}
