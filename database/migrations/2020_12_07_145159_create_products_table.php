<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('subcategory_id');
            //$table->unsignedBigInteger('variant_id');
            $table->string('title', 30);
            $table->string('slug', 50)->unique();
            $table->boolean('status')->default(0);
            $table->text('description');
            $table->unsignedBigInteger('price');
            $table->unsignedTinyInteger('discount')->default(0);
            $table->unsignedSmallInteger('priority')->default(0);
            $table->unsignedInteger('quantity')->default(0);
            $table->boolean('suggested')->default(false);
            $table->text('video')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('variant_id')->references('id')->on('variants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
