<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//         $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        if(!empty(\request()->get('branch_id'))) {
            $products = ProductResource::collection(Product::with(['branches'=>function($b){

            }])->paginate(4));
            $packs = ProductResource::collection(Product::with(['branches'])->where('subcategory_id', 3)->paginate(4));
            $drinks = ProductResource::collection(Category::query()->with(['products' => function ($p) {
                $p->with('branches');
            }])->find(2)->products);
//        return $products;
            return view('home')
                ->with('products', $products)
                ->with('packs', $packs)
                ->with('drinks', $drinks);
        }
        else {
            return redirect()->route('select.branch');
        }
    }

    public function branchRequest()
    {
        return view('branch_request');
    }

    public function cooperationRequest()
    {
        return view('cooperation_request');
    }

    public function aboutBranches()
    {
        return view('about_branches');
    }

	public function selectBranch()
    {
        return view('select_branch');
    }

    public function selectBranchPost(Request $request)
    {
        if($request->directGet){
            Cookie::queue('getMethod', 'directGet', 60);
            Cookie::queue('city', $request->directGet, 60);
//            $cookie = cookie('getMethod', 'directGet', 60);
//            $cookie = cookie('city', $request->directGet, 60);
        }
        elseif($request->NotDirectGet){
            Cookie::queue('getMethod', 'NotDirectGet', 60);
            Cookie::queue('city', $request->NotDirectGet, 60);
        }
        else{
            abort('403');
        }
        return redirect()->route('select.branch')->with(['city']);
//        dd(Cookie::get('city'));
    }

    public function support()
    {
        return view('support');
    }

    public function aboutUs()
    {
        return view('about_us');
    }

    public function contactUs()
    {
        return view('contact_us');
    }

    public function map()
    {
        return view('map');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
