<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'payable_price' => $this->payable_price,
            'orders_price' => $this->orders_price,
            'total_price' => $this->total_price,
            'paid' => $this->paid,
            'address' => $this->address,
            'description' => $this->description,
            'user' => $this->whenLoaded('user'),
            'delivery' => $this->whenLoaded('delivery'),
            'discount' => $this->whenLoaded('discount'),
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'get_ready_at' => $this->get_ready_at ? \Carbon\Carbon::create($this->get_ready_at)->setTimezone('UTC') : null,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
