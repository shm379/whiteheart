@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="/img/profile-avatar.jpg" alt="">
                    </a>
                    <h1>{{$user->name}} {{$user->family}}</h1>
                    <p>{{$user->mobile}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.admins.show',[ $user->id])}}"> <i
                                    class="icon-user"></i> پروفایل</a></li>
                    <li><a href="{{route('admin.dashboard.admins.edit',[ $user->id])}}"> <i
                                    class="icon-user"></i>
                            ویرایش پروفایل</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات ادمین
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$user->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام و نام خانوادگی :</span> {{$user->name}} {{$user->family}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نقش :</span> {{$user->roles->pluck('name')->implode(',')}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام کابری :</span> {{$user->username}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$user->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$user->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فعالیت ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>

                        <th>نوع عملیات</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @foreach($user->activities  as $activity)
                        <tr class="gradeX odd">

                            <td class="hidden-phone "><a
                                    href="{{route('admin.dashboard.logs.show',[$activity->id])}}">{{$activity->log_name}}</a>
                            </td>
                            <td class="center hidden-phone ">{{$activity->created_at_tehran}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
