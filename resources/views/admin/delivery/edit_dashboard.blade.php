@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.deliveries.show',['id' => $delivery->id])}}"> <i
                                class="icon-user"></i> نحوه ارسال</a></li>
                    <li><a href="{{route('admin.dashboard.deliveries.edit',['id' => $delivery->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش </a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات نحوی ارسال
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$delivery->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$delivery->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$delivery->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت :</span> {{$delivery->price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$delivery->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$delivery->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.deliveries.update',['id' => $delivery->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">عنوان</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="title" minlength="2" type="text"
                                           required="" value="{{$delivery->title}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">قیمت</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="price"  type="number"
                                           required="" value="{{$delivery->price}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">توضیحات</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="description"  type="text"
                                           required="" value="{{$delivery->description}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.deliveries.destroy',['id' => $delivery->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </aside>

    </div>
@endsection
