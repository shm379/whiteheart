<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class CreateCommentRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check() ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'=>'nullable|string|required_if:rate,',
            'rate'=>'nullable|integer|between:0,5|required_if:text,'
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $message=__('validation.attributes.invalidForm');
        $response = new JsonResponse(['ok' => 'false',
            'code' => 403,
            'message' => $message,
            'data' => $validator->errors()
        ]);

        throw new ValidationException($validator, $response);
    }
}
