@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                @if($product->images->count())
                    <div class="container">
                        <h2>عکس ها</h2>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @for($i=0;$i<$product->images->count();$i++)
                                    <li data-target="#myCarousel" data-slide-to="{{$i}}"
                                        class="{{$i==0?'active':''}}"></li>
                                @endfor
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @for($i=0;$i<$product->images->count();$i++)
                                    <div class="item {{$i==0?'active':''}}">
                                        <img src="{{route('images.show',[$product->images[$i]->id])}}"
                                             alt="{{$product->images[$i]->original_name}}" style="width:100%;">
                                    </div>
                                @endfor

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                @endif


                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.products.show',[ $product->id])}}">
                            <i
                                    class="icon-user"></i> محصول</a></li>
                    <li><a href="{{route('admin.dashboard.products.edit',[ $product->id])}}"> <i
                                    class="icon-user"></i>
                            ویرایش محصول</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات محصول
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$product->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>گروه :</span> {{$product->subcategory->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$product->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت  :</span> {{$product->price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تعداد  :</span> {{$product->quantity}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تخفیف :</span> {{$product->discount}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت :</span> {{$product->status?'فعال':'غیر فعال'}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$product->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$product->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$product->updated_at_tehran}}</p>
                        </div>


                    </div>
                </div>
            </section>
        </aside>

    </div>
@endsection
