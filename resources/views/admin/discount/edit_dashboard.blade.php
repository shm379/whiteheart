@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.discounts.show',['id' => $discount->id])}}"> <i
                                class="icon-user"></i> کد تخفیف</a></li>
                    <li><a href="{{route('admin.dashboard.discounts.edit',['id' => $discount->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش کد تخفیف</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات کد تخفیف
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>کد :</span> {{$discount->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$discount->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>درصد :</span> {{$discount->percent}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تعداد :</span> {{$discount->count}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>حداقل :</span> {{$discount->min}} تومان</p>
                        </div>
                        <div class="bio-row">
                            <p><span>حداکثر :</span> {{$discount->max}} تومان</p>
                        </div>
                        <div class="bio-row">
                            <p><span>حد مجاز هر کاربر :</span> {{$discount->per_user}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت :</span> {{$discount->status}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ اعتبار :</span> {{$discount->expire_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$discount->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$discount->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.discounts.update',['id' => $discount->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">کد</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="id"  type="text"
                                           required="" value="{{$discount->id}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">عنوان</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="title" minlength="2" type="text"
                                           required="" value="{{$discount->title}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">درضد</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="percent" type="number"
                                           required="" value="{{$discount->percent}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">تعداد</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="count" type="number"
                                           required="" value="{{$discount->count}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">حداقل</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="min"  type="number"
                                           required="" value="{{$discount->min}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">حداکثر</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="max" type="number"
                                           required="" value="{{$discount->max}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">حد مجاز هر کاربر</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="per_user"  type="number"
                                           required="" value="{{$discount->per_user}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.discounts.destroy',['id' => $discount->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </aside>

    </div>
@endsection
