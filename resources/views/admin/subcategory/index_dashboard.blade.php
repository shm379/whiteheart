@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    دسته ها
                    <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                        +
                    </a>
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                         class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                    </button>
                                    <h4 class="modal-title">افزودن زیر دسته</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                                          action="{{route('admin.dashboard.subcategories.store')}}"
                                          novalidate="novalidate">
                                        @csrf
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">عنوان</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control" id="seller" name="title" minlength="6"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-2">سردسته</label>
                                            <div class="col-lg-10">
                                                <div class="col-lg-10">
                                                    <select name="categoryId" id="minbeds" class="form-control bound-s">
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}"><span>{{$category->title}}</span></option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">توضیحات</label>
                                            <div class="col-lg-12">
                                                <textarea class="form-control" name="description" cols="60"
                                                          rows="5" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-lg-4">
                                                <button class="btn btn-danger" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th class="hidden-phone">عنوان</th>
                        <th class="hidden-phone">سرشاخه</th>
                        <th class="hidden-phone">محصولات ها</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                        <th class="hidden-phone">تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($subcategories)
                        @foreach($subcategories as $subcategory)
                            <tr class="gradeX odd">
                                <td class=" "><a
                                        href="{{route('admin.dashboard.subcategories.show',[ $subcategory->id])}}">{{$subcategory->id}}</a>
                                </td>
                                <td class=" "><a
                                        href="{{route('admin.dashboard.subcategories.show',[$subcategory->id])}}">{{$subcategory->title}}</a>
                                </td>
                                <td class="center hidden-phone "><a
                                        href="{{route('admin.dashboard.categories.show',[ $subcategory->category->id])}}">{{$subcategory->category->title}}</a>
                                </td>
                                <td class="center hidden-phone ">{{$subcategory->products()->count()}}</td>
                                <td class="center hidden-phone ">{{$subcategory->created_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$subcategory->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
