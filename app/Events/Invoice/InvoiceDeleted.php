<?php

namespace App\Events\Invoice;

use App\Events\DeletedEvent;
use App\Models\Admin;
use App\Models\Invoice;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class InvoiceDeleted extends DeletedEvent
{
    public $invoice;
    public $user;

    /**
     * AdminUpdated constructor.
     * @param Invoice $invoice
     * @param User|Admin|Model $causer
     * @param string $description
     */
    public function __construct(Invoice $invoice, Model $causer, string $description = "")
    {
        $this->invoice = $invoice;
        $this->causer = $causer;
        parent::__construct($invoice, $causer, 'invoice deleted', $description);
    }


}
