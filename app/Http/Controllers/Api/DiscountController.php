<?php

namespace App\Http\Controllers\Api;

use App\Models\Discount;
use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountController extends Controller
{
    public function check(Request $request)
    {
        try {
            $discount = Discount::findOrFail($request->input('discount'));
            $products = json_decode($request->input('products'), true);
            $price = 0;
            foreach ($products as $p) {
                $product = Product::findOrFail($p[0]);
                $price += $p[1] * $product->price;
            }
            return $discount->validFor($price, auth('api')->user()) ? trueJsonResponse('', '202', ['price' => $price, 'finallyPrice' => $price * (100 - $discount->percent) / 100]) : falseJsonResponse(__('message.api.discountNotValid'));
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.modelNotFound'));
        }
    }
}
