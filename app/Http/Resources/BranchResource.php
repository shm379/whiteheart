<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'lat'=> $this->lat,
            'lng'=>$this->lng,
            'state'=>$this->state,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'address' => $this->address,
            'products' => $this->whenLoaded('products'),
            'couriers' => $this->whenLoaded('couriers'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
