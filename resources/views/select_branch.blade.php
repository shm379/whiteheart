@extends('layouts.theme')
@section('pageTitle')
    انتخاب شعبه
@endsection
@section('content')

    @if(request()->get('directGet')=='0')
    <div class="backk">
{{--        <div class="ciry" >--}}
{{--            <div id="mapa" style="margin:0 auto;text-align:center;width: 100%; height: 300px"></div>--}}
            <div class="">
                <h2 style="text-align: center;color: #fff">نزدیک ترین شعبه به آدرس خود را انتخاب کنید</h2>

                <div id="map" style="height: 82vh; background: rgb(238, 238, 238); position: relative;"></div>
                <div class="map-loc">
{{--                    <a id="a2-map" style="display: none" href="javascript:acceptLocation()">تایید مکان</a>--}}
                    <a id="a-map" href="{{route('select.branch')}}">بازگشت و انتخاب شهر</a>
                </div>
            </div>
		</div>
    @else
    <div class="back">
        @if(!request()->get('city'))
        <div class="ciry" >
            <ul>
                <li><a href="javascript:;" class="sdv tehrann">تهران</a> </li>
                <li><a href="javascript:;" class="sdv esfehan">اصفهان</a></li>
                <li><a href="javascript:alert('در اینجا شعبه نداریم');" class="sdv karaj">کرج</a></li>
                <li><a href="javascript:alert('در اینجا شعبه نداریم');" class="sdv yazd">یزد</a></li>
            </ul>
{{--            <form action="{{route('select.branchPost')}}" method="post">--}}
{{--                @csrf--}}
            <div class="shobe" id="tehrann">
                <a href="{{route('select.branch',['directGet'=>true,'city'=>'tehran'])}}">تحویل در شعبه</a>
                <a href="{{route('select.branch',['directGet'=>false,'city'=>'tehran'])}}">ارسال با پیک</a>
                <i class="far fa-times-circle"></i>
            </div>
            <div class="shobe" id="esfehan">
                <a href="{{route('select.branch',['directGet'=>true,'city'=>'esfehan'])}}">تحویل در شعبه</a>
                <a href="{{route('select.branch',['directGet'=>false,'city'=>'esfehan'])}}">ارسال با پیک</a>
                <i class="far fa-times-circle"></i>
            </div>
{{--            </form>--}}
        </div>
        @else
        <div class="ciry" >
                @if(request()->get('directGet')=='1')
                    <h2 style="text-align: center;color: #fff">انتخاب شعبه</h2>
                    <ul>
                        @if(request()->get('city')=='tehran')
                        <li>
                            <a href="{{route('home',['directGet'=>true,'branch_id'=>3])}}" class="sdv mosala">بالاتر از میدان کاج،خیابان عقبری دوم، نبش بلوار بهزاد</a>
                        </li>
                        @else
                        <li>
                            <a href="{{route('home',['directGet'=>true,'branch_id'=>2])}}" class="sdv mosala">خیابان مصلی،هایپرمی</a>
                        </li>
                        <li>
                            <a href="{{route('home',['directGet'=>true,'branch_id'=>1])}}" class="sdv saadatAbad">خیابان سعادت آباد، جنب بانک تجارت
                            </a>
                        </li>
                        @endif
                    </ul>
                @endif
            </div>
        @endif
        <i class="fab fa-instagram"></i>
        <h1>مارادر اینستاگرام <br>
            دنبــــــــــال کنید
        </h1>
    </div>
    @endif

@endsection
@push('styles')
    <link href="/html/fonts/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="/html/css/L.Control.Locate.min.css" type="text/css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/html/js/axios.min.js" type="text/javascript"></script>
    <script src="/html/js/sweetalert.min.js" type="text/javascript"></script>
    <link href="https://static.neshan.org/sdk/leaflet/1.4.0/leaflet.css" rel="stylesheet" type="text/css">
    <script src="https://static.neshan.org/sdk/leaflet/1.4.0/leaflet.js" type="text/javascript"></script>
    <script src="/html/js/L.Control.Locate.min.js" type="text/javascript"></script>


    <script type="text/javascript">
        let n = this;
        const instance = axios.create({
            baseURL: '{{env('APP_URL')}}/api/v1/',
            // timeout: 1000,
            // headers: {'X-Custom-Header': 'foobar'}
        });
        var myMap = new L.Map('map', {
            key: 'web.eIeqwZhw9sUdhwkYIJPIPrEdSFCLuVK661kwVM9n',
            maptype: 'dreamy',
            poi: true,
            traffic: false,
            center: [35.699739, 51.338097],
            zoom: 14
        });

        myMap.locate({
            setView: true,
            maxZoom: 16
        });

        let i = {};
        let o = L.icon({
            iconUrl: "/html/img/marker-icon-2x.png",
            shadowUrl: "/html/img/marker-shadow.png",
            iconSize: [25, 41],
            shadowSize: [41, 41],
            iconAnchor: [12, 41],
            shadowAnchor: [12, 41],
            popupAnchor: [-3, -76]
        })

        myMap.addControl(L.control.locate({
            locateOptions: {
                enableHighAccuracy: true
        }}));

        function locateMap(e,t,zoom=14){
            myMap.setView(new L.LatLng(e, t), zoom);
        }
        function getCity(city){
            i && myMap.removeLayer(i),
                axios.post('{{route('v1.select_city')}}', {
                    city: '{{request()->get('city')}}',
                }).then(function(t) {
                    if (t.data.success) {
                        locateMap(t.data.lat,t.data.lng,12)
                    }
                })
        }
        function locate (){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                L.control.locate().addTo(myMap);
            }
            else {
                Swal.fire({
                    icon: "error",
                    title: "مشکل در مسیریابی...",
                    text: 'به علت عدم استفاده از تلفن همراه قادر به مسیریابی شما نیستیم!',
                    confirmButtonText: "انتخاب مکان به صورت دستی"
                })

            }
        }

       function marker_on_branches(){
           let branches;
           branches = axios.get('{{route('v1.branches')}}').then(result=>{
               if(result.data.ok){
                   // console.log(result)
                   result.data.data.map(branch=>{
                       L.popup()
                           .setLatLng([branch.lat,branch.lng])
                           .setContent(`<div>${branch.name}</div>
                       <div>شعبه قلب سفید</div>
                       `)
                           .openOn(myMap);
                   })
               }
           })

       }
       // marker_on_branches()
       function get_location(e){
            // alert(e.latlng.lat)
           i && myMap.removeLayer(i),
           axios.post('{{route('v1.select_location')}}', {
               api_token: null,
               lang: "fa",
               device: "site",
               lat_user: e.latlng.lat,
               lng_user:  e.latlng.lng,
               city: '{{request()->get('city')}}',
               package: null,
               user_token: null,
               user_temp_token: null,
               phone_unique_code: null
           }).then(function(t) {
               if (t.data.success) {
                   locateMap(e.latlng.lat,e.latlng.lng)
                   i = new L.marker(e.latlng,{
                       icon: o
                   }).addTo(myMap)
                   jQuery('#a2-map').show()
                   let branch_id = t.data.branch_id
                   window.location = `  {{route('home',['directGet'=>request()->directGet])}}&branch_id=${branch_id}`
                   var r = new Date;
                   r.setTime(r.getTime() + 31536e6);
                   var s = r.toUTCString()
                       , l = t.data.provider;
                   l = {
                       provider_id: l.provider_id,
                       provider_title: l.provider_title,
                       provider_phone: l.provider_phone,
                       provider_lat: l.provider_lat,
                       provider_lng: l.provider_lng,
                   }
                       // n.$cookies.set("provider", l, s);

                   var c = {
                       lng: e.latlng.lng,
                       lat: e.latlng.lat,
                       distance: t.data.distance,
                       type: "sendByShipping"
                   };

                   // n.$cookies.set("address", c, s)
               } else
                   Swal.fire({
                       icon: "error",
                       title: "متاسفیم...",
                       text: t.data.message,
                       confirmButtonText: "انتخاب مکان جدید"
                   })
           })
       }
       function acceptLocation(e){
           console.log(i)
       }
        myMap.on('click', function(e) {
            get_location(e)
        })
        myMap.on('locationfound', function(e) {
                new L.marker(e.latlng,{
                    icon: o
                }).addTo(myMap)
        })
        @if(request()->get('city')!=null)
        getCity('{{request()->get('city')}}');
        @endif
    </script>

    <script>
        $(".tehrann").click(function () {
            $("#tehrann").toggle('slow');
            event.stopPropagation();
        });
        $(".esfehan").click(function () {
            $("#esfehan").toggle('slow');
            event.stopPropagation();
        });
        $(".shobe").click(function () {
            event.stopPropagation();
        });
        $(".far.fa-times-circle").click(function () {
            $(".shobe").toggle('hide');
            event.stopPropagation();
        });

        $(".shobe").click(function () {
            event.stopPropagation();
        });


        $(".fas.fa-bars").click(function () {
            $(".mobb").toggle('slow');
            event.stopPropagation();
        });
        function createMarker()
        {
            var markerFrom = L.circleMarker([28.6100,77.2300], { color: "#F00", radius: 10 });
            var markerTo =  L.circleMarker([18.9750,72.8258], { color: "#4AFF00", radius: 10 });
            var from = markerFrom.getLatLng();
            var to = markerTo.getLatLng();
            markerFrom.bindPopup('Delhi ' + (from).toString());
            markerTo.bindPopup('Mumbai ' + (to).toString());
            myMap.addLayer(markerTo);
            myMap.addLayer(markerFrom);
            getDistance(from, to);
        }

        function getDistance(from, to)
        {
            var container = document.getElementById('map');
            container.innerHTML = ("New Delhi to Mumbai - " + (from.distanceTo(to)).toFixed(0)/1000) + ' km';
        }
        // createMarker()
    </script>


@endpush
