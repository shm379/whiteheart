<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
<script>
    function toast(title, type, url) {
        Swal.mixin({
            toast: true,
            position: 'bottom-start',
            showConfirmButton: false,
            timer: 5000,
            timerProgressBar: true,
            grow: "fullscreen",
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        }).fire({
            icon: type,
            title: title,
            html: '<a href="' + url + '">نمایش</a>'
        })

    }
</script>
<script>
    var socket = io.connect('http://127.0.0.1:6001');

    socket.emit('subscribe', {
        channel: "laravel_database_admin"_,
    }).on("App\\Events\\Banner\\InvoiceCreated", function (channel, message) {
        toast(message.message, message.type, message.url);
    });
</script>
{{--
<script>
    var socket = io.connect('http://localhost:6001');
    socket.emit('subscribe', {
        channel: 'admin-{{auth()->id()}}'
    }).on("admin-{{auth()->id()}}", function (channel, data) {
        toast('test', 'error', 'http://google.com');
    });


</script>
--}}
