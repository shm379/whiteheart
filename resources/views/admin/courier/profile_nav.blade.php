<section class="panel">
    <ul class="nav nav-pills nav-stacked">
        <div class="user-heading round">
            <a href="#">
                <img
                    src="@if($courier->image) {{route('images.show',$courier->image->id)}} @else /img/profile-avatar.jpg @endif"
                    alt="">
            </a>
        </div>
        <li class="{{isActive(['admin.dashboard.couriers.show'])}}"><a href="{{route('admin.dashboard.couriers.show',$courier->id)}}"> <i
                    class="icon-user"></i> پیک</a></li>
        <li class="{{isActive(['admin.dashboard.couriers.edit'])}}"><a href="{{route('admin.dashboard.couriers.edit',$courier->id)}}"> <i
                    class="icon-user"></i>
                ویرایش </a></li>
    </ul>
</section>
