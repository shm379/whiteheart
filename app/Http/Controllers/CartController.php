<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{

    public function __construct()
    {
//        dd(Session::get('cartItems'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Session::get('cartItems');
        $total = Collection::make($items)->sum('price');
//        if(!$items){
//            $items = Session::
//        }
        return view('cart',compact(['items','total']));
    }

    public function add($product_id,$branch_id)
    {
        $product = Product::query()->findOrFail($product_id);
        $branch =$product->branches()->find($branch_id);

        if(is_null($branch)){
            return redirect()->back();
        }
        if(!Session::has('cartItems')) {
            Session::put('cartItems',Collection::make([]));
            $price = $branch->pivot->price;
            $quantity = $branch->pivot->quantity;
            $status = $branch->pivot->status;
            $images = $product->images;
            $product = Collection::make($product);
            $product->put('card_id',rand(11111,99999));
            $product->put('images',$images);
            $product->put('price',$price);
            $product->put('quantity',$quantity);
        }
        else {
            $price = $branch->pivot->price;
            $quantity = $branch->pivot->quantity;
            $status = $branch->pivot->status;
            $images = $product->images;
            $product = Collection::make($product);
            $product->put('card_id',rand(11111,99999));
            $product->put('images',$images);
            $product->put('price',$price);
            $product->put('quantity',$quantity);
        }
        $add = Session::push('cartItems',$product);
        Session::save();
		if(!is_null(request()->directGet)){
			return redirect()->route('cart',['directGet'=>request()->directGet,'branch_id'=>request()->branch_id]);
		}
        return redirect()->route('cart');
    }

    public function remove($product_id)
    {
//        Session::pull('cartItems');
        if(Session::has('cartItems')) {
            $items = Session::get('cartItems');

            foreach ($items as $key => $item) {
                if ($items->where('card_id',$product_id)) {
                    $selected[] = $items->pull($key);
                }
            }
//            Session::save();
        }
        return redirect()->route('cart');
    }

    public function empty()
    {
        if(Session::has('cartItems')) {
            $items = Session::get('cartItems');
            Session::pull('cartItems',[]);
        }
        return redirect()->route('cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
