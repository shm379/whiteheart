@extends('layouts.theme')
@section('pageTitle')
   تسویه حساب
@endsection
@section('content')
    <div class="main-containet">
        <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">سبد خرید</span>
                    <span class="badge badge-secondary badge-pill">{{$items?count($items):0}}</span>
                </h4>
                <ul class="list-group mb-3">
                    @if($items)
                    @foreach($items as $item)
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">{{$item["title"]}}</h6>
                            <small class="text-muted">{{$item['description']}}</small>
                        </div>
                        <span class="text-muted">{{number_format($item['price'])}} تومان</span>
                    </li>
                    @endforeach
                    @if($discount)
                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="text-success">
                            <h6 class="my-0">کد تخفیف</h6>
                            <small>{{$discount->code}}</small>
                        </div>
                        <span class="text-success">-{{number_format($discount->total)}} تومان</span>
                    </li>
                    @endif
                    @endif
                    <li class="list-group-item d-flex justify-content-between">
                        <span>جمع کل</span>
                        <strong>{{number_format($total)}} تومان</strong>
                    </li>
                </ul>

                <form class="card p-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="کد تخفیف خود را وارد کنید">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">استفاده</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">آدرس فاکتور</h4>
                <form class="needs-validation" action="{{route('checkout.pay')}}" method="post" novalidate="">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">نام</label>
                            <input type="text" class="form-control" name="name" id="firstName" placeholder="" value="{{\Illuminate\Support\Facades\Auth::user()->name}}" required="">
                            <div class="invalid-feedback">
                                نام الزامی می باشد.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">نام خانوادگی</label>
                            <input type="text" class="form-control" name="family" id="lastName" placeholder="" value="{{\Illuminate\Support\Facades\Auth::user()->family}}" required="">
                            <div class="invalid-feedback">
                                نام خانوادگی الزامی می باشد.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="mobile">شماره موبایل</label>
                        <div class="input-group">

                            <input type="text" class="form-control" name="mobile" id="mobile" placeholder="mobile" value="{{\Illuminate\Support\Facades\Auth::user()->mobile}}" required="">
                            <div class="invalid-feedback" style="width: 100%;">
                               شماره موبایل خود را وارد کنید
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="mobile">شماره ثابت</label>
                        <div class="input-group">

                            <input type="text" class="form-control" name="phone" id="phone" placeholder="تلفن" value="{{\Illuminate\Support\Facades\Auth::user()->phone}}" required="">
                            <div class="invalid-feedback" style="width: 100%;">
                               شماره ثابت خود را وارد کنید
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="state">استان</label>
                            <select class="custom-select d-block w-100" id="state" required="">
                                <option value="">استان...</option>
                                <option value="اصفهان" selected>اصفهان</option>
                                <option disabled value="تهران">تهران</option>
                            </select>
                            <div class="invalid-feedback">
                                لطفا شهرستان را صحیح انتخاب کنید.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="city">شهر</label>
                            <select class="custom-select d-block w-100" id="city" name="city" required="">
                                <option value="">شهر...</option>
                                <option value="اصفهان" selected>اصفهان</option>
                            </select>
                            <div class="invalid-feedback">
                                لطفا شهرستان را صحیح انتخاب کنید.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="email">ایمیل <span class="text-muted">(اختیاری)</span></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
                        <div class="invalid-feedback">
                            لطفا یک ایمیل صحیح برای دریافت اطلاعات سفارش خود وارد کنید.
                        </div>
                    </div>

                    <div class="col-md-6 mb-3">
                        <label for="address">آدرس</label>
                        <select class="custom-select d-block w-100" id="select-address" name="address">
                            @if(request()->get('directGet')==1)
                                <option value="{{$branch->address}}" selected>{{$branch->address}}</option>
                            @else
                                <option value="" selected>انتخاب آدرس</option>
                                @foreach(\Illuminate\Support\Facades\Auth::user()->addresses as $address)
                                    <option value="{{$address->id}}">{{$address->name.' '.$address->family}} - {{$address->address}}</option>
                                @endforeach
                                <option value="addAddress">اضافه کردن</option>
                            @endif
                        </select>
                        <div class="invalid-feedback">
                            لطفا آدرس خود را وارد کنید.
                        </div>
                    </div>

                    </div>

                    <div class="row" id="newAddress" style="display: none">
                        <div class="col-md-6 mb-3">
                            <label for="address">آدرس خود را وارد کنید</label>
                            <textarea class="form-control" name="newAddress" ></textarea>
                            <div class="invalid-feedback">
                                لطفا آدرس خود را وارد کنید.
                            </div>
                        </div>
                    </div>



                    <hr class="mb-4">

                    <h4 class="mb-3">درگاه پرداخت</h4>

                    <div class="d-block my-3" style="direction: rtl">
                        <div class="custom-control custom-radio" style="text-align: right">
                            <input id="pay_cod" name="paymentMethod" type="radio" class="custom-control-input" value="cod" required="" checked>
                            <label class="custom-control-label" for="pay_cod">پرداخت در محل</label>
                        </div>
                        <div class="custom-control custom-radio" style="text-align: right">
                            <input id="pay_zarinpal" name="paymentMethod" type="radio" class="custom-control-input" value="zarinpal" required="">
                            <label class="custom-control-label" for="pay_zarinpal">درگاه پرداخت زرین پال</label>
                        </div>
                    </div>
                    <input type="hidden" name="branch_id" value="{{request()->branch_id}}">
                    <input type="hidden" name="directGet" value="{{request()->directGet}}">
                    <hr class="mb-4">
                    @if($items)
                        <button type="submit" class="btn btn-primary btn-lg btn-block">پرداخت</button>
                    @else
                        شما محصولی در سبد خرید ندارید
                    @endif
                </form>
            </div>
        </div>
        </div>
    </div>
@endsection
@push('styles')
    <style>
        .list-group{
            padding-right: 0px;
            padding-left: inherit;
        }
        .mb-3{
            text-align: right !important;
        }
        #pak .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto{
            padding-right: inherit;
            padding-left: 25px;
        }
    </style>
@endpush
@push('scripts')
    <script>
        jQuery('#select-address').on('change',function (){
            console.log(jQuery(this).value)
            if(jQuery(this).val()==='addAddress'){
                jQuery('#newAddress').show()
            }
        })
    </script>

@endpush
