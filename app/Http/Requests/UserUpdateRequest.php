<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|between:2,255',
            'family' => 'string|between:2,255',
            'email' => 'nullable|email',
        ];
    }

    public function messages()
    {
        return [];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $message=__('validation.attributes.invalidForm');
        $response = new JsonResponse(['ok' => 'false',
            'code' => 403,
            'message' => $message,
            'data' => $validator->errors()
        ]);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
