<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VariantCollection extends BaseCollection
{
    public $collects ='App\Http\Resources\VariantResource';
}
