<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Http\Resources\BranchCollection;
use App\Http\Resources\BranchResource;
use App\Http\Resources\ProductCollection;
use App\Models\Comment;
use App\Models\Branch;
use App\Models\Rate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;

class BranchController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return BranchCollection
     */
    public function index(Request $request)
    {
        try {
            $branches = Branch::with(['products']);
            return new BranchCollection($branches->get());
        } catch (QueryException $exception) {
            return falseJsonResponse("Bad query for search");
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int | string $id
     * @return BranchResource
     */
    public function show($id)
    {
        try {
            $branch = Branch::with(['products'])
                ->findOrFail($id);
            return new BranchResource($branch);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.bannerNotFound'), 401);
        }
    }

    public function products($id)
    {
        try {
            $products = Branch::with(['products'])->findOrFail($id)->products;
            return new ProductCollection($products);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.bannerNotFound'), 401);
        }
    }

}
