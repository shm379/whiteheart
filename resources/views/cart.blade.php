@extends('layouts.theme')
@section('pageTitle')
    سبد خرید
@endsection
@section('content')

    <div class="main-containet">
	<br>
	<br>
	<br>
    <div class="container">
        @if($items)
        <table id="cart" class="table table-hover table-condensed">
            <thead>
            <tr>
                <th style="width:50%">محصول</th>
                <th style="width:10%">قیمت</th>
                <th style="width:8%">تعداد</th>
                <th style="width:22%" class="text-center">جمع کل</th>
                <th style="width:10%"></th>
            </tr>
            </thead>
            <tbody>

            @foreach($items as $item)
                <tr data-id="{{$item['id']}}">
                    <td data-th="محصولات">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs">
                                @if(!is_null($item['images']))
                                    <img width="100%" src="{{route('images.show',$item['images'][0]['id'])}}" alt="..." class="img-responsive"/>
                                @endif
                            </div>
                            <div class="col-sm-10">
                                <h4 class="nomargin">{{$item['title']}}</h4>
                                <p>{{$item['description']}}</p>
                            </div>
                        </div>
                    </td>
                    <td data-th="قیمت" id="price">{{number_format($item['price'])}} تومان</td>
                    <td data-th="تعداد">
                        <input id="quantity" type="number" class="form-control text-center" value="1">
                    </td>
                    <td data-th="جمع" id="subtotal" class="text-center">{{number_format($item['price'])}} تومان</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm"> <i class="fa fa-refresh"></i> </button>
                        <a href="{{route('cart.remove',$item['card_id'])}}" class="btn btn-danger btn-sm"> <i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr class="visible-xs">
                <td class="text-center"><strong><span  id="total_price">{{$total}}</span> تومان</strong></td>
            </tr>
            <tr>
			@if(!is_null(request()->directGet))
                <td ><a href="{{route('home',['directGet'=>request()->directGet,'branch_id'=>request()->branch_id])}}" class="btn btn-warning"> ادامه خرید <i class="fa fa-angle-left"></i></a>
								<a style="width: 50%;display: inline-block;margin-right: 10px;" href="{{route('checkout',['directGet'=>request()->directGet,'branch_id'=>request()->branch_id])}}" class="btn btn-success btn-block">پرداخت <i class="fa fa-angle-right"></i></a></strong>
			@else
                <td ><a href="http://shop.qalbesefid.com/" class="btn btn-warning"> ادامه خرید <i class="fa fa-angle-left"></i></a>
								<a style="width: 50%;display: inline-block;margin-right: 10px;" href="{{route('checkout')}}" class="btn btn-success btn-block">پرداخت <i class="fa fa-angle-right"></i></a></strong>
			@endif
				</td>
                <td colspan="2" class="hidden-xs"></td>
                <td class="hidden-xs text-center">
				<strong>{{number_format($total)}} تومان</strong>
				</td>
                <td></td>
            </tr>
            </tfoot>
        </table>
        @endif
    </div>
    </div>
@endsection
@push('styles')
    <style>
        .table>tbody>tr>td, .table>tfoot>tr>td{
            vertical-align: middle;
        }
        @media screen and (max-width: 600px) {
            table#cart tbody td .form-control {
                width: 20%;
                display: inline !important;
            }

            .actions .btn {
                width: 36%;
                margin: 1.5em 0;
            }

            .actions .btn-info {
                float: left;
            }

            .actions .btn-danger {
                float: right;
            }

            table#cart thead {
                display: none;
            }

            table#cart tbody td {
                display: block;
                padding: .6rem;
                min-width: 320px;
            }

            table#cart tbody tr td:first-child {
                background: #333;
                color: #fff;
            }

            table#cart tbody td:before {
                content: attr(data-th);
                font-weight: bold;
                display: inline-block;
                width: 8rem;
            }


            table#cart tfoot td {
                display: block;
            }

            table#cart tfoot td .btn {
                display: block;
            }

        }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

@endpush

@push('scripts')
    <script>
        const $ = jQuery

        $(document).ready(function (){
           $('#quantity').on('change',function (){
               let parent = $(this).parents()[1]
               let product_id = $(parent).data("id")
               $(parent).$('#subtotal').text('fff')
           })
        })
    </script>
@endpush
