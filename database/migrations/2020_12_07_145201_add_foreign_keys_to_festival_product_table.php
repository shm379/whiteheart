<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFestivalProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('festival_product', function (Blueprint $table) {
            $table->foreign('festival_id')->references('id')->on('festivals')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('festival_product', function (Blueprint $table) {
            $table->dropForeign('festival_product_festival_id_foreign');
            $table->dropForeign('festival_product_product_id_foreign');
        });
    }
}
