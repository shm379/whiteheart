<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'family' => $this->family,
            'mobile' => $this->mobile,
            'credit' => $this->credit,
            'email' => $this->email,
            'createdAt'=>$this->created_at,
            'updatedAt'=>$this->updated_at
        ];
    }

}
