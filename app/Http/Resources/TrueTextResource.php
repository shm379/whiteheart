<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrueTextResource extends BaseResource
{
    public function __construct(string $message = "", $code = 200, $resource = null)
    {
        $this->with['code'] = $code;
        $this->with['message'] = $message;
        parent::__construct($resource);
    }
}
