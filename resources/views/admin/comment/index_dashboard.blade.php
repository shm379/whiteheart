@extends('admin.frame')
@section('custom-style')
    <script src="/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!--custom switch-->
    <script src="/js/bootstrap-switch.js"></script>
    <script src="/js/jquery.tagsinput.js"></script>

    <!--custom checkbox & radio-->
    <script type="text/javascript" src="/js/ga.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>

    <script src="/js/form-component.js"></script>
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فیلتر ها
                </header>
                <div class="panel-body">
                    <form class="form-horizontal tasi-form" method="GET" action="">
                        <!--date picker start-->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class="control-label ">امتیاز </label>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input name="minRate" type="number" size="8" class="form-control">
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="control-label ">تا</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input name="maxRate" type="number" size="8" class="form-control">
                                        </div>
                                    </div>
                                    <label class="control-label ">ایدی </label>
                                    <input name="id" type="text" size="16" class="form-control">
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label ">کاربر </label>
                                    <input name="user" type="text" size="16" class="form-control">
                                    <label class="control-label ">متن </label>
                                    <input name="text" type="text" size="16" class="form-control">
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label ">تعداد نمایش </label>
                                    <input name="qty" type="number" value="10" size="16" class="form-control">
                                    <label class="control-label ">وضعیت </label>
                                    <select name="status" class="form-control bound-s">
                                        <option value=""></option>
                                        <option value="0">در انتظار تایید</option>
                                        <option value="1">منتشر شده</option>
                                    </select>
                                    <button class="btn btn-info" type="submit">جستجو</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فاکتور ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>کاربر</th>
                        <th>امتیاز</th>
                        <th>متن</th>
                        <th>محصول</th>
                        <th>وضعیت</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($comments)
                        @foreach($comments as $comment)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.comments.edit',['id'=>$comment->id])}}">{{$comment->id}}</a>
                                </td>
                                <td><a href="{{route('admin.dashboard.users.show',['id'=>$comment->user->id])}}">{{$comment->user->name}} {{$comment->user->family}}</a></td>
                                <td>{{$comment->rate?:'----'}}</td>
                                <td>{{$comment->text?:'----'}}</td>
                                <td><a href="{{route('admin.dashboard.pistachios.show',['id'=>$comment->id])}}">{{$comment->pistachio->title}}</a></td>
                                <td>{{$comment->approved?'منتشر شده':'در انتظار تایید'}}</td>
                                <td>{{$comment->created_at_tehran}}</td>
                                <td>{{$comment->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
