<?php

namespace App\Http\Controllers\Api;

use App\Festival;
use App\Http\Resources\FestivalCollection;
use App\Http\Resources\FestivalResource;
use App\Http\Resources\ProductResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;

class FestivalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return FestivalCollection
     */
    public function index(Request $request)
    {
        $festivals = Festival::orderDescBy('created_at');
        !$request->title ?: $festivals->where('title', 'like', "%{$request->title}%") ;
        return new FestivalCollection(Festival::with(['image'])->get());
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return FestivalResource|Response
     */
    public function show($id)
    {
        try {
            $festival = Festival::with(['image'])
                ->findByIdOrSlugOrFail($id);
            return new FestivalResource($festival);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.categoryNotFound'), 401);
        }
    }

    public function products($id, Request $request)
    {
        try {
            $products = Festival::findByIdOrSlugOrFail($id)
                ->products()
                ->with('images')
                ->where('status', '>', '0');
            $perPage = $request->perPage ? $request->perPage : 10;
            return new ProductResource($products->paginate($perPage));
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.categoryNotFound'), 401);
        }
    }


}
