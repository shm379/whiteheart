<section class="panel">
    <ul class="nav nav-pills nav-stacked">
        <div class="user-heading round">
            {{--<a href="#">
                <img
                    src="@if($branch->image) {{route('images.show',$branch->image->id)}} @else /img/profile-avatar.jpg @endif"
                    alt="">
            </a>--}}
        </div>
        <li class="{{isActive(['admin.dashboard.branches.show'])}}"><a href="{{route('admin.dashboard.branches.show',$branch->id)}}"> <i
                    class="icon-user"></i> شعبه</a></li>
        <li class="{{isActive(['admin.dashboard.branches.edit'])}}"><a href="{{route('admin.dashboard.branches.edit',$branch->id)}}"> <i
                    class="icon-user"></i>
                ویرایش </a></li>
    </ul>
</section>
