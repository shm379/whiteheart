<?php

namespace App\Models;


class Flash
{
    public function info($message)
    {
        $title = "ببین...";
        if (!is_string($message))
            $message = $message['text'];
        if (isset($message['title']))
            $title = $message['title'];
        session()->flash('flash_message', ['type' => "info", 'title' => $title, 'text' => $message]);
    }

    public function success($message)
    {
        $title = "درسته!";
        if (!is_string($message))
            $message = $message['text'];
        if (isset($message['title']))
            $title = $message['title'];
        session()->flash('flash_message', ['type' => "success", 'title' => $title, 'text' => $message]);

    }

    public function error($message)
    {
        $title = "خطا!";
        if (!is_string($message))
            $message = $message['text'];
        if (isset($message['title']))
            $title = $message['title'];
        session()->flash('flash_message', ['type' => "error", 'title' => $title, 'text' => $message]);
    }

    public function warning($message)
    {
        $title = "توجه!";
        if (!is_string($message))
            $message = $message['text'];
        if (isset($message['title']))
            $title = $message['title'];
        session()->flash('flash_message', ['type' => "warning", 'title' => $title, 'text' => $message]);
    }
}