<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BranchCollection extends BaseCollection
{
    public $collects = 'App\Http\Resources\BranchResource';
}
