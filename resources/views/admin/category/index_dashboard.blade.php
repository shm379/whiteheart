@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    دسته ها
                    <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                        +
                    </a>
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                         class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                    </button>
                                    <h4 class="modal-title">افزودن دسته</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                                          action="{{route('admin.dashboard.categories.store')}}" novalidate="novalidate">
                                        @csrf
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">عنوان</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control" id="seller" name="title" minlength="6"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">توضیحات</label>
                                            <div class="col-lg-12">
                                                <textarea class="form-control" name="description" cols="60" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-lg-4">
                                                <button class="btn btn-danger" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th class="hidden-phone">عنوان</th>
                        <th class="hidden-phone">زیر دسته ها</th>
                        <th class="hidden-phone">الویت</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                        <th class="hidden-phone">تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($categories)
                        @foreach($categories as $category)
                            <tr class="gradeX odd">
                                <td class=" "><a
                                            href="{{route('admin.dashboard.categories.show',[ $category->id])}}">{{$category->id}}</a>
                                </td>
                                <td class=" "><a
                                            href="{{route('admin.dashboard.categories.show',[ $category->id])}}">{{$category->title}}</a>
                                </td>
                                <td class="center hidden-phone ">{{$category->subcategories()->count()}}</td>
                                <td class="center hidden-phone ">{{$category->priority}}</td>
                                <td class="center hidden-phone ">{{$category->created_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$category->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
