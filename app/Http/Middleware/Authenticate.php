<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param $guard
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login').'?redirect_to='.$request->fullUrl();

        }
//       if(Auth::guard($guard)->check()){
//           switch ($guard){
//               case 'admin':
//                   return route('admin.login.form');
//                   break;
//               case 'sanctum':
//               case 'web':
//                   return route('login');
//               break;
//               case 'api':
//                   return route('api-tokens.index');
//           }
//       }
//

    }
}
