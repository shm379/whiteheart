@extends('layouts.theme')
@section('pageTitle')
    صفحه اصلی
@endsection
@section('content')
    <div class="wave">
        <div class="slid-top">
            <div class="slider">
                <div class="container">
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-md-3 col-sm-12">
                                <div class="box">
                                    @if(!empty($product->images[0]->id))
                                        <img src="{{route('images.show',$product->images[0]->id)}}" alt="">
                                    @endif
                                    <h2>{{$product->title}}</h2>
                                    <h3>{{$product->description}}</h3>
                                    <h2 id="tag">{{$product->title}}</h2>
                                    <div class="text-p">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12" id="p-t">

                                                <div class="text-pp">

                                                    <div class="rectangle">
                                                        <div
                                                            class="circle">

                                                        </div>
                                                    </div>
                                                    <h3>{{$product->subcategory->title}}</h3>
                                                    <h3>{{$product->options}}</h3>
                                                </div>
												@if($product->video)
                                                <div class="video">
                                                    <a href="{{route('video.show',$product->video)}}"><div class="triangle-right">
                                                        </div></a>
                                                    <a href="{{route('video.show',$product->video)}}"> ویدیو</a>
                                                </div>
												@endif
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="text-pro-left">
                                                    @if(!empty($product->priceOfBranch(request()->branch_id))))

                                                        <h1 id="a3">{{$product->priceOfBranch(request()->branch_id)}}</h1>
                                                    @else
                                                        <small id="a3">قیمت ندارد</small>
                                                    @endif
													@if(!is_null(request()->directGet))
                                                    	<a href="{{route('cart.add',[$product->id,request()->branch_id]).'/?directGet='.request()->directGet.'&branch_id='.request()->branch_id}}">
															<img src="html/img/shop.png " alt="">
														</a>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>

            <div class="text">
                <h3>This is how we Roll!</h3>
                <h4>BOOK OUR SHACK TRUCK FOR YOUR NEXT EVENT IN <br> THE CREATER TRI-STATE AREA!</h4>

                <h4></h4>
            </div>
        </div>
    </div>



    <div class="pakage">
        <div class="container">
            <h2>پکیج های هدیه دار قلب سفید</h2>
        </div>

    </div>

    <div class="skider2">
        <div class="container">

            <div class="row" id="pak">
                @foreach($packs as $pack)
                <div class="col-md-3 col-sm-12">
                    <div class="box-pro">
                        @if(!empty($pack->images[0]->id))
                            <img src="{{route('images.show',$pack->images[0]->id)}}" alt="">
                        @endif
                        <h3>{{$pack->title}}
                            <Br>
                            <span>{{$pack->description}}</span>
                        </h3>
						@if(!is_null(request()->directGet))
                        <a href="{{route('cart.add',[$pack->id,request()->branch_id]).'/?directGet='.request()->directGet.'&branch_id='.request()->branch_id}}">
							<img id="shop-slid2" src="html/img/shop.png " alt="">
						</a>
						@endif
                            @if(!empty($pack->price))
                                <h2 id="q4">{{ $pack->finalPrice }}</h2>
                            @endif
                                <!--<h2></h2>-->

                    </div>
                </div>
                @endforeach

            </div>
        </div>

    </div>

    </div>
    <div class="slider3">
        <div class="container">
            <h2>پیش غذا و نوشیدنی</h2>
            <div class="swiper-container2">

                <div class="swiper-wrapper">
                    @foreach($drinks as $drink)
                        <div class="swiper-slide">
                            <div class="swp-bot">
                                @if(!empty($drink->images[0]->id))
                                    <img src="{{route('images.show',$drink->images[0]->id)}}" alt="">
                                @endif
                                <p>{{$drink->title}}</p>
                                    @if(!empty($drink->branches()->get()->first()->pivot))
                                    <h4>
                                        {{$drink->finalPrice}}
                                    <span>تومان</span></h4>
                                    @endif
									@if(!is_null(request()->directGet))
                        <a  href="{{route('cart.add',[$product->id,request()->branch_id]).'/?directGet='.request()->directGet.'&branch_id='.request()->branch_id}}">
						 افزودن به سبد خرید
						</a>
									@endif
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')


@endpush
