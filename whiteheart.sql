-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 01:49 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `whiteheart`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `family`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'مدیر', 'سیستم', 'admin', '$2y$10$3jaC2axx8v4bd9F8JWnOl.ghodfM6p1ZQBbEJuyX8lzqJEX.gfe82', '2020-03-12 13:00:31', '2020-03-16 03:17:09'),
(2, 'پشتیبانی رستوران', 'ایرانی', 'support', '$2y$10$HJGdrtPxqTbc1JDptbOGW.63lFsEigxJhrUcxY3h2L53YA7RolIES', '2020-03-19 12:56:11', '2020-03-19 12:56:11');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branch_product`
--

CREATE TABLE `branch_product` (
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `price` bigint(20) UNSIGNED NOT NULL,
  `discount` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `description`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'کباب', 'کباب', 'کباب', 2, '2020-03-12 13:04:41', '2020-03-15 16:28:18'),
(3, 'نوشیدنی', 'نوشیدنی', 'نوشیدنی', 0, '2020-03-14 06:52:16', '2020-03-14 06:52:16'),
(4, 'پیش غذا', 'پیش-غذا', 'پیش غذا', 0, '2020-03-14 07:44:00', '2020-03-14 07:44:00'),
(6, 'دریایی', 'دریایی', 'دریایی', 2, '2020-03-15 11:58:32', '2020-03-15 16:28:29'),
(7, 'جوجه کباب', 'جوجه-کباب', 'جوجه کباب', 2, '2020-03-15 12:59:55', '2020-03-15 16:25:55'),
(9, 'چلو ها', 'چلو-ها', 'چلو ایرانی', 0, '2020-03-20 05:38:47', '2020-03-20 05:38:47'),
(12, 'مرغ', 'مرغ', 'مرغ', 0, '2020-03-20 06:48:38', '2020-03-20 06:48:38'),
(14, 'دسر', 'دسر', 'دسر', 0, '2020-03-20 06:57:56', '2020-03-20 06:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_buyer` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `national_number`, `mobile`, `password`, `branch_id`, `created_at`, `updated_at`) VALUES
(1, 'تست', '2000000', '09391727950', '828', 0, '2020-10-06 17:30:09', '2020-10-16 17:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

CREATE TABLE `deliveries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percent` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) UNSIGNED NOT NULL,
  `per_user` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `min` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `max` bigint(20) UNSIGNED NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `festivals`
--

CREATE TABLE `festivals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `festival_product`
--

CREATE TABLE `festival_product` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `festival_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gateway_transactions`
--

CREATE TABLE `gateway_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `port` enum('MELLAT','SADAD','ZARINPAL','PAYLINE','JAHANPAY','PARSIAN','PASARGAD','SAMAN','ASANPARDAKHT','PAYPAL','PAYIR') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `ref_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('INIT','SUCCEED','FAILED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INIT',
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `payment_date` timestamp NULL DEFAULT NULL,
  `transactionable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `transactionable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gateway_transactions_logs`
--

CREATE TABLE `gateway_transactions_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED NOT NULL,
  `result_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `original_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `imageable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `original_name`, `hash_name`, `imageable_id`, `imageable_type`, `created_at`, `updated_at`) VALUES
(2, 'کباب-کوبیده-گوسفندی.jpg', 'GSAL8DcsOuezQaJYrdwkATRxoSgBDe9XH4rPizdy.jpeg', 1, 'App\\Models\\SubCategory', '2020-03-12 13:10:04', '2020-03-12 13:10:04'),
(3, 'طرز-تهیه-کباب-کوبیده.jpg', 'OWUPFzikqWWGt08hjRmRt6M4dg6ffg342QmkU8u3.jpeg', 1, 'App\\Models\\Product', '2020-03-12 13:10:41', '2020-03-12 13:10:41'),
(8, 'وغ.jpg', 'gYBVAtgchEBGguCdj6ifS0ib9OdydpOdvC7GqZ9h.jpeg', 3, 'App\\Models\\SubCategory', '2020-03-14 06:53:14', '2020-03-14 06:53:14'),
(10, 'برگ.jpeg', 'YR9waratOXCCmZZXTqF01Zpf6o5dmEXkT9LQbecT.jpeg', 5, 'App\\Models\\Product', '2020-03-14 07:43:17', '2020-03-14 07:43:17'),
(11, 'برگ۲.jpg', 'lm4Edk0EqRs0xbFhqhsI0y4lkNBH85NQ2jcequlX.jpeg', 5, 'App\\Models\\Product', '2020-03-14 07:43:24', '2020-03-14 07:43:24'),
(13, 'برگ۵.jpeg', 'z7tVDhrzOBaHBwj948x8dBdt0ndMFWeXEQRF8UYi.jpeg', 5, 'App\\Models\\Product', '2020-03-14 07:46:13', '2020-03-14 07:46:13'),
(14, 'نوشابه.jpg', 'GJnOyN0dfXME6thSTAeGpPCgTPx2SIRrjwcWCCat.jpeg', 5, 'App\\Models\\SubCategory', '2020-03-15 11:00:28', '2020-03-15 11:00:28'),
(15, 'کوکا قوطی.jpg', 'Ar0JZqS1M02fY263DFnbuNxPuzaKz93L6I0Gpg5B.jpeg', 7, 'App\\Models\\Product', '2020-03-15 11:01:34', '2020-03-15 11:01:34'),
(17, 'زیرو قوطی.jpg', 'YYz1A7Xxc2iAYT7qBduB4QXNoJvGC3T6U7ZVT7PW.jpeg', 8, 'App\\Models\\Product', '2020-03-15 11:03:48', '2020-03-15 11:03:48'),
(18, 'سونآب قوطی.jpg', '2oKZUVewdlyMreYV2zxaxqbAkPeLEt3NrKQXfglf.jpeg', 10, 'App\\Models\\Product', '2020-03-15 11:06:39', '2020-03-15 11:06:39'),
(19, 'زیرو خانواده.jpeg', 'EEXKriUHKYmgUcAqTttPN5Qx6zQarJ5rWxbY00M4.jpeg', 11, 'App\\Models\\Product', '2020-03-15 11:08:21', '2020-03-15 11:08:21'),
(20, '0010856_نوشابه_قوطي_پرتقالي_فانتا_550.jpg', 'HHbyiDCy6HiWXKumMWhTktE0gAp7XgtJ7U3f4eBG.jpeg', 12, 'App\\Models\\Product', '2020-03-15 11:09:50', '2020-03-15 11:09:50'),
(21, 'fanta-portaghali.jpg', 'RkoZYGgOH8124LcSrbDdcwLTVYWbuHI7bPI6mTR8.jpeg', 13, 'App\\Models\\Product', '2020-03-15 11:11:26', '2020-03-15 11:11:26'),
(22, 'نوشابه-کولا-1500-سی-سی-پت-کوکا-کولا-min.jpg', 't38hR7QP732RrLlXw5DNl0B28mXNpuAnkRTJZiqL.jpeg', 14, 'App\\Models\\Product', '2020-03-15 11:23:43', '2020-03-15 11:23:43'),
(23, 'اسپریت خانواده.jpeg', '72c6hRW32kjIAbUJzj1U1UewsBoYSjQ6v8JlYE8y.jpeg', 15, 'App\\Models\\Product', '2020-03-15 11:24:25', '2020-03-15 11:24:25'),
(25, 'khoresh-mast-min.jpg', 'DbraODTxAMUh5tS3ztGlIEXFRNBFFJBGyejcRIHI.jpeg', 16, 'App\\Models\\Product', '2020-03-15 11:28:46', '2020-03-15 11:28:46'),
(26, 'موس کارامل.jpg', 'X7UPYvLI2hfcafAh78LmhpM9JwSVY4dEfzf0komz.jpeg', 17, 'App\\Models\\Product', '2020-03-15 11:30:43', '2020-03-15 11:30:43'),
(27, 'موس شکلات.jpg', '5PImCjukghjs2A5dOiahmy7S9bao3dEzJaPTcWZ9.jpeg', 18, 'App\\Models\\Product', '2020-03-15 11:32:14', '2020-03-15 11:32:14'),
(28, 'شارلوت.jpg', 'FEzpcSjiNhFUYMSP6aN22MMPFc2PzWu7IujPFE2s.jpeg', 19, 'App\\Models\\Product', '2020-03-15 11:33:28', '2020-03-15 11:33:28'),
(30, 'photo_2020-03-15_19-00-47.jpg', 'j3NAkrxS1esIln4ElcVTKLR4ZKw0p7QaEcm4dERS.jpeg', 21, 'App\\Models\\Product', '2020-03-15 13:05:45', '2020-03-15 13:05:45'),
(31, 'photo_2020-03-15_19-00-25.jpg', 'zahmdGOBj17DLVtMOtdzrzE2GsHoVGJ4qlxQ3sJZ.jpeg', 22, 'App\\Models\\Product', '2020-03-15 13:06:50', '2020-03-15 13:06:50'),
(32, 'photo_2020-03-15_19-00-21.jpg', 'ImmIo7rAlzUxoJgB5UQPAuTyUfWRt0wp0KVk8vdU.jpeg', 23, 'App\\Models\\Product', '2020-03-15 13:08:18', '2020-03-15 13:08:18'),
(33, 'photo_2020-03-15_19-00-38.jpg', '6WQ1gZptH0sAdoPxdUDPMroDhStvDUcPgYECXrdf.jpeg', 24, 'App\\Models\\Product', '2020-03-15 13:12:01', '2020-03-15 13:12:01'),
(34, 'photo_2020-03-15_19-00-41.jpg', 'TO6whGQjDWkmXUBlgNHNuAFo4Barr03M8BTuwbKt.jpeg', 25, 'App\\Models\\Product', '2020-03-15 13:12:51', '2020-03-15 13:12:51'),
(35, 'photo_2020-03-15_19-00-44.jpg', 'Al1nnAryfPBb6RaQOgacWcvJx06fTr2ssVbvEp3F.jpeg', 26, 'App\\Models\\Product', '2020-03-15 13:14:46', '2020-03-15 13:14:46'),
(36, 'photo_2020-03-15_19-00-35.jpg', 'FSBv5fweZld3RDYIsjRJdTSudd2bzAYTWldQ7zvK.jpeg', 6, 'App\\Models\\Product', '2020-03-15 13:17:23', '2020-03-15 13:17:23'),
(37, 'دوغ 300 سی سی.jpg', 'p8ZveCfgB43ItQxxNLvroVlF3bhrwABvQ35lkgZF.jpeg', 4, 'App\\Models\\Product', '2020-03-15 15:32:28', '2020-03-15 15:32:28'),
(38, 'دوغ خانواده.jpg', 'hlGp8mqwxhnF36thMOR1AJYvhP3tneEbORSfd5jx.jpeg', 27, 'App\\Models\\Product', '2020-03-15 15:33:34', '2020-03-15 15:33:34'),
(40, 'photo_2020-03-16_09-23-08.jpg', '1OD3h9KvzkueLzkIP9ZctDqbBWPcrPk6YQ7VCuQ7.jpeg', 28, 'App\\Models\\Product', '2020-03-16 02:24:08', '2020-03-16 02:24:08'),
(41, 'photo_2020-03-16_09-32-59.jpg', 'nmF7tCry3fq2VzgpdIUiffJ4m2OBEEuFppA850xd.jpeg', 31, 'App\\Models\\Product', '2020-03-16 02:33:13', '2020-03-16 02:33:13'),
(42, 'ماست موسیر.jpg', 'mfbJrnDkmLA1l7UZz4IqqUqP8gCdkfdbxRsJ7rPc.jpeg', 30, 'App\\Models\\Product', '2020-03-16 02:33:50', '2020-03-16 02:33:50'),
(43, 'logo-AP-ok.png', 'uRpbJMsCf6n7WDqC60N0wS0h21OtdTT8bVs6Zp8C.png', 32, 'App\\Models\\Product', '2020-03-16 02:41:53', '2020-03-16 02:41:53'),
(44, 'logo-AP-ok.png', 'ghDVvWfdiJddyZkTeJJjk2YBth6KItEQuN3mHnU1.png', 33, 'App\\Models\\Product', '2020-03-16 02:42:56', '2020-03-16 02:42:56'),
(45, 'logo-AP-ok.png', 'zrzI8Vke7l7dlaU92pIkbg9sT4bz6bESLKJUpvhH.png', 34, 'App\\Models\\Product', '2020-03-16 02:46:36', '2020-03-16 02:46:36'),
(46, 'logo-AP-ok.png', 'E9MD97RNEvWRHg1BjTwonihIcWkA1U81jtDXrLA0.png', 35, 'App\\Models\\Product', '2020-03-16 02:49:59', '2020-03-16 02:49:59'),
(48, 'logo-AP-ok.png', 'FGPTN4tQDtIpJBqlzQdTZN0OukAH92y6PHVNINCR.png', 39, 'App\\Models\\Product', '2020-03-16 02:53:52', '2020-03-16 02:53:52'),
(49, 'logo-AP-ok.png', 'a1C8bCaE1p1z8vDGsi4ah1gEYVYlxMf2hoamoWFY.png', 40, 'App\\Models\\Product', '2020-03-16 03:03:33', '2020-03-16 03:03:33'),
(50, 'logo-AP-ok.png', 'XxKO2OmOZaZY1IlMYTBocFrAgU5eZp0IXnEgdeN8.png', 41, 'App\\Models\\Product', '2020-03-16 03:04:49', '2020-03-16 03:04:49'),
(51, 'logo-AP-ok.png', 'z3jFpzfhCcwmotIqWiFngKxhLihcaSljRuqgXFDH.png', 42, 'App\\Models\\Product', '2020-03-16 03:06:05', '2020-03-16 03:06:05'),
(52, 'logo-AP-ok.png', 'Lp0vGMn4NC2pEFLtOpjY1AnAgSuGRxKI3HQMu4FW.png', 43, 'App\\Models\\Product', '2020-03-16 03:07:03', '2020-03-16 03:07:03'),
(54, 'خورش ماست.jpg', '8iN097UjdsO8jdzIJrBjM1WqS4Gwx78tTSbu7zmw.jpeg', 16, 'App\\Models\\Product', '2020-03-19 11:30:31', '2020-03-19 11:30:31'),
(56, 'WhatsApp Image 2020-03-19 at 8.02.28 PM (4).jpeg', 'C0ICIFN5i78WT0PMZi0CGKCbhPcFkmSV0wEpCMBp.jpeg', 45, 'App\\Models\\Product', '2020-03-19 13:07:38', '2020-03-19 13:07:38'),
(57, 'WhatsApp Image 2020-03-19 at 8.02.28 PM (3).jpeg', 'KRoGTMboEsGZVpR7wbdldlpmFpAmQTkVqRPKuQUx.jpeg', 44, 'App\\Models\\Product', '2020-03-19 13:11:07', '2020-03-19 13:11:07'),
(58, 'photo_2020-03-15_18-59-57.jpg', 'xlQei9ehr81ZTEY9xgvbhNzzO5hdKlAK2E3leb9q.jpeg', 35, 'App\\Models\\Product', '2020-03-20 05:43:41', '2020-03-20 05:43:41'),
(59, 'logo-AP-ok.png', 'KICKUi6PspggTJxUUb6EsbZkWw9cYF8VroBkdPX1.png', 20, 'App\\Models\\Product', '2020-03-20 05:44:15', '2020-03-20 05:44:15'),
(60, 'photo_2020-03-15_18-59-57.jpg', 'vb4faaLydntBamkTFQF2azGrRQ7CwpX5MnYHPXC0.jpeg', 20, 'App\\Models\\Product', '2020-03-20 06:14:25', '2020-03-20 06:14:25'),
(61, 'logo-AP-ok.png', 'wLlGEwHut9lNvX95PRrnY1yRBnbBevLYuSX06HYq.png', 19, 'App\\Models\\Subcategory', '2020-03-20 06:33:14', '2020-03-20 06:33:14'),
(62, 'logo-AP-ok.png', 'DPblFGft06Zhl5THEUy70wclZ7W4fxJjXVW4r22a.png', 48, 'App\\Models\\Product', '2020-03-20 06:38:37', '2020-03-20 06:38:37');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `payable_price` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `orders_price` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `total_price` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `address` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid` enum('None','Cash','Card') COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `get_ready_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `user_id`, `discount_id`, `status`, `payable_price`, `orders_price`, `total_price`, `address`, `paid`, `description`, `get_ready_at`, `created_at`, `updated_at`) VALUES
(98, 36, NULL, 1, 114000, 114000, 114000, 'ندخدخخلخلخلحللخدرحوجدخرحلخل\nاحااحل-09223738411-03833442118-رضا حیدری', 'None', NULL, '2020-03-20 13:57:53', '2020-03-20 13:55:12', '2020-03-20 13:55:53'),
(99, 40, NULL, 0, 20000, 20000, 20000, 'بلوار شاهد-09901176505-03136507100-مهدی اصفهانی', 'None', NULL, NULL, '2020-03-21 13:05:06', '2020-03-21 13:05:06'),
(110, 36, NULL, 0, 27000, 27000, 27000, 'ندخدخخلخلخلحللخدرحوجدخرحلخل\nاحااحل-09223738411-03833442118-رضا حیدری', 'None', NULL, NULL, '2020-03-22 12:18:00', '2020-03-22 12:18:01'),
(111, 36, NULL, 0, 340000, 340000, 340000, 'خلخاحاحدح\nخررحرحرححرححر-09665354178-08966552443-رضا حیدری', 'None', NULL, NULL, '2020-03-28 05:24:33', '2020-03-28 05:24:33'),
(112, 49, NULL, 0, 27000, 27000, 27000, 'پشت سرت-09010653269-09140832329-جلال جهانی', 'None', NULL, NULL, '2020-03-29 06:27:57', '2020-03-29 06:27:58'),
(113, 49, NULL, 0, 341000, 341000, 341000, 'پشت سرت-09010653269-09140832329-جلال جهانی', 'None', NULL, NULL, '2020-03-29 06:30:11', '2020-03-29 06:30:11'),
(114, 52, NULL, 0, 72000, 72000, 72000, 'بلوارسهروردی،جنب ترمینال زاینده رود،کوچه جلالان شرقی،کوچه آریا،ساختمان کاج،طبقه۱-09309489538-03137861249-ناهید ماهرو', 'None', NULL, NULL, '2020-04-12 13:49:40', '2020-04-12 13:49:40'),
(115, 53, NULL, 0, 81000, 81000, 81000, 'قم-09391727950-02537226190-سید حسین موسوی', 'None', NULL, NULL, '2020-04-12 17:16:26', '2020-04-12 17:16:26'),
(116, 53, NULL, 0, 12500, 12500, 12500, 'قم-09391727950-02537226190-سید حسین موسوی', 'None', NULL, NULL, '2020-04-14 13:25:10', '2020-04-14 13:25:10'),
(117, 55, NULL, 0, 32000, 32000, 32000, 'خیابان هزار جریب - بعد از دانشگاه مهاجر - نرسیده به پل عابر پیاده - کوی شهید رضا پاکدل - مجموعه پیامبر اعظم ساختمان وسطى - طبقه ۴ - واحد ۱۵ - آقای جندی - ۰۹۱۹۸۴۷۱۸۳۷-09198471837-09198471837-حسين جندى', 'None', NULL, NULL, '2020-04-27 15:24:07', '2020-04-27 15:24:07'),
(118, 36, NULL, 0, 390000, 390000, 390000, 'خلخاحاحدح\nخررحرحرححرححر-09665354178-08966552443-رضا حیدری', 'None', NULL, NULL, '2020-06-13 00:57:36', '2020-06-13 00:57:36');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE `invoice_product` (
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`invoice_id`, `product_id`, `count`) VALUES
(98, 1, 2),
(98, 22, 3),
(99, 21, 1),
(110, 1, 1),
(111, 43, 4),
(112, 1, 1),
(113, 5, 2),
(113, 43, 1),
(113, 44, 1),
(114, 22, 1),
(114, 21, 1),
(114, 24, 1),
(115, 1, 3),
(116, 55, 1),
(117, 24, 1),
(118, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_05_02_193213_create_gateway_transactions_table', 1),
(4, '2016_05_02_193229_create_gateway_status_log_table', 1),
(5, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(6, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(8, '2016_06_01_000004_create_oauth_clients_table', 1),
(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(10, '2017_04_05_103357_alter_id_in_transactions_table', 1),
(11, '2018_03_04_224213_add_description_to_gateway_transactions', 1),
(12, '2019_04_18_050500_create_discounts_table', 1),
(13, '2019_07_16_043610_create_categories_table', 1),
(14, '2019_07_16_043652_create_sub_categories_table', 1),
(15, '2019_07_16_044111_create_products_table', 1),
(16, '2019_07_25_051544_create_images_table', 1),
(17, '2019_07_30_122602_create_admins_table', 1),
(18, '2019_07_31_065644_create_user_favorite_table', 1),
(19, '2019_08_02_122551_create_deliveries_table', 1),
(20, '2019_08_03_053752_create_invoices_table', 1),
(21, '2019_08_03_062257_create_user_addresses_table', 1),
(22, '2019_08_04_044445_create_comments_table', 1),
(23, '2019_08_04_074900_create_invoice_product_table', 1),
(24, '2019_09_07_060019_create_festivals_table', 1),
(25, '2019_09_07_062519_create_festival_product_table', 1),
(26, '2019_09_14_071242_create_suggestions_table', 1),
(27, '2019_09_15_115049_create_contact_us_table', 1),
(28, '2019_10_20_161133_create_questions_table', 1),
(29, '2019_10_26_071455_create_activity_log_table', 1),
(30, '2019_10_28_061234_create_permission_tables', 1),
(31, '2020_01_28_082246_create_rates_table', 1),
(32, '2020_10_05_203901_create_couriers_table', 2),
(33, '2020_10_15_135853_create_branches_table', 2),
(61, '2020_10_15_191433_create_branches_table', 3),
(62, '2020_10_15_192439_create_branch_product', 3),
(63, '2020_10_15_193103_add_branch_id_and_courier_id_to_invoices', 3),
(64, '2020_10_15_193448_add_branch_id_to_couriers', 4),
(65, '2020_10_15_201757_remove_columns_from_products', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\Admin', 1),
(1, 'App\\Models\\Admin', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('04e6524c1460d32ff2dca202cd844a5e9bd8be0092e7c22e0aa1e4b9bd5ae4ce2e40085d36fab179', 37, 1, 'Api Login', '[]', 0, '2020-03-19 06:06:19', '2020-03-19 06:06:19', '2021-03-19 09:36:19'),
('0d4c9a81eb3164ec5754715567607cec27b8ddc53925b8b73797bf4e7dbb01ae528900b59be882de', 5, 1, 'Api Login', '[]', 0, '2020-03-17 10:00:16', '2020-03-17 10:00:16', '2021-03-17 13:30:16'),
('0f15ab2f69680af7e9d3f373f1c2b6741e63906110ca0a4a8630c0731e88471193aa6c927ea79c3f', 36, 1, 'Api Login', '[]', 0, '2020-03-28 05:53:06', '2020-03-28 05:53:06', '2021-03-28 10:23:06'),
('1381bd9feb366ab7207f77b0f2bf971b0d6887879cf7bbee54312f5841cdecbc8904e81f31924fee', 5, 1, 'Api Login', '[]', 0, '2020-03-18 05:03:34', '2020-03-18 05:03:34', '2021-03-18 08:33:34'),
('186b1501a7cd374e82cad3b3dbdd60f899bfc9d8374a1aabd09eec3a6a5cdfdaf9ee7f9111a124ba', 40, 1, 'Api Login', '[]', 0, '2020-03-20 05:06:55', '2020-03-20 05:06:55', '2021-03-20 08:36:55'),
('18918b0580cfaf0706f74272a4f6971e8818022597037a9004179b76ab41ed5516110755fa6cdfe8', 53, 1, 'Api Login', '[]', 0, '2020-04-18 18:46:55', '2020-04-18 18:46:55', '2021-04-18 23:16:55'),
('1b7b81699b523a6c682391d85838f276ac6fb5cff640850c6a254a402a43012a0735526b3d629c88', 41, 1, 'Api Login', '[]', 0, '2020-03-19 08:18:12', '2020-03-19 08:18:12', '2021-03-19 11:48:12'),
('1c0e55e9f5ae9318662ff986e9626201534141d53e6d8488ba63fe18de9210908052048c3a62c724', 36, 1, 'Api Login', '[]', 0, '2020-03-28 05:23:53', '2020-03-28 05:23:53', '2021-03-28 09:53:53'),
('1e5847f04e2e46c2404f752aff465d67e0f22ae4a50b9cd41c276baaf3b2c2c947fcbe26abd4ab80', 1, 1, 'Api Login', '[]', 0, '2020-03-14 02:14:43', '2020-03-14 02:14:43', '2021-03-14 05:44:43'),
('237c9719aff35fe6ffac33cb457982aa2a46032ca8639401c29f3b7937b304da98a0167097f55168', 53, 3, 'Api Login', '[]', 0, '2020-10-18 13:21:29', '2020-10-18 13:21:29', '2021-10-18 16:51:29'),
('28f76cdbf8dda241bb0360051eb43706ccc0769711f17a26514a6c47c41efae6b9b7308b855b5b30', 5, 1, 'Api Login', '[]', 0, '2020-03-19 04:23:53', '2020-03-19 04:23:53', '2021-03-19 07:53:53'),
('29352bd59a49dcb71b5a7b0186dcdf9fd44cfe5d718338fa262f7f479d3ad54123d8576cdd5009ff', 5, 1, 'Api Login', '[]', 0, '2020-03-15 05:53:16', '2020-03-15 05:53:16', '2021-03-15 09:23:16'),
('2ab416f7446a63419e0b3b88f947a912ab037c33fb74a2837c07c2dc11730346c0a8c2b5267288ff', 5, 1, 'Api Login', '[]', 0, '2020-03-19 02:14:56', '2020-03-19 02:14:56', '2021-03-19 05:44:56'),
('2c3b4302421ac2452fd86962b1d519ff27f9fdb4a22e9d5b3b2906138658416a04dd2043e16ae367', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:04:54', '2020-03-14 05:04:54', '2021-03-14 08:34:54'),
('2fdc78d3455681308af2d712ecfada1ba79327f02ed4f324fc49d43d5bc1be97925ea3c0d6ced734', 50, 1, 'Api Login', '[]', 0, '2020-03-29 07:02:58', '2020-03-29 07:02:58', '2021-03-29 11:32:58'),
('2fde06b65e0fdc5ccdfdc7add265441294457add30a6795e92a9c9ca6ccc18043e7a9e5adf69edba', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:50:16', '2020-03-14 04:50:16', '2021-03-14 08:20:16'),
('33295c1c8a934b779847e3924a903a183c4a9b5ace73a95b192e7d547d673fe643123ca02b09b2ba', 5, 1, 'Api Login', '[]', 0, '2020-03-16 10:36:55', '2020-03-16 10:36:55', '2021-03-16 14:06:55'),
('33eb6394d54a4c1937bf91142d8947ea27e4d64daacca7de07a148bdcd1aa2ae043726f999bce350', 55, 1, 'Api Login', '[]', 0, '2020-04-27 15:22:12', '2020-04-27 15:22:12', '2021-04-27 19:52:12'),
('3534b6444de8fe528751c6a926ba0cb3391a73db4b76466e6133c8d0771ac8a223624aaf2d7219d8', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:00:53', '2020-03-14 05:00:53', '2021-03-14 08:30:53'),
('3ae3408710b100e157369558f63a85d94f8befa1c87aa62027fee9a02eafe4f76f657ea7317e43eb', 5, 1, 'Api Login', '[]', 0, '2020-03-18 03:55:12', '2020-03-18 03:55:12', '2021-03-18 07:25:12'),
('3d27dbee408210518336d69f52af0cc2c1146350652619a10bbf80d71ab7bdb3373d050ca75b3ec4', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:49:37', '2020-03-14 05:49:37', '2021-03-14 09:19:37'),
('425f71f58685a5df3ea31e07494e95fab75aeedbe3aa501578e66cd30965f7007bb41f09ee3f55c7', 5, 1, 'Api Login', '[]', 0, '2020-03-17 11:16:04', '2020-03-17 11:16:04', '2021-03-17 14:46:04'),
('463cd9f5cbb785428ea326d5ada34264014713677ebde16a7e953f5d3586fe6faa232c07fd81691b', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:50:57', '2020-03-14 04:50:57', '2021-03-14 08:20:57'),
('467a95ae675a00b78fb4ea938cd86dd8e56ccfc128aedae846de86399ab823dc27ac4ffe02b3fe3f', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:54:06', '2020-03-14 04:54:06', '2021-03-14 08:24:06'),
('468f203f3325c8ed93f1066d70760204459a2b0618b55af62c313ed9c715a269ca87e6ef6d3e7bf4', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:33:50', '2020-03-14 05:33:50', '2021-03-14 09:03:50'),
('486400b3b5ff725d33eceb31174729ff5bf4b6c0cf9bf4b0244d3e36aaaa9b16a37f617d3a13b475', 5, 1, 'Api Login', '[]', 0, '2020-03-16 09:27:22', '2020-03-16 09:27:22', '2021-03-16 12:57:22'),
('4bff095e15373709c82a4c24711c84af94137d0b16e6973cffbe543a0a35d5f60295041800b057db', 5, 1, 'Api Login', '[]', 0, '2020-03-17 10:04:41', '2020-03-17 10:04:41', '2021-03-17 13:34:41'),
('50a3baa059d83cc3140b6dcf78f9ed670a7fd1e7905b7860228103ce5b11d7ed9bc287f4903a54f1', 36, 1, 'Api Login', '[]', 0, '2020-03-19 09:50:43', '2020-03-19 09:50:43', '2021-03-19 13:20:43'),
('520699a8ca0f5506efb3d4d68b00ca3e49afbd7a5e55dac32b18bc38772cd4d9aef19daebf132b96', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:54:04', '2020-03-14 04:54:04', '2021-03-14 08:24:04'),
('556e6f61df035063461b20bb1fb80f629d3a3657c878f9ad4a10f60a69c5fa60248e607ad1b08517', 36, 1, 'Api Login', '[]', 0, '2020-03-19 13:48:32', '2020-03-19 13:48:32', '2021-03-19 17:18:32'),
('59a251a29cf0124f6907a3e9da8e5c14c4eacd36d0ef9aabc29f040c7daf9521d7d9dba5d6f45197', 35, 1, 'Api Login', '[]', 0, '2020-03-19 05:19:05', '2020-03-19 05:19:05', '2021-03-19 08:49:05'),
('5ad956b12e04ca235951712b86150e7ebbd3d537c23e421ef8857edd346349510a76c9cfb8d3b660', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:04:13', '2020-03-14 05:04:13', '2021-03-14 08:34:13'),
('5bef76ce2d326899f87c9721fd758c17c35b2c066a5a09b83a51558ad3635fc3475d31a9113a6b50', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:52:27', '2020-03-14 04:52:27', '2021-03-14 08:22:27'),
('5e9492eedc96044774c712768801e1426c88c42f59a6124e592ce6bad6de774d618d1c6a3f3d0ce1', 4, 1, 'Api Login', '[]', 0, '2020-03-14 02:45:38', '2020-03-14 02:45:38', '2021-03-14 06:15:38'),
('60980cba16f6c18331645bb1f191b6f78e619a3f973b903e81e43fd9ad696163bfc09054945b2dbb', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:57:24', '2020-03-14 04:57:24', '2021-03-14 08:27:24'),
('61709c8c51ec87de50aeff1c7e5f92780b107c37a2288c1adacc36e91a7ee50fc7b1b9f82d0ba4cc', 36, 1, 'Api Login', '[]', 0, '2020-03-19 12:56:36', '2020-03-19 12:56:36', '2021-03-19 16:26:36'),
('6ac266f462647b259f77291ad7094dd3784cbbc2225021a1d2d0180f9bdac179c639265390d2cbc6', 23, 1, 'Api Login', '[]', 0, '2020-03-18 06:07:46', '2020-03-18 06:07:46', '2021-03-18 09:37:46'),
('6b56202e0bbd716d8f1d3b5a17b76df592d9ef769c1c10c98d90888291859c786916d343defc14d7', 31, 1, 'Api Login', '[]', 0, '2020-03-18 12:51:39', '2020-03-18 12:51:39', '2021-03-18 16:21:39'),
('6b6d9a9b1ab85c244be9f4773b322d83c0e8064dfa9e16046190cea4847f2027efdd0efdfb9dd25e', 56, 1, 'Api Login', '[]', 0, '2020-05-02 16:04:36', '2020-05-02 16:04:36', '2021-05-02 20:34:36'),
('6c8326be948603c80fa92bc07508fe12e41a871bfe9744608df35a0bf3f380d70039be459e74a15b', 33, 1, 'Api Login', '[]', 0, '2020-03-19 04:37:48', '2020-03-19 04:37:48', '2021-03-19 08:07:48'),
('6e73d6e920fc4187e9dd6cdab2829905126c870f195bbd79727268f320a4a334d350b4441fa923fc', 36, 1, 'Api Login', '[]', 0, '2020-03-22 11:22:07', '2020-03-22 11:22:07', '2021-03-22 15:52:07'),
('71a01525a749d96568b6cab87d28d297b3e5087733f3715232853c34d3384b8622cbca2a033509ce', 5, 1, 'Api Login', '[]', 0, '2020-03-17 04:04:54', '2020-03-17 04:04:54', '2021-03-17 07:34:54'),
('7357ad61b6353b7ba6f5f5787ce6c4b7d77d23681ed89a7d6e2247a55f26dba9c3bebd86bae70700', 45, 1, 'Api Login', '[]', 0, '2020-03-20 09:02:10', '2020-03-20 09:02:10', '2021-03-20 12:32:10'),
('73d71c7fb0581007e866af336dcc74af20d55cb0ee7892ea03653f063d79de9d29e9be3512a5c7ea', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:47:59', '2020-03-14 04:47:59', '2021-03-14 08:17:59'),
('7612e65f4a6dc009351007c1bae05c6f3e29eabb89aad019f12691bd6c24175c3ee967dd4da42dd6', 49, 1, 'Api Login', '[]', 0, '2020-03-29 06:26:26', '2020-03-29 06:26:26', '2021-03-29 10:56:26'),
('7703fd2dd4cf6ad171e8e385377d85b5f7a3aac1d8898b14f0881ad167ded046c4b7f97edb5f55a1', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:58:00', '2020-03-14 04:58:00', '2021-03-14 08:28:00'),
('7c4ca22eeca52552a6ab97b468290ba16fadb44114649181b2c8124696eb69261d48738fe5f9381d', 5, 1, 'Api Login', '[]', 0, '2020-03-17 09:53:48', '2020-03-17 09:53:48', '2021-03-17 13:23:48'),
('7dd62c964f69ad7521a386e017e4f40162d9306167ab3feac8145c247c3dc3b5615f2930bba5f258', 5, 1, 'Api Login', '[]', 0, '2020-03-18 12:52:18', '2020-03-18 12:52:18', '2021-03-18 16:22:18'),
('80954dfaee4989260b96bc99baade321c9e801fdcafe191a28013fbc8672580f866e0a88ddda2e58', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:58:03', '2020-03-14 04:58:03', '2021-03-14 08:28:03'),
('8225b50736adb1251d66c12e83e94452d8849c2c3c6cdaa31f1177d222022ba2b8ebe331911d27c7', 5, 1, 'Api Login', '[]', 0, '2020-03-19 04:57:24', '2020-03-19 04:57:24', '2021-03-19 08:27:24'),
('822e0da1bdcd3fda7d31d40b6a036c53bcc394b70c5dd5bcaa60790efa7f2a6a76b64baaac6d3bcb', 53, 1, 'Api Login', '[]', 0, '2020-04-12 17:15:41', '2020-04-12 17:15:41', '2021-04-12 21:45:41'),
('835b4ff7b5b3180b95c1ca3c7cd0e4dd3c7899973433ff828372dd7a5cf841329e6fb2eb56147e1c', 1, 1, 'Api Login', '[]', 0, '2020-03-14 02:20:08', '2020-03-14 02:20:08', '2021-03-14 05:50:08'),
('867fbd27f2212526e9977c8c748e1d833dc7096f9fce51eb74f66114d631a33707971ff2110dc3f6', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:44:28', '2020-03-14 04:44:28', '2021-03-14 08:14:28'),
('870272542cb534a2f74bf6ad7722340c7bc51b92271db7bb9d00eb75d98cf733cb78aa3211db688c', 17, 1, 'Api Login', '[]', 0, '2020-03-14 07:39:00', '2020-03-14 07:39:00', '2021-03-14 11:09:00'),
('897edfba17007e6613b8981a386471846cd979c727cd0b12827595c8d0a5bb643e0916163cf74c5d', 36, 1, 'Api Login', '[]', 0, '2020-05-02 09:19:04', '2020-05-02 09:19:04', '2021-05-02 13:49:04'),
('8a017f8447135cfd256561e2c28aa96632198cd6aee60583b85bde85276aa2be45002c46abbb9be2', 5, 1, 'Api Login', '[]', 0, '2020-03-18 12:39:51', '2020-03-18 12:39:51', '2021-03-18 16:09:51'),
('8f29fcdbdffbde4ff447243278fe8751c75c43cfea37d71151f6e39764ca9b5349fab0f67b283da0', 47, 1, 'Api Login', '[]', 0, '2020-03-21 09:31:39', '2020-03-21 09:31:39', '2021-03-21 14:01:39'),
('91ff75bf90734fa40f3dd8d6f1459614b61bcaa13d0e27233a5902dd4d5dd164e12edc4b2fa94457', 46, 1, 'Api Login', '[]', 0, '2020-03-20 11:19:47', '2020-03-20 11:19:47', '2021-03-20 14:49:47'),
('949910dafb9b419f1d990035daa580053001e874a53b13d9e971c572efb49932d8bebb8cf55a69e1', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:56:50', '2020-03-14 04:56:50', '2021-03-14 08:26:50'),
('98ddef3985e43e82720520a5af7a594e4e3b2ae3405f3db31bedb3ace43c4d8a8c36dae84dd63c66', 5, 1, 'Api Login', '[]', 0, '2020-03-18 11:43:01', '2020-03-18 11:43:01', '2021-03-18 15:13:01'),
('9953f1159e889da289b42f191aef53f01e8b46a3a9f24487a48840665e262c8605fd35bf43eda100', 5, 1, 'Api Login', '[]', 0, '2020-03-17 09:46:22', '2020-03-17 09:46:22', '2021-03-17 13:16:22'),
('9996b4258288540a09c1edae80c0fc7d0fb84046c7081923168ee2dba2866a9d181fedc99c9f3ad4', 5, 1, 'Api Login', '[]', 0, '2020-03-14 07:58:27', '2020-03-14 07:58:27', '2021-03-14 11:28:27'),
('9b4fa0b90a7152043dba333e59b0bc3430cb1e8471c3ebe5c9036c57ab77b184f30ac619495b9a45', 5, 1, 'Api Login', '[]', 0, '2020-03-17 04:04:17', '2020-03-17 04:04:17', '2021-03-17 07:34:17'),
('9bbf516d905754da7823fb577df5b3191d2a773902a8264e5a0483e27ca5e31a0bffe83cfc596a27', 5, 1, 'Api Login', '[]', 0, '2020-03-14 07:53:26', '2020-03-14 07:53:26', '2021-03-14 11:23:26'),
('9fd839e46e6c3f97d6bc84aa1da71db2a8be13279fb211eaf22e1d8e2380ad826169f945c6790c12', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:05:52', '2020-03-14 05:05:52', '2021-03-14 08:35:52'),
('9ff936a7dfad65f4108096ffc749ecabf05afa6aaadf1ae55ebbbb04c5b9ab69c91c925db00edbfe', 14, 1, 'Api Login', '[]', 0, '2020-03-14 06:42:52', '2020-03-14 06:42:52', '2021-03-14 10:12:52'),
('a0fc45fd2c1221283e428f73399400b7c6ed00a06b576da60c527b6c50b855385039f9c995510f70', 5, 1, 'Api Login', '[]', 0, '2020-03-14 12:07:26', '2020-03-14 12:07:26', '2021-03-14 15:37:26'),
('a396d903caedfc463fc60f4e115b5bd02bed4ea14f6061f71ed5b85ecd1250c3feee78928597bee6', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:57:34', '2020-03-14 04:57:34', '2021-03-14 08:27:34'),
('a4529f3ff79da41e2c6d37c1dd7e0c43fed8b4a8805c5b9de2b4f5e06861d72ef6872c87286ac05f', 18, 1, 'Api Login', '[]', 0, '2020-03-14 07:39:40', '2020-03-14 07:39:40', '2021-03-14 11:09:40'),
('a5f40e89c8316b9b2bfd18d1b98870f577d13b5990e7018026dc0769194bc19c1df3b1cc095d0873', 5, 1, 'Api Login', '[]', 0, '2020-03-18 11:57:48', '2020-03-18 11:57:48', '2021-03-18 15:27:48'),
('a60afbfe6c318075ae87d54b7440f62eed296169f4654037cf4aa4f475b2379038419a54c0165fef', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:53:47', '2020-03-14 04:53:47', '2021-03-14 08:23:47'),
('a6508c658e1fc02d5a6238735041f164c551a28c9267b3d824458c306b04b45cad4e854e218209be', 53, 1, 'Api Login', '[]', 0, '2020-04-18 18:47:17', '2020-04-18 18:47:17', '2021-04-18 23:17:17'),
('a9b8fb5658e27a69b9bf04102e824b04568603770b671a9f28fcc1464e8960470654cd51a6f82250', 36, 1, 'Api Login', '[]', 0, '2020-06-13 00:56:59', '2020-06-13 00:56:59', '2021-06-13 05:26:59'),
('aaca9386b0a426e9994530c07db90790bf216768af66bf2132d181af0fa3adf12e060d1d49950516', 5, 1, 'Api Login', '[]', 0, '2020-03-14 06:04:53', '2020-03-14 06:04:53', '2021-03-14 09:34:53'),
('aae65260d3ab3ca3be4892ec9ae619b0515f9e504eb038a70153863d8a87bb5e7178b1389dc2076d', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:03:55', '2020-03-14 05:03:55', '2021-03-14 08:33:55'),
('abe01c7f91019fc8e7c2e0a24be3da7b78e18463d164aa8d43470251894010c3064cee89f3990945', 5, 1, 'Api Login', '[]', 0, '2020-03-14 06:16:46', '2020-03-14 06:16:46', '2021-03-14 09:46:46'),
('b12b44a0fd7b20b484bffba442285d5ceca56d12a380a160a298f55304e38f3c11b8e913bd0a9d33', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:44:59', '2020-03-14 04:44:59', '2021-03-14 08:14:59'),
('b30f2b57f39791031aabc2dd588c5ea05926de7bd6d0e133c543dab558f1995ed3bfea511f0521b1', 1, 1, 'Api Login', '[]', 0, '2020-03-14 02:21:25', '2020-03-14 02:21:25', '2021-03-14 05:51:25'),
('b4dc4e25c52a26fb96ae415276a251acb7fc8098512daa3ae8c69d471f07d1fb83895d6483c981f6', 36, 1, 'Api Login', '[]', 0, '2020-03-22 12:12:22', '2020-03-22 12:12:22', '2021-03-22 16:42:22'),
('b70765ff5f6f84fe910bdea95c04d949bdf2642dee2d68ee240bb4671f7649276405772b3ecd4aa2', 5, 1, 'Api Login', '[]', 0, '2020-03-14 02:46:14', '2020-03-14 02:46:14', '2021-03-14 06:16:14'),
('b7cd65e023c1c9ac1123cc0247d05a313ab97a6204000f2ebeb8c9afbcee16ddd4802cff7108fc70', 39, 1, 'Api Login', '[]', 0, '2020-03-19 06:31:44', '2020-03-19 06:31:44', '2021-03-19 10:01:44'),
('ba7fa283903050db275b1bd02d02ee869a633526e1f3820622eef76582e3a62d92acf774e69f5304', 5, 1, 'Api Login', '[]', 0, '2020-03-14 10:32:56', '2020-03-14 10:32:56', '2021-03-14 14:02:56'),
('bca8ada6f9d7d7f20c45e515a886bbb411ba94e7302dde4ead1c850406c044f30aea0b5a31ccb654', 5, 1, 'Api Login', '[]', 0, '2020-03-14 07:57:49', '2020-03-14 07:57:49', '2021-03-14 11:27:49'),
('c366dda843e8bc224cf24acf5c4af626b91281e3bad676d615972566793ebf7a0663eeaca98f9e23', 52, 1, 'Api Login', '[]', 0, '2020-04-12 13:43:49', '2020-04-12 13:43:49', '2021-04-12 18:13:49'),
('c4079f4d4cc8a756b102d657322a24d0e0f74a63e39320ece7c09d48af04d4e1993b5c9a6af7f25f', 51, 1, 'Api Login', '[]', 0, '2020-03-31 08:48:46', '2020-03-31 08:48:46', '2021-03-31 13:18:46'),
('c4612545576924b2625def71c6414510152c7ed25a555c1e379fab15e31effefb0b475b58f246abe', 3, 1, 'Api Login', '[]', 0, '2020-03-13 12:22:43', '2020-03-13 12:22:43', '2021-03-13 15:52:43'),
('c7352069e3107dd21af88b3527ed5c83bdb9a8579a25809428e31e023f569d7df67fdeec550b9c4f', 53, 3, 'Api Login', '[]', 0, '2020-10-15 19:03:31', '2020-10-15 19:03:31', '2021-10-15 22:33:31'),
('c8203fc7a84a5b2f54b1ce2056b7e3267a45a23e84dffdb8773d8ac0a1f1d02760b46c28f36e03a8', 1, 1, 'Api Login', '[]', 0, '2020-03-12 13:16:46', '2020-03-12 13:16:46', '2021-03-12 16:46:46'),
('c8edec76ac7e0aa3f2fa40143d1b3f933977dab7d3da6aeb793e4cf7a1924e4622474cb317dc8e69', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:49:53', '2020-03-14 04:49:53', '2021-03-14 08:19:53'),
('cd4fec26199dd00e9e8ae3bf944ee31ca618c5dd1839552ad07338d7a2ced56b60cc8c6b8d2ed48b', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:53:39', '2020-03-14 04:53:39', '2021-03-14 08:23:39'),
('ce5728853291c1ac1a5dac7e32536fd9256e9d1ee969becfa89f895fc661a3a1f97e8cd60e4ed80d', 5, 1, 'Api Login', '[]', 0, '2020-03-14 06:04:29', '2020-03-14 06:04:29', '2021-03-14 09:34:29'),
('cf9c2503d2e1025d5fe38dfb4284d491bcb38d403257086a7696e9bbe6c8d0e3f55d19838ab28673', 1, 1, 'Api Login', '[]', 0, '2020-03-17 11:36:48', '2020-03-17 11:36:48', '2021-03-17 15:06:48'),
('d3ca294eafd6051665553c24b351d86f6173ed6b6a0eed955bf29abfa6ccb9771b95fd3fe6506242', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:53:42', '2020-03-14 04:53:42', '2021-03-14 08:23:42'),
('d697831fbd3c5bcc504ee7ec6b689abe547193965c2795ef2174d533d2c8083b07cf42195a5716f4', 20, 1, 'Api Login', '[]', 0, '2020-03-14 07:45:10', '2020-03-14 07:45:10', '2021-03-14 11:15:10'),
('d6e63208951f88c974cb21aac8bc9a911a6471704a0ccfa4e090c8492bbcd75749fc104eda5a0955', 36, 1, 'Api Login', '[]', 0, '2020-03-20 13:53:25', '2020-03-20 13:53:25', '2021-03-20 17:23:25'),
('e0a94c0f740e152a436337a4db2ea6871106d327e0736eaf058f7527cb837061201275f15a1354bb', 40, 1, 'Api Login', '[]', 0, '2020-03-19 06:45:33', '2020-03-19 06:45:33', '2021-03-19 10:15:33'),
('e10d5638be1d1a6927b82f61429472a9ac31e12c261d13bb2306d1420ad08a9287ecd20328761194', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:58:07', '2020-03-14 04:58:07', '2021-03-14 08:28:07'),
('e2c9a58a27ac1a583f2dec107a7badf227f8181bc60e3daed66f45a448a1a339bb9d01b891bae4a1', 54, 1, 'Api Login', '[]', 0, '2020-04-22 17:28:10', '2020-04-22 17:28:10', '2021-04-22 21:58:10'),
('e368bfa25627d56dea1001b96bbe7f35f10001e6138aeee56d966edd9f7e3ea883cfb677434eba60', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:57:12', '2020-03-14 04:57:12', '2021-03-14 08:27:12'),
('e75a6c9c36f86379a9587ce4f7c6b5e0ab106a5ba36de9d1a82c84fbb326ad05eea1a6d7677f0a19', 5, 1, 'Api Login', '[]', 0, '2020-03-18 07:13:10', '2020-03-18 07:13:10', '2021-03-18 10:43:10'),
('e947e607fcbc53820ec83ed6ed9fafb22642956c56110dbae5a1953115a44bbe2c0bb24f37a12623', 36, 1, 'Api Login', '[]', 0, '2020-04-21 05:30:31', '2020-04-21 05:30:31', '2021-04-21 10:00:31'),
('ee894e4c794c40925c7323e0cf292325d8e778b553bba6201c910c23e416ad80f6efff125955e4fe', 1, 1, 'Api Login', '[]', 0, '2020-03-13 12:17:07', '2020-03-13 12:17:07', '2021-03-13 15:47:07'),
('efe0f382dd291557f8fddfbf5157280f894598ec2a640ad35117ce9050d76014ac40092bcfa485b0', 5, 1, 'Api Login', '[]', 0, '2020-03-18 01:42:50', '2020-03-18 01:42:50', '2021-03-18 05:12:50'),
('f178d1353b5882f8095f0f4366cb16ab432e3e88f3c36226842121c8268fcfbe0a3b94f3fed96841', 32, 1, 'Api Login', '[]', 0, '2020-03-19 02:24:22', '2020-03-19 02:24:22', '2021-03-19 05:54:22'),
('f6a44e349e48109741290dc7cfe8d741a49e81b3d54a762a5673d4135f0d802c88fb5f42962854b6', 5, 1, 'Api Login', '[]', 0, '2020-03-14 08:00:19', '2020-03-14 08:00:19', '2021-03-14 11:30:19'),
('f86aeb0dc0d08c1ee2c17221d56586a58ded4667fbcf6c2398504c00a69091bf8325c0fc41bfd6a9', 1, 1, 'Api Login', '[]', 0, '2020-03-14 02:18:58', '2020-03-14 02:18:58', '2021-03-14 05:48:58'),
('f8df9452f75f33f0870794c8ffd1478450c1f55085c7fc0cd0baa0dab7be5e9d0519432d5970f5b6', 5, 1, 'Api Login', '[]', 0, '2020-03-14 05:23:37', '2020-03-14 05:23:37', '2021-03-14 08:53:37'),
('fd3d3f66cc064268d544e83a9a557826bcca7684fe58a9579249de814ec68dc790094dd5dc87ebfc', 5, 1, 'Api Login', '[]', 0, '2020-03-14 04:59:10', '2020-03-14 04:59:10', '2021-03-14 08:29:10'),
('ff0310bcb34eda467da19b91511870f4fca35ecfa215eb126fd590a1fbb1dcf47758c4bee8598fee', 44, 1, 'Api Login', '[]', 0, '2020-03-20 05:47:57', '2020-03-20 05:47:57', '2021-03-20 09:17:57'),
('ff99e6d8eee0ecc8f700590dd748d19e09c227fa8a8ab428e81062dc3904550f716ec06f2b3ea0ec', 43, 1, 'Api Login', '[]', 0, '2020-03-20 04:59:34', '2020-03-20 04:59:34', '2021-03-20 08:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'EPg38fjaxTa1lvBYJ2D9Tsj8GJXH0Bmx33MK9wbo', 'http://localhost', 1, 0, 0, '2020-03-12 12:59:08', '2020-03-12 12:59:08'),
(2, NULL, 'Laravel Password Grant Client', 'iTZ63t8T2CWRARqK6KdiEXkq3MvWPiQ57LZTDqgN', 'http://localhost', 0, 1, 0, '2020-03-12 12:59:08', '2020-03-12 12:59:08'),
(3, NULL, 'Laravel Personal Access Client', 'NYYTclW3aqCxfmmlXUhqBjoZwwOpAdaBIdaqK2dc', 'http://localhost', 1, 0, 0, '2020-10-15 19:00:56', '2020-10-15 19:00:56'),
(4, NULL, 'Laravel Password Grant Client', 'M97JZtkwUhI3OdxFl84UHMtnlYdPQsvTp3GJUbT3', 'http://localhost', 0, 1, 0, '2020-10-15 19:00:56', '2020-10-15 19:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-12 12:59:08', '2020-03-12 12:59:08'),
(2, 3, '2020-10-15 19:00:56', '2020-10-15 19:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `suggested` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `subcategory_id`, `title`, `slug`, `description`, `priority`, `suggested`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'کباب کوبیده گوسفندی 1', 'کباب-کوبیده-گوسفندی-1', '۱ سیخ ۱۶۰ گرمی گوشت گوسفندی , دورچین مخصوص', 0, 0, '2020-03-12 09:40:32', '2020-06-14 02:18:55', NULL),
(4, 3, 'دوغ ۳۰۰cc', 'دوغ-۳۰۰cc', 'دوغ محلی ۳۰۰cc', 0, 0, '2020-03-14 03:23:47', '2020-03-20 04:58:01', NULL),
(5, 1, 'کباب برگ', 'کباب-برگ', '180 گرم گوشت راسته گوسفندی  -دورچین مخصوص', 0, 0, '2020-03-14 04:12:54', '2020-06-12 20:27:36', NULL),
(6, 4, 'سوپ جو', 'سوپ-جو', 'سوپ جو', 0, 0, '2020-03-14 04:15:18', '2020-03-20 02:34:54', NULL),
(7, 5, 'کوکا قوطی', 'کوکا-قوطی', 'کوکا ۳۰۰cc قوطی', 0, 0, '2020-03-15 07:31:17', '2020-03-17 07:58:03', NULL),
(8, 5, 'کوکا قوطی zero', 'کوکا-قوطی-zero', 'کوکا قوطی بدون شکر و رژیمی 300cc', 0, 0, '2020-03-15 07:32:30', '2020-03-17 07:58:03', NULL),
(9, 1, 'اسپیرت قوطی', 'اسپیرت-قوطی', 'اسپیرت قوطی 300cc', 0, 0, '2020-03-15 07:35:35', '2020-03-15 07:35:49', '2020-03-15 07:35:49'),
(10, 5, 'اسپیرت قوطی', 'اسپیرت-قوطی-1', 'اسپیرت قوطی 300cc', 0, 0, '2020-03-15 07:36:26', '2020-03-15 07:36:31', NULL),
(11, 5, 'نوشابه خانواده', 'نوشابه-خانواده', 'خانواده', 0, 0, '2020-03-15 07:38:09', '2020-03-20 02:17:14', NULL),
(12, 5, 'فانتا قوطی', 'فانتا-قوطی', 'فانتا قوطی 300cc', 0, 0, '2020-03-15 07:39:40', '2020-03-17 06:45:56', NULL),
(13, 5, 'فانتا خانواده', 'فانتا-خانواده', 'فانتا خانواده یک لیتر', 0, 0, '2020-03-15 07:41:14', '2020-03-15 07:49:32', NULL),
(14, 5, 'کوکا خانواده', 'کوکا-خانواده', 'کوکا خانواده یک لیتری', 0, 0, '2020-03-15 07:53:27', '2020-03-20 02:48:36', NULL),
(15, 5, 'اسپریت خانواده', 'اسپریت-خانواده', 'اسپریت خانواده یک لیتری', 0, 0, '2020-03-15 07:54:14', '2020-03-18 00:37:54', NULL),
(20, 8, 'ماهی کباب قزل آلا', 'ماهی-کباب-قزل-آلا', 'ماهی کباب قزل آلا', 0, 0, '2020-03-15 09:21:35', '2020-03-20 02:44:28', NULL),
(21, 9, 'بال کباب', 'بال-کباب', '8 تکه بال کباب', 0, 0, '2020-03-15 09:34:24', '2020-04-12 09:19:40', NULL),
(22, 9, 'کتف کباب', 'کتف-کباب', '8 تکه کتف کباب', 0, 0, '2020-03-15 09:36:29', '2020-04-12 09:19:40', NULL),
(23, 8, 'ماهی کباب سالمون', 'ماهی-کباب-سالمون', 'ماهی کباب سالمون', 0, 0, '2020-03-15 09:37:50', '2020-03-20 01:31:20', NULL),
(24, 11, 'جوجه و کباب بدون استخوان سینه', 'جوجه-و-کباب-بدون-استخوان-سینه', '330گرم جوجه کباب بدون استخوان سینه', 0, 0, '2020-03-15 09:40:45', '2020-04-27 10:54:07', NULL),
(25, 11, 'جوجه و کباب بدون استخوان ران', 'جوجه-و-کباب-بدون-استخوان-ران', '330 گرم جوجه کباب بدون استخوان ران', 0, 0, '2020-03-15 09:42:31', '2020-03-20 01:59:27', NULL),
(26, 11, 'جوجه رول(فیله سینه)', 'جوجه-رولفیله-سینه', '330گرم فیله سینه', 0, 0, '2020-03-15 09:44:01', '2020-03-20 02:00:56', NULL),
(27, 3, 'دوغ خانواده', 'دوغ-خانواده', 'دوغ خانواده', 0, 0, '2020-03-15 12:03:25', '2020-03-15 12:03:38', NULL),
(28, 12, 'سالاد فصل مخصوص', 'سالاد-فصل-مخصوص', 'سالاد فصل مخصوص', 0, 0, '2020-03-15 22:51:39', '2020-03-20 02:22:25', NULL),
(29, 1, 'ماست موسیر', 'ماست-موسیر', 'ماست,موسیر', 0, 0, '2020-03-15 22:59:00', '2020-03-15 22:59:05', '2020-03-15 22:59:05'),
(30, 13, 'ماست موسیر', 'ماست-موسیر-1', 'ماست موسیر', 0, 0, '2020-03-15 23:01:15', '2020-03-20 03:37:49', NULL),
(31, 14, 'زیتون پرورده', 'زیتون-پرورده', 'زیتون پرورده', 0, 0, '2020-03-15 23:02:25', '2020-03-20 02:34:37', NULL),
(35, 8, 'ماهی قزل آلا سرخ شده', 'ماهی-قزل-آلا-سرخ-شده', 'ماهی قزل آلا سرخ شده دورچین مخصوص , سس مخصوص', 0, 0, '2020-03-15 23:19:49', '2020-03-17 07:58:03', NULL),
(36, 1, 'زرشک پلو با مرغ خورشتی', 'زرشک-پلو-با-مرغ-خورشتی', 'زرشک پلو مرغ خورشتی ,دورچین مخصوص, سس مخصوص', 0, 0, '2020-03-15 23:21:44', '2020-03-15 23:21:54', '2020-03-15 23:21:54'),
(37, 1, 'زرشک پلو با مرغ خورشتی', 'زرشک-پلو-با-مرغ-خورشتی-1', 'زرشک پلو مرغ خورشتی ,دورچین مخصوص, سس مخصوص', 0, 0, '2020-03-15 23:22:16', '2020-03-15 23:22:20', '2020-03-15 23:22:20'),
(40, 1, 'کباب لبنانی', 'کباب-لبنانی', '`میکس کوبیده جوجه , دورچین مخصوص', 0, 0, '2020-03-15 23:33:20', '2020-03-20 01:49:18', NULL),
(41, 1, 'کباب قفقازی', 'کباب-قفقازی', 'میکس جوجه و فیله شترمرغ ,دورچین مخصوص', 0, 0, '2020-03-15 23:34:34', '2020-03-20 01:52:41', NULL),
(42, 1, 'کباب بلغاری', 'کباب-بلغاری', 'فیله شترمرغ , دورچین مخصوص', 0, 0, '2020-03-15 23:35:55', '2020-03-20 01:53:02', NULL),
(43, 1, 'کباب بره ممتاز', 'کباب-بره-ممتاز', '220 گرم فیله گوسفندی , دورچین مخصوص', 0, 0, '2020-03-15 23:36:57', '2020-03-29 02:00:11', NULL),
(44, 1, 'شیشلیک ممتاز', 'شیشلیک-ممتاز', '450 گرم دنده گوسفندی - دورچین مخصوص', 0, 0, '2020-03-15 23:38:04', '2020-03-29 02:00:11', NULL),
(46, 10, 'جوجه کباب با استخوان', 'جوجه-کباب-با-استخوان', '450 گرم ران بااستخوان', 0, 0, '2020-03-20 01:55:15', '2020-03-20 03:41:38', NULL),
(50, 20, 'چلو کره', 'چلو-کره', 'چلو درجه یک شمال', 0, 0, '2020-03-20 03:10:40', '2020-03-20 03:42:09', NULL),
(51, 20, 'کته برنجی', 'کته-برنجی', 'چلو شمال بصورت قالبی با ته دیگ سیب زمینی', 0, 0, '2020-03-20 03:15:42', '2020-03-20 03:42:22', NULL),
(52, 20, 'زرشک پلو', 'زرشک-پلو', 'چلو درجه یک شمال', 0, 0, '2020-03-20 03:17:03', '2020-03-20 03:42:36', NULL),
(53, 21, 'مرغ سرخ شده', 'مرغ-سرخ-شده', '350 گرم ران مرغ سرخ شده،سرو بهمراه سس انار', 0, 0, '2020-03-20 03:23:08', '2020-03-20 03:55:00', NULL),
(54, 22, 'مرغ خورشتی', 'مرغ-خورشتی', '350گرم ران مرغ', 0, 0, '2020-03-20 03:25:10', '2020-03-20 03:43:37', NULL),
(55, 23, 'خورشت ماست', 'خورشت-ماست', '250 گرم خورشت ماست مخصوص', 0, 0, '2020-03-20 03:29:53', '2020-04-14 08:55:10', NULL),
(56, 1, 'jjk', 'jjk', 'sd', 0, 0, '2020-10-15 19:31:45', '2020-10-15 19:31:45', NULL),
(57, 1, 'jjk', 'jjk-1', 'sd', 0, 0, '2020-10-15 19:32:20', '2020-10-15 19:32:20', NULL),
(58, 1, 'jjk', 'jjk-2', 'sd', 0, 0, '2020-10-15 19:32:46', '2020-10-15 19:32:46', NULL),
(59, 1, 'jjk', 'jjk-3', 'sd', 0, 0, '2020-10-15 19:33:10', '2020-10-15 19:33:10', NULL),
(60, 1, 'jjk', 'jjk-4', 'sd', 0, 0, '2020-10-15 19:36:48', '2020-10-15 19:36:48', NULL),
(61, 1, 'jjk', 'jjk-5', 'sd', 0, 0, '2020-10-15 19:37:19', '2020-10-15 19:37:19', NULL),
(62, 1, 'jjk', 'jjk-6', 'sd', 0, 0, '2020-10-15 19:37:30', '2020-10-15 19:37:30', NULL),
(63, 1, 'jjk', 'jjk-7', 'sd', 0, 0, '2020-10-15 19:38:53', '2020-10-15 19:38:53', NULL),
(64, 1, 'jjk', 'jjk-8', 'sd', 0, 0, '2020-10-15 19:39:01', '2020-10-15 19:39:01', NULL),
(65, 3, 'jjk', 'jjk-9', 'sd', 0, 0, '2020-10-15 19:39:18', '2020-10-15 19:39:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', '2020-03-12 13:02:26', '2020-03-12 13:02:26');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `title`, `slug`, `description`, `priority`, `created_at`, `updated_at`) VALUES
(1, 1, 'کباب', 'کباب', 'کباب ها دستهه ای از غذا هستن که بر روی اتش تبخ می شوند', 0, '2020-03-12 13:09:45', '2020-03-12 13:09:45'),
(3, 3, 'دوغ', 'دوغ', 'دوغ', 0, '2020-03-14 06:52:34', '2020-03-14 06:52:34'),
(4, 4, 'سوپ', 'سوپ', 'سوپ', 0, '2020-03-14 07:44:20', '2020-03-14 07:44:20'),
(5, 3, 'نوشابه', 'نوشابه', 'نوشابه', 0, '2020-03-15 10:59:17', '2020-03-15 10:59:17'),
(8, 6, 'ماهی', 'ماهی', 'ماهی', 0, '2020-03-15 12:48:56', '2020-03-15 12:48:56'),
(9, 7, 'کتف و بال', 'کتف-و-بال', 'کتف و بال', 0, '2020-03-15 13:01:56', '2020-03-15 13:01:56'),
(10, 7, 'جوجه بااستخوان', 'جوجه-بااستخوان', 'جوجه بااستخوان', 0, '2020-03-15 13:09:54', '2020-03-15 13:09:54'),
(11, 7, 'جوجه بی استخوان', 'جوجه-بی-استخوان', 'جوجه بی استخوان', 0, '2020-03-15 13:10:13', '2020-03-15 13:10:13'),
(12, 4, 'سالاد', 'سالاد', 'سالاد', 0, '2020-03-15 16:29:53', '2020-03-15 16:29:53'),
(13, 4, 'ماست', 'ماست', 'ماست', 0, '2020-03-16 02:30:13', '2020-03-16 02:30:13'),
(14, 4, 'ترشی و زیتون', 'ترشی-و-زیتون', 'ترشیجات و زیتون', 0, '2020-03-16 02:30:41', '2020-03-16 02:30:41'),
(20, 9, 'چلو کره', 'چلو-کره', 'چلو درجه یک ایرانی', 0, '2020-03-20 06:40:04', '2020-03-20 06:40:04'),
(21, 12, 'مرغ سرخ شده', 'مرغ-سرخ-شده', 'ران مرغ سرخ شده،سرو بهمراه سس انار', 0, '2020-03-20 06:51:54', '2020-03-20 06:51:54'),
(22, 12, 'مرغ خورشتی', 'مرغ-خورشتی', 'مرغ خورشتی', 0, '2020-03-20 06:54:06', '2020-03-20 06:54:06'),
(23, 14, 'خورشت ماست', 'خورشت-ماست', 'خورشت ماست', 0, '2020-03-20 06:59:01', '2020-03-20 06:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `suggested_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit` bigint(20) NOT NULL DEFAULT '0',
  `mobile` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `mobile_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `family`, `credit`, `mobile`, `password`, `email`, `email_verified_at`, `mobile_verified_at`, `created_at`, `updated_at`) VALUES
(36, 'رضا', 'حیدری', 0, '09223738411', '$2y$10$.uKCLsBM3eNSJYmRXvCex.4Uxpw8A1tBnIctjtKzr0NOBVlHdm0A6', NULL, NULL, '2020-03-19 09:50:43', '2020-03-19 05:56:14', '2020-03-19 09:50:43'),
(37, 'Saleh', 'MomenZadeh', 0, '09373337574', '$2y$10$lRPkPqxJAePONMgfQPM7jezSF0XUqplX1e5.0psHYAEPCpbwFzPYi', NULL, NULL, '2020-03-19 06:06:19', '2020-03-19 06:06:01', '2020-03-19 06:06:19'),
(38, 'محسن', 'ولیخانی', 0, '0913443780', '$2y$10$HBuz.GqQVP/bszonhMkl.u2YaHwes10NrYYNnDv1VK2l23a/vkPrK', NULL, NULL, NULL, '2020-03-19 06:30:34', '2020-03-19 06:30:34'),
(39, 'محسن', 'ولیخانی', 0, '09138938611', '$2y$10$ieZx3I.cl1aBH8jtVZRuDeOVuaHjSDwhUZ9o4MjEOU1ik3uclKQnq', NULL, NULL, '2020-03-19 06:31:44', '2020-03-19 06:31:24', '2020-03-19 06:31:44'),
(40, 'مهدی', 'اصفهانی', 0, '09901176505', '$2y$10$f8hGaVtrN3CBS73sTz8wjuezonY3WwcD6LvZp55AG63v6jLvlZum2', NULL, NULL, '2020-03-19 06:45:33', '2020-03-19 06:45:14', '2020-03-19 06:45:33'),
(41, 'مسیح', 'جزایری', 0, '09361520178', '$2y$10$g/J3jvh.gUX87pAiy.Sr4.98IX6s481MOOO9IQiA9EjdAG1YjobzO', NULL, NULL, '2020-03-19 08:18:12', '2020-03-19 08:15:50', '2020-03-19 08:18:12'),
(42, 'رضا', 'حیدری', 0, '09227378411', '$2y$10$VLganSv1n1Oal26g7TFqFeMqzdBL6fdKWPCOBLdyBFTyNq4.MjYgu', NULL, NULL, NULL, '2020-03-19 09:34:16', '2020-03-19 09:34:16'),
(43, 'Naz', 'Usfn', 0, '09300820680', '$2y$10$8W5d6v2rPzCeEkfsF08TKe7yj5oBjsMYqD2rWlowWZko9tmHIuN9u', NULL, NULL, '2020-03-20 04:59:34', '2020-03-20 04:59:21', '2020-03-20 04:59:34'),
(44, 'مینو', 'مرادی', 0, '09120350426', '$2y$10$ju3mrmzZJHMbV8mDoK.BZOew1gwqZ8e4D20avUPsMkNPBkJP0zF1K', NULL, NULL, '2020-03-20 05:47:57', '2020-03-20 05:47:17', '2020-03-20 05:47:57'),
(45, 'کامیار', 'کیان پور', 0, '09125979866', '$2y$10$72ZqnMBGzokwkDbjlZ3CUe4PE31FTbOotFgejHuPgfamh/vvwKy0O', NULL, NULL, '2020-03-20 09:02:10', '2020-03-20 09:01:46', '2020-03-20 09:02:10'),
(46, 'محسن', 'مسرورنیا', 0, '09131651009', '$2y$10$AQhWm56dJextJ2dOas6JJu59CBrZVG0VHvCEawVls2.GYRUkhB/D2', NULL, NULL, '2020-03-20 11:19:47', '2020-03-20 11:19:22', '2020-03-20 11:19:47'),
(47, 'مسعود', 'دباغی', 0, '09392322159', '$2y$10$wLlh3zoJvMz6.nMrp9K0Y.LrPFinaxCYlk9t0kXfIU9x.7ZZWLI0u', NULL, NULL, '2020-03-21 09:31:39', '2020-03-21 09:31:21', '2020-03-21 09:31:39'),
(48, 'masih', 'jazayeri', 0, '09131706878', '$2y$10$wmeJMTeWu9T.Aa7mOsMS4.e0Y1TwtvouiqFp/Al6sdGZwWQoyaqFa', NULL, NULL, NULL, '2020-03-22 12:02:50', '2020-03-22 12:02:50'),
(49, 'جلال', 'جهانی', 0, '09140832329', '$2y$10$dauOZC0AAvHLhpWGmII3iusps.XhE/fsawy4dMRAYOoHipWsXaOmG', NULL, NULL, '2020-03-29 06:26:25', '2020-03-29 06:26:03', '2020-03-29 06:26:25'),
(50, 'mehnoosh', 'moradi', 0, '09133800245', '$2y$10$CTVYG9jAqjT7CrN5M697ne7BL4aB8tqRBmBIBbydQL../HHl0eh8y', NULL, NULL, '2020-03-29 07:02:57', '2020-03-29 07:02:31', '2020-03-29 07:02:57'),
(51, 'Zahra', 'Hashemi', 0, '09138133095', '$2y$10$6z5h1fNdqLNOww34r.PYNuz9a7cO8CGROiZKp0OfSA8aj4jO1uJp.', NULL, NULL, '2020-03-31 08:48:45', '2020-03-31 08:48:31', '2020-03-31 08:48:45'),
(52, 'ناهید', 'ماهرو', 0, '09309489538', '$2y$10$7MX4/60Y/LOI6/Rnr3zN6uO/BYT.g/ICr9bws6xLJcaxCUQJNxDNi', NULL, NULL, '2020-04-12 13:43:48', '2020-04-12 13:43:32', '2020-04-12 13:43:48'),
(53, 'سیدحسین', 'موسوی', 0, '09391727950', '$2y$10$jQxgum6kaYIOA/vFR934PuodfWDZ8c7P4kjiD99u.TyMyW2CuRqk2', NULL, NULL, '2020-04-12 17:15:40', '2020-04-12 17:15:15', '2020-04-12 17:15:40'),
(54, 'اصغر', 'طالعی', 0, '09305622007', '$2y$10$RbIT7fIlAKyGuAjMz5KcQelXK54ODxBKeeJQykKmN.tSPtVRdmpHG', NULL, NULL, '2020-04-22 17:28:09', '2020-04-22 17:27:42', '2020-04-22 17:28:09'),
(55, 'حسين', 'جندى', 0, '09198471837', '$2y$10$LAc2PEmztE1dWvfRivc1TOCQ0C15KiEbvezOpEHapWe5izApE6HWy', NULL, NULL, '2020-04-27 15:22:11', '2020-04-27 15:21:56', '2020-04-27 15:22:11'),
(56, 'Reza', 'Saeidi', 0, '09131061974', '$2y$10$XMnxRPo.f8.Epvp5.FkEleyJCtTKqcH5NsiRTg/4dWr1fLJP1nTii', NULL, NULL, '2020-05-02 16:04:36', '2020-05-02 16:04:23', '2020-05-02 16:04:36'),
(57, 'Reza', 'Mehrani', 0, '09138769383', '$2y$10$g0zAvsAlNc1aChCEeA6quuSxkiPDnYbnJ1LgfsAFJtvFA8NmyA8Ye', NULL, NULL, NULL, '2020-05-27 01:26:05', '2020-05-27 01:27:37'),
(58, 'سمانه', 'نجفی', 0, '09227118947', '$2y$10$20sOibWj2sOEhmnzpFH8Ru09q5RV3Ehf9fJVWShx3DHbcQ0OyHqDK', NULL, NULL, NULL, '2020-05-27 11:35:45', '2020-05-27 11:36:41'),
(59, 'آرش', 'رضاخواه', 0, '09138053248', '$2y$10$j0dEzoXydp5qiItsiaApcOaFyePSMNx2v78/OS3GfbnbCtSLm8I5u', NULL, NULL, NULL, '2020-06-05 14:11:50', '2020-06-05 14:17:41'),
(60, 'پریا', 'قدیریان', 0, '09138179500', '$2y$10$DWa2hBOaBAgD97Yqr4iDzeEkhM6oYJw8CFaQJ3eBpoBn.vkkrHf9W', NULL, NULL, NULL, '2020-06-11 11:45:04', '2020-06-11 11:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `user_id`, `name`, `family`, `address`, `mobile`, `phone`, `created_at`, `updated_at`) VALUES
(7, 39, 'محسن', 'ولیخانی', 'خیابان شریف واقفی.کوچه ۲۰', '09134437800', '03136626262', '2020-03-19 06:37:25', '2020-03-19 06:37:25'),
(8, 41, 'مسیح', 'جزایری', 'سپاهان شهر الوند ۴ الوند ۴', '09361520178', '03136515811', '2020-03-19 12:03:44', '2020-03-19 12:03:44'),
(9, 36, 'تره', 'رهرهر', 'فززعزره', '08889653212', '03899655214', '2020-03-19 13:07:10', '2020-03-19 13:07:10'),
(10, 36, 'رضا', 'حیدری', 'ندخدخخلخلخلحللخدرحوجدخرحلخل\nاحااحل', '09223738411', '03833442118', '2020-03-19 14:55:03', '2020-03-19 14:55:03'),
(11, 43, 'Naz', 'Usfn', 'سپاهان شهر بلوارشاهد', '09300820680', '03136507100', '2020-03-20 05:03:28', '2020-03-20 05:03:28'),
(12, 37, 'صالح', 'مومن زاده', 'اصفهان', '09373337574', '03138762120', '2020-03-20 05:03:40', '2020-03-20 05:03:40'),
(13, 40, 'مهدی', 'اصفهانی', 'بلوار شاهد', '09901176505', '03136507100', '2020-03-20 05:07:43', '2020-03-20 05:07:43'),
(14, 45, 'کامیار', 'کیان پور', 'ابتدای بلوار توحید- ساختمان خشایار- واحد۱۳', '09125979866', '03136504284', '2020-03-20 09:07:33', '2020-03-20 09:07:33'),
(15, 36, 'رضا', 'حیدری', 'خلخاحاحدح\nخررحرحرححرححر', '09665354178', '08966552443', '2020-03-22 11:38:05', '2020-03-22 11:38:05'),
(16, 49, 'جلال', 'جهانی', 'پشت سرت', '09010653269', '09140832329', '2020-03-29 06:27:53', '2020-03-29 06:27:53'),
(17, 52, 'ناهید', 'ماهرو', 'بلوارسهروردی،جنب ترمینال زاینده رود،کوچه جلالان شرقی،کوچه آریا،ساختمان کاج،طبقه۱', '09309489538', '03137861249', '2020-04-12 13:47:41', '2020-04-12 13:47:41'),
(18, 53, 'سید حسین', 'موسوی', 'قم', '09391727950', '02537226190', '2020-04-12 17:16:21', '2020-04-12 17:16:21'),
(19, 55, 'حسين', 'جندى', 'خیابان هزار جریب - بعد از دانشگاه مهاجر - نرسیده به پل عابر پیاده - کوی شهید رضا پاکدل - مجموعه پیامبر اعظم ساختمان وسطى - طبقه ۴ - واحد ۱۵ - آقای جندی - ۰۹۱۹۸۴۷۱۸۳۷', '09198471837', '09198471837', '2020-04-27 15:23:54', '2020-04-27 15:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_favorite`
--

CREATE TABLE `user_favorite` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch_product`
--
ALTER TABLE `branch_product`
  ADD UNIQUE KEY `branch_product_branch_id_product_id_unique` (`branch_id`,`product_id`),
  ADD KEY `branch_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_product_id_foreign` (`product_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `couriers_mobile_unique` (`mobile`);

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `festivals`
--
ALTER TABLE `festivals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `festivals_slug_unique` (`slug`);

--
-- Indexes for table `festival_product`
--
ALTER TABLE `festival_product`
  ADD KEY `festival_product_product_id_foreign` (`product_id`),
  ADD KEY `festival_product_festival_id_foreign` (`festival_id`);

--
-- Indexes for table `gateway_transactions`
--
ALTER TABLE `gateway_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateway_transactions_logs`
--
ALTER TABLE `gateway_transactions_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gateway_transactions_logs_transaction_id_foreign` (`transaction_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_user_id_foreign` (`user_id`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD KEY `invoice_product_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_subcategory_id_foreign` (`subcategory_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rates_user_id_foreign` (`user_id`),
  ADD KEY `rates_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subcategories_slug_unique` (`slug`),
  ADD KEY `subcategories_category_id_foreign` (`category_id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD KEY `suggestions_product_id_foreign` (`product_id`),
  ADD KEY `suggestions_suggested_id_foreign` (`suggested_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_favorite`
--
ALTER TABLE `user_favorite`
  ADD KEY `user_favorite_user_id_foreign` (`user_id`),
  ADD KEY `user_favorite_product_id_foreign` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `festivals`
--
ALTER TABLE `festivals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gateway_transactions`
--
ALTER TABLE `gateway_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gateway_transactions_logs`
--
ALTER TABLE `gateway_transactions_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `branch_product`
--
ALTER TABLE `branch_product`
  ADD CONSTRAINT `branch_product_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branch_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `festival_product`
--
ALTER TABLE `festival_product`
  ADD CONSTRAINT `festival_product_festival_id_foreign` FOREIGN KEY (`festival_id`) REFERENCES `festivals` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `festival_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gateway_transactions_logs`
--
ALTER TABLE `gateway_transactions_logs`
  ADD CONSTRAINT `gateway_transactions_logs_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `gateway_transactions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD CONSTRAINT `invoice_product_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoice_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD CONSTRAINT `suggestions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suggestions_suggested_id_foreign` FOREIGN KEY (`suggested_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_favorite`
--
ALTER TABLE `user_favorite`
  ADD CONSTRAINT `user_favorite_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_favorite_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

