@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.invoices.show',[ $invoice->id])}}">
                            <i class="icon-user"></i> فاکتور</a></li>
                    <li><a href="{{route('admin.dashboard.invoices.edit',[ $invoice->id])}}"> <i class="icon-user"></i>
                            ویرایش فاکتور</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات فاکتور
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$invoice->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کاربر :</span> {{$invoice->user->name}} {{$invoice->user->family}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>پیک :</span> {{$invoice->courier?$invoice->courier->name:$invoice->courier}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت پرداخت :</span> {{$invoice->paid}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت کل :</span> {{$invoice->total_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت کالاها :</span> {{$invoice->orders_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت پرداختی :</span> {{$invoice->payable_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کدتخفیف :</span> {{isset($invoice->discount)?$invoice->discount->title:'نداشته'}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تخفیف کل :</span> {{$invoice->total_price-$invoice->payable_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت :</span>{{$invoice->statusLabel}}</p>
                        </div>
                        @foreach($invoice->products as $product)
                            <div class="bio-row">
                                <p><span>{{$product->title}} :</span> {{$product->pivot->count}} عدد</p>
                            </div>
                        @endforeach
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$invoice->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$invoice->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$invoice->updated_at_tehran}}</p>
                        </div>
                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    تراکنش ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th class="hidden-phone">درگاه</th>
                        <th class="hidden-phone">قیمت</th>
                        <th class="hidden-phone">ref id</th>
                        <th class="hidden-phone">وضعیت</th>
                        <th class="hidden-phone">ip</th>
                        <th class="hidden-phone">تاریخ شروع</th>
                        <th class="hidden-phone">تاریخ پایان</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach($invoice->transactions as $transaction)
                            <tr class="gradeX odd">
                                <td class=" ">{{$transaction->id}}</td>
                                <td class=" ">{{$transaction->port}}</td>
                                <td class="center hidden-phone ">{{$transaction->price}}</td>
                                <td class="center hidden-phone ">{{$transaction->ref_id}}</td>
                                <td class="center hidden-phone ">{{$transaction->status}}</td>
                                <td class="center hidden-phone ">{{$transaction->ip}}</td>
                                <td class="center hidden-phone ">{{$transaction->created_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$transaction->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
